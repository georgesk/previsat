/*
 *     PreviSat, Satellite tracking software
 *     Copyright (C) 2005-2024  Astropedia web: http://previsat.free.fr  -  mailto: previsat.app@gmail.com
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * _______________________________________________________________________________________________________
 *
 * Nom du fichier
 * >    messageconst.h
 *
 * Localisation
 * >    librairies.exceptions
 *
 * Heritage
 * >
 *
 * Description
 * >     Constantes liees aux boites de messages
 *
 * Auteur
 * >    Astropedia
 *
 * Date de creation
 * >    1er septembre 2012
 *
 * Date de revision
 * >    3 juin 2015
 *
 */

#ifndef MESSAGECONST_H
#define MESSAGECONST_H

/*
 * Enumerations
 */
enum class MessageType {

    ERREUR = -1,
    INFO = 0,
    WARNING = 1
};


/*
 * Declaration des constantes
 */



#endif // MESSAGECONST_H
