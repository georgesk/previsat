/*
 *     PreviSat, Satellite tracking software
 *     Copyright (C) 2005-2024  Astropedia web: http://previsat.free.fr  -  mailto: previsat.app@gmail.com
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * _______________________________________________________________________________________________________
 *
 * Nom du fichier
 * >    corpsconst.h
 *
 * Localisation
 * >    librairies.corps
 *
 * Heritage
 * >
 *
 * Description
 * >     Constantes liees aux corps
 *
 * Auteur
 * >    Astropedia
 *
 * Date de creation
 * >    23 juillet 2022
 *
 * Date de revision
 * >
 *
 */

#ifndef CORPSCONST_H
#define CORPSCONST_H

/*
 * Enumerations
 */

/*
 * Definitions des constantes
 */
namespace CORPS {

static constexpr int ELEMENT_PHASAGE_INDEFINI = -999;
static constexpr int NB_ORB_INDEFINI = -1;

static constexpr double MAGNITUDE_INDEFINIE = 99.;

}

#endif // CORPSCONST_H
