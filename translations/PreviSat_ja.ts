<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ja_JP" sourcelanguage="fr_FR">
<context>
    <name>AfficherResultats</name>
    <message>
        <location filename="../src/interface/afficherresultats.ui" line="128"/>
        <source>Afficher la carte</source>
        <translation>地図を表示する</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.ui" line="204"/>
        <location filename="../src/interface/afficherresultats.ui" line="207"/>
        <source>Enregistrer</source>
        <translation>保存する</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.ui" line="216"/>
        <location filename="../src/interface/afficherresultats.ui" line="219"/>
        <source>Enregistrer fichier texte</source>
        <translation>テキスト形式で保存する</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="127"/>
        <source>Prévisions de passage</source>
        <translation>通過予報</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="128"/>
        <location filename="../src/interface/afficherresultats.cpp" line="139"/>
        <location filename="../src/interface/afficherresultats.cpp" line="147"/>
        <location filename="../src/interface/afficherresultats.cpp" line="157"/>
        <location filename="../src/interface/afficherresultats.cpp" line="1194"/>
        <location filename="../src/interface/afficherresultats.cpp" line="1204"/>
        <location filename="../src/interface/afficherresultats.cpp" line="1216"/>
        <source>Satellite</source>
        <translation>衛星</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="128"/>
        <location filename="../src/interface/afficherresultats.cpp" line="139"/>
        <location filename="../src/interface/afficherresultats.cpp" line="157"/>
        <source>Date de début</source>
        <comment>Date and hour</comment>
        <translation>開始日時</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="128"/>
        <location filename="../src/interface/afficherresultats.cpp" line="139"/>
        <location filename="../src/interface/afficherresultats.cpp" line="157"/>
        <source>Date de fin</source>
        <comment>Date and hour</comment>
        <translation>終了日時</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="129"/>
        <location filename="../src/interface/afficherresultats.cpp" line="158"/>
        <location filename="../src/interface/afficherresultats.cpp" line="191"/>
        <location filename="../src/interface/afficherresultats.cpp" line="1247"/>
        <location filename="../src/interface/afficherresultats.cpp" line="1255"/>
        <source>Magnitude</source>
        <translation>等級</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="129"/>
        <location filename="../src/interface/afficherresultats.cpp" line="148"/>
        <location filename="../src/interface/afficherresultats.cpp" line="158"/>
        <location filename="../src/interface/afficherresultats.cpp" line="193"/>
        <location filename="../src/interface/afficherresultats.cpp" line="1249"/>
        <location filename="../src/interface/afficherresultats.cpp" line="1258"/>
        <location filename="../src/interface/afficherresultats.cpp" line="1272"/>
        <source>Hauteur Soleil</source>
        <translation>太陽仰角</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="133"/>
        <source>Évènements orbitaux</source>
        <translation>軌道上事象</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="137"/>
        <source>Flashs</source>
        <translation>フレア</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="140"/>
        <location filename="../src/interface/afficherresultats.cpp" line="1197"/>
        <location filename="../src/interface/afficherresultats.cpp" line="1208"/>
        <source>Magn</source>
        <comment>Magnitude</comment>
        <translation>等級</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="140"/>
        <location filename="../src/interface/afficherresultats.cpp" line="1207"/>
        <source>Mir</source>
        <comment>Mirror</comment>
        <translation>鏡面</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="140"/>
        <location filename="../src/interface/afficherresultats.cpp" line="1199"/>
        <location filename="../src/interface/afficherresultats.cpp" line="1209"/>
        <location filename="../src/interface/afficherresultats.cpp" line="1221"/>
        <source>Haut Soleil</source>
        <comment>Solar elevation</comment>
        <translation>太陽仰角</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="144"/>
        <source>Transits</source>
        <translation>太陽面・月面通過</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="147"/>
        <location filename="../src/interface/afficherresultats.cpp" line="1219"/>
        <source>Cst</source>
        <comment>Constellation</comment>
        <translation>星座</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="147"/>
        <location filename="../src/interface/afficherresultats.cpp" line="1253"/>
        <location filename="../src/interface/afficherresultats.cpp" line="1266"/>
        <source>Angle</source>
        <translation>角度</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="148"/>
        <location filename="../src/interface/afficherresultats.cpp" line="1219"/>
        <source>Type</source>
        <comment>Transit or conjunction</comment>
        <translation>種類</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="148"/>
        <location filename="../src/interface/afficherresultats.cpp" line="1220"/>
        <source>Corps</source>
        <translation>天体</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="1220"/>
        <source>Ill</source>
        <comment>Illumination</comment>
        <translation>照射</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="148"/>
        <location filename="../src/interface/afficherresultats.cpp" line="1220"/>
        <source>Durée</source>
        <translation>継続時間</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="129"/>
        <location filename="../src/interface/afficherresultats.cpp" line="158"/>
        <source>Hauteur max</source>
        <comment>Maximum elevation</comment>
        <translation>最大仰角</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="140"/>
        <source>Hauteur Max</source>
        <comment>Maximum elevation</comment>
        <translation>最大仰角</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="147"/>
        <source>Date du maximum</source>
        <comment>Date and hour</comment>
        <translation>最適日時</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="148"/>
        <source>Illum</source>
        <comment>Illumination</comment>
        <translation>照射</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="156"/>
        <source>Passages des Starlink</source>
        <translation>Starlinkの通過</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="190"/>
        <source>Hauteur maximale</source>
        <translation>最大仰角</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="192"/>
        <location filename="../src/interface/afficherresultats.cpp" line="1254"/>
        <source>Miroir</source>
        <translation>鏡面</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="198"/>
        <location filename="../src/interface/afficherresultats.cpp" line="1241"/>
        <source>Constellation</source>
        <translation>星座</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="199"/>
        <location filename="../src/interface/afficherresultats.cpp" line="1267"/>
        <source>Illumination</source>
        <translation>照射</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="200"/>
        <location filename="../src/interface/afficherresultats.cpp" line="1268"/>
        <source>secondes</source>
        <translation>秒</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="691"/>
        <source>Unité de distance               : %1</source>
        <translation>距離の単位     : %1</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="699"/>
        <source>Age de l&apos;élément                : %1 jours (au %2)</source>
        <translation>要素の年齢                : %1日 (%2現在)</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="1194"/>
        <location filename="../src/interface/afficherresultats.cpp" line="1204"/>
        <location filename="../src/interface/afficherresultats.cpp" line="1216"/>
        <source>Date</source>
        <comment>Date and hour</comment>
        <translation>日時</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="1195"/>
        <location filename="../src/interface/afficherresultats.cpp" line="1205"/>
        <location filename="../src/interface/afficherresultats.cpp" line="1217"/>
        <source>Azimut Sat</source>
        <comment>Satellite azimuth</comment>
        <translation>衛星方位角</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="1195"/>
        <location filename="../src/interface/afficherresultats.cpp" line="1205"/>
        <location filename="../src/interface/afficherresultats.cpp" line="1217"/>
        <source>Hauteur Sat</source>
        <comment>Satellite elevation</comment>
        <translation>衛星仰角</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="1196"/>
        <location filename="../src/interface/afficherresultats.cpp" line="1206"/>
        <location filename="../src/interface/afficherresultats.cpp" line="1218"/>
        <source>AD Sat</source>
        <comment>Satellite right ascension</comment>
        <translation>衛星赤経</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="1196"/>
        <location filename="../src/interface/afficherresultats.cpp" line="1206"/>
        <location filename="../src/interface/afficherresultats.cpp" line="1218"/>
        <source>Decl Sat</source>
        <comment>Satellite declination</comment>
        <translation>衛星赤緯</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="1197"/>
        <location filename="../src/interface/afficherresultats.cpp" line="1207"/>
        <source>Const</source>
        <comment>Constellation</comment>
        <translation>星座</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="1198"/>
        <location filename="../src/interface/afficherresultats.cpp" line="1208"/>
        <location filename="../src/interface/afficherresultats.cpp" line="1269"/>
        <source>Altitude</source>
        <comment>Altitude of satellite</comment>
        <translation>高度</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="1198"/>
        <location filename="../src/interface/afficherresultats.cpp" line="1211"/>
        <location filename="../src/interface/afficherresultats.cpp" line="1223"/>
        <location filename="../src/interface/afficherresultats.cpp" line="1256"/>
        <location filename="../src/interface/afficherresultats.cpp" line="1270"/>
        <source>Distance</source>
        <translation>距離</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="1198"/>
        <location filename="../src/interface/afficherresultats.cpp" line="1209"/>
        <location filename="../src/interface/afficherresultats.cpp" line="1221"/>
        <source>Az Soleil</source>
        <comment>Solar azimuth</comment>
        <translation>太陽の方位角</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="1207"/>
        <location filename="../src/interface/afficherresultats.cpp" line="1219"/>
        <source>Ang</source>
        <comment>Angle</comment>
        <translation>角度</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="1208"/>
        <location filename="../src/interface/afficherresultats.cpp" line="1221"/>
        <source>Dist</source>
        <comment>Range</comment>
        <translation>距離</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="1210"/>
        <location filename="../src/interface/afficherresultats.cpp" line="1222"/>
        <source>Long Max</source>
        <comment>Longitude of the maximum</comment>
        <translation>最適経度</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="1210"/>
        <location filename="../src/interface/afficherresultats.cpp" line="1222"/>
        <source>Lat Max</source>
        <comment>Latitude of the maximum</comment>
        <translation>最適緯度</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="1211"/>
        <source>Magn Max</source>
        <comment>Magnitude at the maximum</comment>
        <translation>最大等級</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="1220"/>
        <source>Alt</source>
        <comment>Altitude of satellite</comment>
        <translation>高度</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="1237"/>
        <source>Azimut satellite</source>
        <translation>衛星方位角</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="1238"/>
        <source>Hauteur satellite</source>
        <translation>衛星仰角</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="1239"/>
        <source>Ascension droite satellite</source>
        <translation>衛星赤経</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="1240"/>
        <source>Déclinaison satellite</source>
        <translation>衛星赤緯</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="1248"/>
        <location filename="../src/interface/afficherresultats.cpp" line="1257"/>
        <location filename="../src/interface/afficherresultats.cpp" line="1271"/>
        <source>Azimut Soleil</source>
        <translation>太陽方位角</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="1259"/>
        <location filename="../src/interface/afficherresultats.cpp" line="1273"/>
        <source>Longitude du maximum</source>
        <translation>最適地点の経度</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="1260"/>
        <location filename="../src/interface/afficherresultats.cpp" line="1274"/>
        <source>Latitude du maximum</source>
        <translation>最適地点の緯度</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="1261"/>
        <source>Magnitude au maximum</source>
        <translation>最適地点の等級</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="1262"/>
        <location filename="../src/interface/afficherresultats.cpp" line="1275"/>
        <source>Distance au maximum</source>
        <translation>最適地点からの距離</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="1382"/>
        <source>Détail du passage</source>
        <translation>通過の詳細</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="1386"/>
        <source>Détail du flash</source>
        <translation>フレアの詳細</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="1390"/>
        <source>Détail du transit ou conjonction</source>
        <translation>通過又は合の詳細</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="1434"/>
        <source>Enregistrer sous</source>
        <translation>名前を付けて保存</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="1435"/>
        <source>Fichiers PNG (*.png);;Fichiers JPEG (*.jpg *.jpeg);;Fichiers BMP (*.bmp);;Tous les fichiers (*.*)</source>
        <translation>PNGファイル (*.png);;JPEGファイル (*.jpg *.jpeg);;BMPファイル (*.bmp);;すべてのファイル (*.*)</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="1465"/>
        <source>Fichiers texte (*.txt);;Tous les fichiers (*.*)</source>
        <translation>テキストファイル (*.txt);;すべてのファイル (*.*)</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="1531"/>
        <source>   Date      Heure      Azimut Sat Hauteur Sat  AD Sat    Decl Sat   Cst  Ang  Type Corps Ill Durée  Altitude  Distance  Az Soleil  Haut Soleil   Long Max    Lat Max     Distance</source>
        <comment>Date, Hour, Satellite azimuth, Satellite elevation, Satellite right ascension, Satellite declination, Constellation, Angle, Type, Body, Illumination, Duration, Altitude of satellite, Range, Solar azimuth, Solar elevation, Longitude of the maximum, Latitude of the maximum, Range from the maximum</comment>
        <translation>    日付      時刻       衛星方位角   衛星仰角    衛星赤経   衛星赤緯  星座  角度   種類    天体   照射 継続時間   高度      距離  太陽方位角   太陽仰角      最適経度    最適緯度       距離</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="1464"/>
        <source>Enregistrer sous...</source>
        <translation>名前を付けて保存...</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="1502"/>
        <source>(numéro NORAD : %1)</source>
        <translation>(NORAD番号 : %1)</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="1510"/>
        <location filename="../src/interface/afficherresultats.cpp" line="1541"/>
        <source>   Date      Heure    Azimut Sat Hauteur Sat  AD Sat    Decl Sat  Const Magn  Altitude  Distance  Az Soleil  Haut Soleil</source>
        <comment>Date, Hour, Satellite azimuth, Satellite elevation, Satellite right ascension, Satellite declination, Constellation, Magnitude, Altitude of satellite, Range, Solar azimuth, Solar elevation</comment>
        <translation>   日付      時刻      衛星方位角   衛星仰角   衛星赤経    衛星赤緯  星座  等級      高度      距離   太陽方位角   太陽仰角</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="1519"/>
        <source>Satellite     Date      Heure      Azimut Sat Hauteur Sat  AD Sat    Decl Sat   Cst  Ang   Mir Magn       Alt      Dist  Az Soleil  Haut Soleil   Long Max    Lat Max    Magn Max  Distance</source>
        <comment>Satellite, Date, Hour, Satellite azimuth, Satellite elevation, Satellite right ascension, Satellite declination, Constellation, Angle, Mirror, Magnitude, Altitude of satellite, Range, Solar azimuth, Solar elevation, Longitude of the maximum, Latitude of the maximum, Magnitude at the maximum, Range from the maximum</comment>
        <translation>衛星           日付      時刻       衛星方位角   衛星仰角    衛星赤経   衛星赤緯  星座  角度  鏡面 等級      高度      距離  太陽方位角   太陽仰角      最適経度     最適緯度    最大等級    距離</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="1550"/>
        <source>   Date      Heure     PSO    Longitude  Latitude  Évènements</source>
        <comment>Date, Hour, In orbit position, Longitude, Latitude, Events</comment>
        <translation>    日付      時刻     位置       経度         緯度     事象</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="1682"/>
        <source>Temps écoulé : %1s</source>
        <translation>経過時間 : %1s</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="420"/>
        <source>km</source>
        <comment>Kilometer</comment>
        <translation>km</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="420"/>
        <location filename="../src/interface/afficherresultats.cpp" line="421"/>
        <source>m</source>
        <comment>meter</comment>
        <translation>m</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="420"/>
        <source>ft</source>
        <comment>foot</comment>
        <translation>ft</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="446"/>
        <source>Longitude</source>
        <translation>経度</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="447"/>
        <source>Latitude</source>
        <translation>緯度</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="448"/>
        <source>Altitude</source>
        <comment>Altitude of observer</comment>
        <translation>高さ</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="564"/>
        <source>Double-cliquez sur une ligne pour afficher plus de détails</source>
        <translation>行をダブルクリックすると詳細が表示されます</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="703"/>
        <source>Age de l&apos;élément le plus récent : %1 jours (au %2)
Age de l&apos;élément le plus ancien : %3 jours</source>
        <translation>最新要素の年齢 : %1日 (%2現在)
最古要素の年齢 : %3日</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="755"/>
        <source>W</source>
        <comment>West</comment>
        <translation>西</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="755"/>
        <source>E</source>
        <comment>East</comment>
        <translation>東</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="757"/>
        <source>N</source>
        <comment>North</comment>
        <translation>北</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="757"/>
        <source>S</source>
        <comment>South</comment>
        <translation>南</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="875"/>
        <location filename="../src/interface/afficherresultats.cpp" line="1021"/>
        <location filename="../src/interface/afficherresultats.cpp" line="1139"/>
        <source>nmi</source>
        <comment>nautical mile</comment>
        <translation>nmi</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="1063"/>
        <location filename="../src/interface/afficherresultats.cpp" line="1118"/>
        <source>T</source>
        <comment>transit</comment>
        <translation>通過</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="1063"/>
        <location filename="../src/interface/afficherresultats.cpp" line="1118"/>
        <source>C</source>
        <comment>conjunction</comment>
        <translation> 合 </translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="1064"/>
        <location filename="../src/interface/afficherresultats.cpp" line="1119"/>
        <source>S</source>
        <comment>Sun</comment>
        <translation>太陽</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="1064"/>
        <location filename="../src/interface/afficherresultats.cpp" line="1119"/>
        <source>L</source>
        <comment>Moon</comment>
        <translation> 月 </translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="1067"/>
        <location filename="../src/interface/afficherresultats.cpp" line="1122"/>
        <source>Lum</source>
        <comment>Lit</comment>
        <translation>照射</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="1069"/>
        <location filename="../src/interface/afficherresultats.cpp" line="1124"/>
        <source>Omb</source>
        <comment>Shadow</comment>
        <translation>本影</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="1073"/>
        <location filename="../src/interface/afficherresultats.cpp" line="1128"/>
        <source>Pen</source>
        <comment>Penumbra</comment>
        <translation>半影</translation>
    </message>
</context>
<context>
    <name>AjustementDates</name>
    <message>
        <location filename="../src/interface/onglets/telescope/ajustementdates.ui" line="29"/>
        <source>Ajuster les dates...</source>
        <translation>日時を調整する...</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/telescope/ajustementdates.ui" line="93"/>
        <source>Azimut (N)</source>
        <translation>方位角 (北)</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/telescope/ajustementdates.ui" line="112"/>
        <location filename="../src/interface/onglets/telescope/ajustementdates.ui" line="135"/>
        <source>dd/MM/yyyy HH:mm:ss</source>
        <translation>yyyy年MM月dd日 HH:mm:ss</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/telescope/ajustementdates.ui" line="142"/>
        <source>Date initiale :</source>
        <translation>開始日 :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/telescope/ajustementdates.ui" line="156"/>
        <source>Hauteur</source>
        <translation>仰角</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/telescope/ajustementdates.ui" line="76"/>
        <source>Date finale :</source>
        <translation>終了日 :</translation>
    </message>
</context>
<context>
    <name>Antenne</name>
    <message>
        <location filename="../src/interface/onglets/antenne/antenne.ui" line="41"/>
        <source>Adresse IP :</source>
        <translation>IP アドレス :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/antenne/antenne.ui" line="58"/>
        <source>Port :</source>
        <translation>ポート :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/antenne/antenne.ui" line="90"/>
        <source>Données transmises</source>
        <translation>送信されたデータ</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/antenne/antenne.ui" line="123"/>
        <source>Hauteur :</source>
        <translation>仰角 :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/antenne/antenne.ui" line="136"/>
        <source>Azimut (N) :</source>
        <extracomment>Azimuth (from the North)</extracomment>
        <translation>方位角 (北) :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/antenne/antenne.ui" line="149"/>
        <source>Variation distance :</source>
        <translation>レンジレート :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/antenne/antenne.ui" line="194"/>
        <source>Double-cliquer pour changer d&apos;unités</source>
        <translation>ダブルクリックして単位を変更します</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/antenne/antenne.ui" line="244"/>
        <source>Fréquence montante :</source>
        <translation>アップリンク :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/antenne/antenne.ui" line="254"/>
        <location filename="../src/interface/onglets/antenne/antenne.ui" line="401"/>
        <source>Fréquence réelle :</source>
        <translation>実際の周波数 :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/antenne/antenne.ui" line="271"/>
        <location filename="../src/interface/onglets/antenne/antenne.ui" line="418"/>
        <source>Doppler :</source>
        <extracomment>Doppler effect</extracomment>
        <translation>ドップラー :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/antenne/antenne.ui" line="288"/>
        <location filename="../src/interface/onglets/antenne/antenne.ui" line="469"/>
        <source>Balise :</source>
        <translation>ビーコン :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/antenne/antenne.ui" line="305"/>
        <location filename="../src/interface/onglets/antenne/antenne.ui" line="486"/>
        <source>Mode :</source>
        <extracomment>Beacon mode</extracomment>
        <translation>モード :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/antenne/antenne.ui" line="322"/>
        <location filename="../src/interface/onglets/antenne/antenne.ui" line="503"/>
        <source>Signal d&apos;appel :</source>
        <extracomment>Callsign</extracomment>
        <translation>コールサイン :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/antenne/antenne.ui" line="339"/>
        <location filename="../src/interface/onglets/antenne/antenne.ui" line="435"/>
        <source>Atténuation :</source>
        <extracomment>Free space path loss</extracomment>
        <translation>減衰 :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/antenne/antenne.ui" line="356"/>
        <location filename="../src/interface/onglets/antenne/antenne.ui" line="452"/>
        <source>Délai :</source>
        <translation>遅延 :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/antenne/antenne.ui" line="391"/>
        <source>Fréquence descendante :</source>
        <translation>ダウンリンク :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/antenne/antenne.ui" line="530"/>
        <location filename="../src/interface/onglets/antenne/antenne.cpp" line="456"/>
        <source>Connecter</source>
        <translation>接続する</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/antenne/antenne.ui" line="552"/>
        <source>Paramétrage par défaut</source>
        <translation>デフォルト設定</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/antenne/antenne.ui" line="565"/>
        <location filename="../src/interface/onglets/antenne/antenne.cpp" line="491"/>
        <source>Ouvrir CatRotator</source>
        <translation>CatRotatorを開く</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/antenne/antenne.ui" line="639"/>
        <source>Nom :</source>
        <translation>名称 :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/antenne/antenne.cpp" line="277"/>
        <source>h</source>
        <comment>hour</comment>
        <translation>h</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/antenne/antenne.cpp" line="277"/>
        <location filename="../src/interface/onglets/antenne/antenne.cpp" line="281"/>
        <source>min</source>
        <comment>minute</comment>
        <translation>min</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/antenne/antenne.cpp" line="281"/>
        <source>s</source>
        <comment>second</comment>
        <translation>s</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/antenne/antenne.cpp" line="284"/>
        <source>Prochain %1 dans %2</source>
        <comment>Next AOS or LOS, and delay</comment>
        <translation>次の%1まであと%2です</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/antenne/antenne.cpp" line="286"/>
        <source>AOS</source>
        <translation>AOS</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/antenne/antenne.cpp" line="286"/>
        <source>Acquisition du signal</source>
        <translation>信号受信開始</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/antenne/antenne.cpp" line="286"/>
        <source>Perte du signal</source>
        <translation>信号受信終了</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/antenne/antenne.cpp" line="412"/>
        <source>Déconnecter</source>
        <translation>切断する</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/antenne/antenne.cpp" line="442"/>
        <source>Connexion en cours...</source>
        <translation>接続中です...</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/antenne/antenne.cpp" line="491"/>
        <source>Fichiers exécutables (*.exe)</source>
        <translation>実行ファイル (*.exe)</translation>
    </message>
</context>
<context>
    <name>Apropos</name>
    <message>
        <location filename="../src/interface/apropos/apropos.ui" line="146"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../src/interface/apropos/apropos.ui" line="162"/>
        <source>PreviSat est un logiciel de suivi des satellites artificiels afin de les observer.

Il permet d&apos;afficher la position des satellites en temps réel ou en mode manuel. PreviSat est capable d&apos;effectuer les prévisions de passage, de calculer les flashs de certains satellites, ainsi que plusieurs autres calculs de prévisions.

PreviSat est entièrement gratuit!</source>
        <translation>PreviSatは、人工衛星を観測するための追跡ソフトです。

 衛星の位置をリアルタイムまたは手動モードで表示できます。それに衛星の通過やフレアなどの予測計算ができます。

PreviSatは、完全無料です!</translation>
    </message>
    <message>
        <location filename="../src/interface/apropos/apropos.ui" line="188"/>
        <source>Un immense merci à Michel Casabonne pour ses nombreuses suggestions pour PreviSat.</source>
        <translation>PreviSatに関する多く提案をしてくれたMichel Casabonneさんに心より感謝申し上げます。</translation>
    </message>
    <message>
        <location filename="../src/interface/apropos/apropos.ui" line="210"/>
        <source>Vérifier les mises à jour...</source>
        <translation>更新の確認...</translation>
    </message>
    <message>
        <location filename="../src/interface/apropos/apropos.cpp" line="110"/>
        <source>À propos de %1 %2</source>
        <translation>%1 %2について</translation>
    </message>
    <message>
        <location filename="../src/interface/apropos/apropos.cpp" line="129"/>
        <source>Version %1  (%2)</source>
        <translation>バージョン %1  (%2)</translation>
    </message>
    <message>
        <location filename="../src/interface/apropos/apropos.cpp" line="132"/>
        <source>d MMMM yyyy</source>
        <comment>Date format</comment>
        <translation>yyyy年MM月dd日</translation>
    </message>
    <message>
        <location filename="../src/interface/apropos/apropos.cpp" line="165"/>
        <source>Information</source>
        <translation>情報</translation>
    </message>
    <message>
        <location filename="../src/interface/apropos/apropos.cpp" line="165"/>
        <source>Vous utilisez la dernière version de %1</source>
        <translation>最新バージョンの「%1」を使用しています</translation>
    </message>
</context>
<context>
    <name>CalculsEvenementsOrbitaux</name>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsevenementsorbitaux.ui" line="68"/>
        <source>Évènements</source>
        <translation>事象</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsevenementsorbitaux.ui" line="83"/>
        <source>Passages aux noeuds</source>
        <translation>公点の通過</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsevenementsorbitaux.ui" line="102"/>
        <source>Passages à PSO = 90° et 270°</source>
        <translation>位置=90°又は180°の場合の通過</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsevenementsorbitaux.ui" line="121"/>
        <source>Passages apogée/périgée</source>
        <translation>遠地点・近地点の通過</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsevenementsorbitaux.ui" line="140"/>
        <source>Passages ombre/pénombre/lumière</source>
        <translation>本影・半影・太陽光の通過</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsevenementsorbitaux.ui" line="159"/>
        <source>Passages terminateur</source>
        <translation>明暗境界線の通過</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsevenementsorbitaux.ui" line="188"/>
        <source>Date finale :</source>
        <translation>終了日 :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsevenementsorbitaux.ui" line="207"/>
        <location filename="../src/interface/onglets/previsions/calculsevenementsorbitaux.ui" line="239"/>
        <source>dd/MM/yyyy HH:mm:ss</source>
        <extracomment>Date format</extracomment>
        <translation>yyyy年MM月dd日 HH:mm:ss</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsevenementsorbitaux.ui" line="220"/>
        <source>Date initiale :</source>
        <translation>開始日 :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsevenementsorbitaux.ui" line="258"/>
        <source>Effacer heures</source>
        <translation>時刻を消す</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsevenementsorbitaux.ui" line="277"/>
        <source>Calculs</source>
        <translation>計算</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsevenementsorbitaux.ui" line="296"/>
        <source>Paramétrage par défaut</source>
        <translation>デフォルト設定</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsevenementsorbitaux.ui" line="315"/>
        <source>Filtre</source>
        <translation>フィルター</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsevenementsorbitaux.ui" line="353"/>
        <source>Ages des éléments orbitaux : [</source>
        <translation>軌道要素の年齢 : [</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsevenementsorbitaux.ui" line="379"/>
        <source>] jours</source>
        <translation>] 日</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsevenementsorbitaux.ui" line="395"/>
        <source>Mettre à jour les éléments orbitaux...</source>
        <translation>軌道要素を更新する...</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsevenementsorbitaux.cpp" line="315"/>
        <source>Aucun</source>
        <translation>無し</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsevenementsorbitaux.cpp" line="318"/>
        <source>Tous</source>
        <translation>すべて</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsevenementsorbitaux.cpp" line="369"/>
        <source>Aucun satellite n&apos;est sélectionné dans la liste</source>
        <translation>リストで衛星が選択されていません</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsevenementsorbitaux.cpp" line="399"/>
        <source>km</source>
        <comment>kilometer</comment>
        <translation>km</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsevenementsorbitaux.cpp" line="399"/>
        <source>nmi</source>
        <comment>nautical mile</comment>
        <translation>nmi</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsevenementsorbitaux.cpp" line="434"/>
        <source>evenements</source>
        <comment>file name (without accent)</comment>
        <translation>events</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsevenementsorbitaux.cpp" line="444"/>
        <source>Calculs en cours...</source>
        <translation>進行中の計算...</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsevenementsorbitaux.cpp" line="445"/>
        <source>Annuler</source>
        <translation>キャンセル</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsevenementsorbitaux.cpp" line="468"/>
        <source>Calculs terminés</source>
        <translation>計算が完了しました</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsevenementsorbitaux.cpp" line="471"/>
        <source>Aucun évènement n&apos;a été trouvé sur la période donnée</source>
        <translation>指定された期間に事象が見つかりませんでした</translation>
    </message>
</context>
<context>
    <name>CalculsFlashs</name>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsflashs.ui" line="49"/>
        <source>Lieu d&apos;observation :</source>
        <translation>観測地 :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsflashs.ui" line="69"/>
        <source>Hauteur du Soleil :</source>
        <translation>太陽仰角 :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsflashs.ui" line="98"/>
        <source>Horizon (0°)</source>
        <translation>地平線 (0°)</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsflashs.ui" line="103"/>
        <source>Crépuscule civil (-6°)</source>
        <translation>市民薄明 (-6°)</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsflashs.ui" line="108"/>
        <source>Crépuscule nautique (-12°)</source>
        <translation>航海薄明 (-12°)</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsflashs.ui" line="113"/>
        <source>Crépuscule astronomique (-18°)</source>
        <translation>天文薄明 (-18°)</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsflashs.ui" line="118"/>
        <source>Indifférent</source>
        <extracomment>Pas de hauteur limite pour le Soleil : les calculs de jour sont inclus</extracomment>
        <translation>すべての仰角</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsflashs.ui" line="123"/>
        <location filename="../src/interface/onglets/previsions/calculsflashs.ui" line="398"/>
        <source>Autre...</source>
        <translation>その他...</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsflashs.ui" line="180"/>
        <source>Classer par ordre chronologique</source>
        <translation>時系列で分類する</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsflashs.ui" line="199"/>
        <source>Paramétrage par défaut</source>
        <translation>デフォルト設定</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsflashs.ui" line="224"/>
        <source>Date finale :</source>
        <translation>終了日 :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsflashs.ui" line="253"/>
        <location filename="../src/interface/onglets/previsions/calculsflashs.ui" line="285"/>
        <source>dd/MM/yyyy HH:mm:ss</source>
        <extracomment>Date format</extracomment>
        <translation>yyyy年MM月dd日 HH:mm:ss</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsflashs.ui" line="298"/>
        <source>Date initiale :</source>
        <translation>開始日 :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsflashs.ui" line="314"/>
        <source>Effacer heures</source>
        <translation>時刻を消す</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsflashs.ui" line="350"/>
        <source>Hauteur minimale du satellite :</source>
        <translation>最小衛星仰角 :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsflashs.ui" line="373"/>
        <source>0°</source>
        <translation>0°</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsflashs.ui" line="378"/>
        <source>5°</source>
        <translation>5°</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsflashs.ui" line="383"/>
        <source>10°</source>
        <translation>10°</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsflashs.ui" line="388"/>
        <source>15°</source>
        <translation>15°</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsflashs.ui" line="393"/>
        <source>20°</source>
        <translation>20°</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsflashs.ui" line="464"/>
        <source>Magnitude maximale :</source>
        <translation>最大等級 :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsflashs.ui" line="505"/>
        <source>Calculs</source>
        <translation>計算</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsflashs.ui" line="543"/>
        <source>Ages des éléments orbitaux : [</source>
        <translation>軌道要素の年齢 : [</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsflashs.ui" line="569"/>
        <source>] jours</source>
        <translation>] 日</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsflashs.ui" line="585"/>
        <source>Mettre à jour flares-spctrk.xml...</source>
        <translation>flares-spctrk.xmlを更新する...</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsflashs.cpp" line="348"/>
        <source>km</source>
        <comment>kilometer</comment>
        <translation>km</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsflashs.cpp" line="348"/>
        <source>nmi</source>
        <comment>nautical mile</comment>
        <translation>nmi</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsflashs.cpp" line="403"/>
        <source>Aucun satellite produisant des flashs n&apos;a été trouvé dans le fichier d&apos;éléments orbitaux</source>
        <translation>軌道要素ファイルにはフレアを生成する衛星が見つかりませんでした</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsflashs.cpp" line="437"/>
        <source>flashs</source>
        <comment>file name (without accent)</comment>
        <translation>flares</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsflashs.cpp" line="447"/>
        <source>Calculs en cours...</source>
        <translation>進行中の計算...</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsflashs.cpp" line="448"/>
        <source>Annuler</source>
        <translation>キャンセル</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsflashs.cpp" line="471"/>
        <source>Calculs terminés</source>
        <translation>計算が完了しました</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsflashs.cpp" line="475"/>
        <source>Aucun flash n&apos;a été trouvé sur la période donnée</source>
        <translation>指定された期間にフレアが見つかりませんでした</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsflashs.cpp" line="583"/>
        <source>Mise à jour du fichier GP %1 en cours...</source>
        <translation>GPファイル「%1」を更新しています...</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsflashs.cpp" line="588"/>
        <source>Téléchargement terminé</source>
        <translation>ダウンロードが完了しました</translation>
    </message>
</context>
<context>
    <name>CalculsPrevisions</name>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsprevisions.ui" line="53"/>
        <source>Illumination requise</source>
        <translation>必要な照射</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsprevisions.ui" line="75"/>
        <source>Magnitude maximale</source>
        <translation>最大等級</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsprevisions.ui" line="190"/>
        <location filename="../src/interface/onglets/previsions/calculsprevisions.ui" line="245"/>
        <source>dd/MM/yyyy HH:mm:ss</source>
        <extracomment>Date format</extracomment>
        <translation>yyyy年MM月dd日 HH:mm:ss</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsprevisions.ui" line="206"/>
        <source>Date finale :</source>
        <translation>終了日 :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsprevisions.ui" line="261"/>
        <source>Date initiale :</source>
        <translation>開始日 :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsprevisions.ui" line="280"/>
        <source>Effacer heures</source>
        <translation>時刻を消す</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsprevisions.ui" line="302"/>
        <source>Calculs</source>
        <translation>計算</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsprevisions.ui" line="338"/>
        <source>Pas de génération :</source>
        <translation>出力データ時間間隔 :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsprevisions.ui" line="351"/>
        <source>Hauteur du Soleil :</source>
        <translation>太陽仰角 :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsprevisions.ui" line="364"/>
        <source>Lieu d&apos;observation :</source>
        <translation>観測地 :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsprevisions.ui" line="393"/>
        <source>1 seconde</source>
        <translation>1秒</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsprevisions.ui" line="398"/>
        <source>5 secondes</source>
        <translation>5秒</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsprevisions.ui" line="403"/>
        <source>10 secondes</source>
        <translation>10秒</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsprevisions.ui" line="408"/>
        <source>20 secondes</source>
        <translation>20秒</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsprevisions.ui" line="413"/>
        <source>30 secondes</source>
        <translation>30秒</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsprevisions.ui" line="418"/>
        <source>1 minute</source>
        <translation>1分</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsprevisions.ui" line="423"/>
        <source>2 minutes</source>
        <translation>2分</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsprevisions.ui" line="428"/>
        <source>5 minutes</source>
        <translation>5分</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsprevisions.ui" line="480"/>
        <source>Horizon (0°)</source>
        <translation>地平線 (0°)</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsprevisions.ui" line="485"/>
        <source>Crépuscule civil (-6°)</source>
        <translation>市民薄明 (-6°)</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsprevisions.ui" line="490"/>
        <source>Crépuscule nautique (-12°)</source>
        <translation>航海薄明 (-12°)</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsprevisions.ui" line="495"/>
        <source>Crépuscule astronomique (-18°)</source>
        <translation>天文薄明 (-18°)</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsprevisions.ui" line="500"/>
        <source>Indifférent</source>
        <extracomment>Pas de hauteur limite pour le Soleil : les calculs de jour sont inclus</extracomment>
        <translation>すべての仰角</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsprevisions.ui" line="505"/>
        <location filename="../src/interface/onglets/previsions/calculsprevisions.ui" line="654"/>
        <source>Autre...</source>
        <translation>その他...</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsprevisions.ui" line="600"/>
        <source>Hauteur minimale du satellite :</source>
        <translation>最小衛星仰角 :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsprevisions.ui" line="629"/>
        <source>0°</source>
        <translation>0°</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsprevisions.ui" line="634"/>
        <source>5°</source>
        <translation>5°</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsprevisions.ui" line="639"/>
        <source>10°</source>
        <translation>10°</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsprevisions.ui" line="644"/>
        <source>15°</source>
        <translation>15°</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsprevisions.ui" line="649"/>
        <source>20°</source>
        <translation>20°</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsprevisions.ui" line="711"/>
        <source>Paramétrage par défaut</source>
        <translation>デフォルト設定</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsprevisions.ui" line="730"/>
        <source>Filtre</source>
        <translation>フィルター</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsprevisions.ui" line="768"/>
        <source>Ages des éléments orbitaux : [</source>
        <translation>軌道要素の年齢 : [</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsprevisions.ui" line="794"/>
        <source>] jours</source>
        <translation>] 日</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsprevisions.ui" line="810"/>
        <source>Mettre à jour les éléments orbitaux...</source>
        <translation>軌道要素を更新する...</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsprevisions.cpp" line="348"/>
        <source>Aucun</source>
        <translation>無し</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsprevisions.cpp" line="351"/>
        <source>Tous</source>
        <translation>すべて</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsprevisions.cpp" line="405"/>
        <source>Aucun satellite n&apos;est sélectionné dans la liste</source>
        <translation>リストで衛星が選択されていません</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsprevisions.cpp" line="446"/>
        <source>km</source>
        <comment>kilometer</comment>
        <translation>km</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsprevisions.cpp" line="446"/>
        <source>nmi</source>
        <comment>nautical mile</comment>
        <translation>nmi</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsprevisions.cpp" line="517"/>
        <source>previsions</source>
        <comment>filename (without accent)</comment>
        <translation>predictions</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsprevisions.cpp" line="527"/>
        <source>Calculs en cours...</source>
        <translation>進行中の計算...</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsprevisions.cpp" line="528"/>
        <source>Annuler</source>
        <translation>キャンセル</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsprevisions.cpp" line="551"/>
        <source>Calculs terminés</source>
        <translation>計算が完了しました</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsprevisions.cpp" line="555"/>
        <source>Aucun passage n&apos;a été trouvé sur la période donnée</source>
        <translation>指定された期間に通過が見つかりませんでした</translation>
    </message>
</context>
<context>
    <name>CalculsStarlink</name>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsstarlink.ui" line="35"/>
        <source>Groupe :</source>
        <extracomment>Starlink group</extracomment>
        <translation>グループ :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsstarlink.ui" line="278"/>
        <source>Hauteur minimale du satellite :</source>
        <translation>最小衛星仰角 :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsstarlink.ui" line="338"/>
        <source>0°</source>
        <translation>0°</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsstarlink.ui" line="343"/>
        <source>5°</source>
        <translation>5°</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsstarlink.ui" line="348"/>
        <source>10°</source>
        <translation>10°</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsstarlink.ui" line="353"/>
        <source>15°</source>
        <translation>15°</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsstarlink.ui" line="358"/>
        <source>20°</source>
        <translation>20°</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsstarlink.ui" line="363"/>
        <location filename="../src/interface/onglets/previsions/calculsstarlink.ui" line="468"/>
        <source>Autre...</source>
        <translation>その他...</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsstarlink.ui" line="72"/>
        <source>Paramétrage par défaut</source>
        <translation>デフォルト設定</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsstarlink.ui" line="91"/>
        <source>Calculs</source>
        <translation>計算</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsstarlink.ui" line="127"/>
        <source>Pas de génération :</source>
        <translation>出力データ時間間隔 :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsstarlink.ui" line="291"/>
        <source>Hauteur du Soleil :</source>
        <translation>太陽仰角 :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsstarlink.ui" line="140"/>
        <source>Lieu d&apos;observation :</source>
        <translation>観測地 :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsstarlink.ui" line="169"/>
        <source>1 seconde</source>
        <translation>1秒</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsstarlink.ui" line="174"/>
        <source>5 secondes</source>
        <translation>5秒</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsstarlink.ui" line="179"/>
        <source>10 secondes</source>
        <translation>10秒</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsstarlink.ui" line="184"/>
        <source>20 secondes</source>
        <translation>20秒</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsstarlink.ui" line="189"/>
        <source>30 secondes</source>
        <translation>30秒</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsstarlink.ui" line="194"/>
        <source>1 minute</source>
        <translation>1分</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsstarlink.ui" line="199"/>
        <source>2 minutes</source>
        <translation>2分</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsstarlink.ui" line="204"/>
        <source>5 minutes</source>
        <translation>5分</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsstarlink.ui" line="443"/>
        <source>Horizon (0°)</source>
        <translation>地平線 (0°)</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsstarlink.ui" line="448"/>
        <source>Crépuscule civil (-6°)</source>
        <translation>市民薄明 (-6°)</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsstarlink.ui" line="453"/>
        <source>Crépuscule nautique (-12°)</source>
        <translation>航海薄明 (-12°)</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsstarlink.ui" line="458"/>
        <source>Crépuscule astronomique (-18°)</source>
        <translation>天文薄明 (-18°)</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsstarlink.ui" line="463"/>
        <source>Indifférent</source>
        <extracomment>Pas de hauteur limite pour le Soleil : les calculs de jour sont inclus</extracomment>
        <translation>すべての仰角</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsstarlink.ui" line="578"/>
        <source>Ouvrir RocketLaunch.Live</source>
        <translation>RocketLaunch.Liveを開く</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsstarlink.ui" line="591"/>
        <source>Vérifier les éléments orbitaux disponibles...</source>
        <translation>利用可能な軌道要素を確認する...</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsstarlink.ui" line="212"/>
        <source>Nombre de jours de génération :</source>
        <translation>生成日数 :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsstarlink.ui" line="541"/>
        <source>Lancement :</source>
        <extracomment>Starlink launch</extracomment>
        <translation>打ち上げ :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsstarlink.ui" line="555"/>
        <source>Déploiement :</source>
        <extracomment>Starlink deployment</extracomment>
        <translation>展開 :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsstarlink.cpp" line="210"/>
        <location filename="../src/interface/onglets/previsions/calculsstarlink.cpp" line="211"/>
        <location filename="../src/interface/onglets/previsions/calculsstarlink.cpp" line="309"/>
        <location filename="../src/interface/onglets/previsions/calculsstarlink.cpp" line="310"/>
        <source>UTC</source>
        <translation>UTC</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsstarlink.cpp" line="215"/>
        <source>Aucun groupe Starlink trouvé</source>
        <translation>Starlinkグループが見つかりませんでした</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsstarlink.cpp" line="454"/>
        <source>km</source>
        <comment>kilometer</comment>
        <translation>km</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsstarlink.cpp" line="454"/>
        <source>nmi</source>
        <comment>nautical mile</comment>
        <translation>nmi</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsstarlink.cpp" line="523"/>
        <source>starlink</source>
        <comment>filename (without accent)</comment>
        <translation>starlink</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsstarlink.cpp" line="533"/>
        <source>Calculs en cours...</source>
        <translation>進行中の計算...</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsstarlink.cpp" line="534"/>
        <source>Annuler</source>
        <translation>キャンセル</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsstarlink.cpp" line="557"/>
        <source>Calculs terminés</source>
        <translation>計算が完了しました</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsstarlink.cpp" line="561"/>
        <source>Aucun passage n&apos;a été trouvé sur la période donnée</source>
        <translation>指定された期間に通過が見つかりませんでした</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsstarlink.cpp" line="588"/>
        <source>Mise à jour des éléments orbitaux...</source>
        <translation>軌道要素を更新しています...</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsstarlink.cpp" line="594"/>
        <source>Téléchargement terminé</source>
        <translation>ダウンロードが完了しました</translation>
    </message>
</context>
<context>
    <name>CalculsTransits</name>
    <message>
        <location filename="../src/interface/onglets/previsions/calculstransits.ui" line="32"/>
        <source>Inclure transits/conjonctions lunaires de jour</source>
        <translation>昼間の月面通過・月との合を含む</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculstransits.ui" line="63"/>
        <source>Élongation maximale avec le corps :</source>
        <translation>天体からの最大離角 :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculstransits.ui" line="107"/>
        <source>Calculs</source>
        <translation>計算</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculstransits.ui" line="143"/>
        <source>Hauteur minimale du satellite :</source>
        <translation>最小衛星仰角 :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculstransits.ui" line="160"/>
        <source>0°</source>
        <translation>0°</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculstransits.ui" line="165"/>
        <source>5°</source>
        <translation>5°</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculstransits.ui" line="170"/>
        <source>10°</source>
        <translation>10°</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculstransits.ui" line="175"/>
        <source>15°</source>
        <translation>15°</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculstransits.ui" line="180"/>
        <source>20°</source>
        <translation>20°</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculstransits.ui" line="185"/>
        <source>Autre...</source>
        <translation>その他...</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculstransits.ui" line="248"/>
        <source>Date initiale :</source>
        <translation>開始日 :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculstransits.ui" line="258"/>
        <source>Date finale :</source>
        <translation>終了日 :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculstransits.ui" line="277"/>
        <location filename="../src/interface/onglets/previsions/calculstransits.ui" line="299"/>
        <source>dd/MM/yyyy HH:mm:ss</source>
        <extracomment>Date format</extracomment>
        <translation>yyyy年MM月dd日 HH:mm:ss</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculstransits.ui" line="318"/>
        <source>Effacer heures</source>
        <translation>時刻を消す</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculstransits.ui" line="334"/>
        <source>Mettre à jour Éléments orbitaux de l&apos;ISS...</source>
        <translation>ISSの軌道要素を更新する...</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculstransits.ui" line="544"/>
        <source>Ages des éléments orbitaux : [</source>
        <translation>軌道要素の年齢 : [</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculstransits.ui" line="570"/>
        <source>] jours</source>
        <translation>] 日</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculstransits.ui" line="586"/>
        <source>Mettre à jour...</source>
        <translation>更新する...</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculstransits.ui" line="353"/>
        <source>Corps</source>
        <translation>天体</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculstransits.ui" line="368"/>
        <source>Soleil</source>
        <translation>太陽</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculstransits.ui" line="387"/>
        <source>Lune</source>
        <translation>月</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculstransits.ui" line="419"/>
        <source>Lieu d&apos;observation :</source>
        <translation>観測地 :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculstransits.ui" line="454"/>
        <source>Paramétrage par défaut</source>
        <translation>デフォルト設定</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculstransits.ui" line="506"/>
        <source>Filtre</source>
        <translation>フィルター</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculstransits.cpp" line="351"/>
        <source>Les éléments orbitaux de l&apos;ISS sont vieux, il est conseillé de les mettre à jour</source>
        <translation>ISSの軌道要素は古くなっているため更新することをお勧めします</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculstransits.cpp" line="389"/>
        <source>Aucun</source>
        <translation>無し</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculstransits.cpp" line="392"/>
        <source>Tous</source>
        <translation>すべて</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculstransits.cpp" line="470"/>
        <source>km</source>
        <comment>kilometer</comment>
        <translation>km</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculstransits.cpp" line="470"/>
        <source>nmi</source>
        <comment>nautical mile</comment>
        <translation>nmi</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculstransits.cpp" line="526"/>
        <source>transits</source>
        <comment>file name (without accent)</comment>
        <translation>transits</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculstransits.cpp" line="536"/>
        <source>Calculs en cours...</source>
        <translation>進行中の計算...</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculstransits.cpp" line="537"/>
        <source>Annuler</source>
        <translation>キャンセル</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculstransits.cpp" line="560"/>
        <source>Calculs terminés</source>
        <translation>計算が完了しました</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculstransits.cpp" line="563"/>
        <source>Aucun transit ISS n&apos;a été trouvé sur la période donnée</source>
        <translation>指定された期間にISSの通過が見つかりませんでした</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculstransits.cpp" line="692"/>
        <source>Téléchargement du fichier d&apos;élements orbitaux de l&apos;ISS...</source>
        <translation>ISSの軌道要素ファイルのダウンロード...</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculstransits.cpp" line="696"/>
        <source>Téléchargement terminé</source>
        <translation>ダウンロードが完了しました</translation>
    </message>
</context>
<context>
    <name>Carte</name>
    <message>
        <location filename="../src/interface/carte/carte.cpp" line="150"/>
        <source>Est</source>
        <translation>東</translation>
    </message>
    <message>
        <location filename="../src/interface/carte/carte.cpp" line="150"/>
        <source>Ouest</source>
        <translation>西</translation>
    </message>
    <message>
        <location filename="../src/interface/carte/carte.cpp" line="154"/>
        <source>Sud</source>
        <translation>南</translation>
    </message>
    <message>
        <location filename="../src/interface/carte/carte.cpp" line="154"/>
        <source>Nord</source>
        <translation>北</translation>
    </message>
    <message>
        <location filename="../src/interface/carte/carte.cpp" line="157"/>
        <source>Longitude : %1° %2</source>
        <translation>経度 : %1° %2</translation>
    </message>
    <message>
        <location filename="../src/interface/carte/carte.cpp" line="158"/>
        <source>Latitude : %1° %2</source>
        <translation>緯度 : %1° %2</translation>
    </message>
    <message>
        <location filename="../src/interface/carte/carte.cpp" line="177"/>
        <source>&lt;font color=&apos;blue&apos;&gt;&lt;b&gt;%1&lt;/b&gt;&lt;/font&gt;&lt;br /&gt;NORAD : &lt;b&gt;%2&lt;/b&gt;&lt;br /&gt;COSPAR : &lt;b&gt;%3&lt;/b&gt;</source>
        <translation>&lt;font color=&apos;blue&apos;&gt;&lt;b&gt;%1&lt;/b&gt;&lt;/font&gt;&lt;br /&gt;NORAD : &lt;b&gt;%2&lt;/b&gt;&lt;br /&gt;COSPAR : &lt;b&gt;%3&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../src/interface/carte/carte.cpp" line="180"/>
        <source>&lt;b&gt;%1&lt;/b&gt; (numéro NORAD : &lt;b&gt;%2&lt;/b&gt;  -  COSPAR : &lt;b&gt;%3&lt;/b&gt;)</source>
        <translation>&lt;b&gt;%1&lt;/b&gt; (NORAD番号 : &lt;b&gt;%2&lt;/b&gt;  -  COSPAR : &lt;b&gt;%3&lt;/b&gt;)</translation>
    </message>
    <message>
        <location filename="../src/interface/carte/carte.cpp" line="205"/>
        <location filename="../src/interface/carte/carte.cpp" line="206"/>
        <source>Soleil</source>
        <translation>太陽</translation>
    </message>
    <message>
        <location filename="../src/interface/carte/carte.cpp" line="231"/>
        <location filename="../src/interface/carte/carte.cpp" line="232"/>
        <source>Lune</source>
        <translation>月</translation>
    </message>
    <message>
        <location filename="../src/interface/carte/carte.cpp" line="1446"/>
        <source>W</source>
        <comment>Symbol for West</comment>
        <translation>西</translation>
    </message>
    <message>
        <location filename="../src/interface/carte/carte.cpp" line="1452"/>
        <source>E</source>
        <comment>Symbol for East</comment>
        <translation>東</translation>
    </message>
    <message>
        <location filename="../src/interface/carte/carte.cpp" line="1480"/>
        <source>N</source>
        <comment>Symbol for North</comment>
        <translation>北</translation>
    </message>
    <message>
        <location filename="../src/interface/carte/carte.cpp" line="1486"/>
        <source>S</source>
        <comment>Symbol for South</comment>
        <translation>南</translation>
    </message>
    <message>
        <location filename="../src/interface/carte/carte.cpp" line="1782"/>
        <source>Le fichier %1 n&apos;existe pas, veuillez réinstaller %2</source>
        <translation>ファイル「%1」が存在しないので、「%2」を再インストールしてください</translation>
    </message>
</context>
<context>
    <name>Ciel</name>
    <message>
        <location filename="../src/interface/ciel/ciel.ui" line="70"/>
        <source>Nord</source>
        <translation>北</translation>
    </message>
    <message>
        <location filename="../src/interface/ciel/ciel.ui" line="91"/>
        <source>Est</source>
        <translation>東</translation>
    </message>
    <message>
        <location filename="../src/interface/ciel/ciel.ui" line="132"/>
        <source>Ouest</source>
        <translation>西</translation>
    </message>
    <message>
        <location filename="../src/interface/ciel/ciel.ui" line="156"/>
        <source>Sud</source>
        <translation>南</translation>
    </message>
    <message>
        <location filename="../src/interface/ciel/ciel.cpp" line="1069"/>
        <source>Flash %1</source>
        <translation>フレア %1</translation>
    </message>
    <message>
        <location filename="../src/interface/ciel/ciel.cpp" line="387"/>
        <source>Ascension droite : %1</source>
        <translation>赤経 : %1</translation>
    </message>
    <message>
        <location filename="../src/interface/ciel/ciel.cpp" line="389"/>
        <source>Déclinaison : %1</source>
        <translation>赤緯 : %1</translation>
    </message>
    <message>
        <location filename="../src/interface/ciel/ciel.cpp" line="406"/>
        <source>&lt;font color=&apos;blue&apos;&gt;&lt;b&gt;%1&lt;/b&gt;&lt;/font&gt;&lt;br /&gt;NORAD : &lt;b&gt;%2&lt;/b&gt;&lt;br /&gt;COSPAR : &lt;b&gt;%3&lt;/b&gt;</source>
        <translation>&lt;font color=&apos;blue&apos;&gt;&lt;b&gt;%1&lt;/b&gt;&lt;/font&gt;&lt;br /&gt;NORAD : &lt;b&gt;%2&lt;/b&gt;&lt;br /&gt;COSPAR : &lt;b&gt;%3&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../src/interface/ciel/ciel.cpp" line="409"/>
        <source>&lt;b&gt;%1&lt;/b&gt; (numéro NORAD : &lt;b&gt;%2&lt;/b&gt;  -  COSPAR : &lt;b&gt;%3&lt;/b&gt;)</source>
        <translation>&lt;b&gt;%1&lt;/b&gt; (NORAD番号 : &lt;b&gt;%2&lt;/b&gt;  -  COSPAR : &lt;b&gt;%3&lt;/b&gt;)</translation>
    </message>
    <message>
        <location filename="../src/interface/ciel/ciel.cpp" line="468"/>
        <location filename="../src/interface/ciel/ciel.cpp" line="469"/>
        <source>Soleil</source>
        <translation>太陽</translation>
    </message>
    <message>
        <location filename="../src/interface/ciel/ciel.cpp" line="495"/>
        <location filename="../src/interface/ciel/ciel.cpp" line="496"/>
        <source>Lune</source>
        <translation>月</translation>
    </message>
</context>
<context>
    <name>General</name>
    <message>
        <location filename="../src/interface/onglets/general/general.ui" line="84"/>
        <location filename="../src/interface/onglets/general/general.ui" line="139"/>
        <source>Date :</source>
        <extracomment>Date and hour</extracomment>
        <translation>日時 :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.ui" line="152"/>
        <source>dddd dd MMMM yyyy HH:mm:ss</source>
        <translation>yyyy年MM月dd日 (dddd)  HH:mm:ss</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.ui" line="189"/>
        <location filename="../src/interface/onglets/general/general.cpp" line="842"/>
        <source>Lieu d&apos;observation :</source>
        <translation>観測地 :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.ui" line="226"/>
        <location filename="../src/interface/onglets/general/general.ui" line="529"/>
        <location filename="../src/interface/onglets/general/general.ui" line="1824"/>
        <source>Longitude :</source>
        <translation>経度 :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.ui" line="258"/>
        <location filename="../src/interface/onglets/general/general.ui" line="555"/>
        <location filename="../src/interface/onglets/general/general.ui" line="1850"/>
        <source>Latitude :</source>
        <translation>緯度 :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.ui" line="290"/>
        <source>Altitude :</source>
        <extracomment>altitude of observer</extracomment>
        <translation>高さ :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.ui" line="328"/>
        <source>Conditions :</source>
        <extracomment>Conditions of observations</extracomment>
        <translation>状況 :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.ui" line="335"/>
        <source>Crépuscule astronomique</source>
        <translation>天文薄明</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.ui" line="495"/>
        <location filename="../src/interface/onglets/general/general.cpp" line="893"/>
        <source>Coordonnées du Soleil :</source>
        <translation>太陽の座標 :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.ui" line="388"/>
        <location filename="../src/interface/onglets/general/general.ui" line="777"/>
        <location filename="../src/interface/onglets/general/general.ui" line="1932"/>
        <source>Ascension droite :</source>
        <translation>赤経 :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.ui" line="401"/>
        <location filename="../src/interface/onglets/general/general.ui" line="790"/>
        <location filename="../src/interface/onglets/general/general.ui" line="1945"/>
        <source>Déclinaison :</source>
        <translation>赤緯 :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.ui" line="414"/>
        <location filename="../src/interface/onglets/general/general.ui" line="803"/>
        <location filename="../src/interface/onglets/general/general.ui" line="1958"/>
        <source>Constellation :</source>
        <translation>星座 :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.ui" line="640"/>
        <location filename="../src/interface/onglets/general/general.ui" line="885"/>
        <location filename="../src/interface/onglets/general/general.ui" line="1713"/>
        <source>Hauteur :</source>
        <translation>仰角 :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.ui" line="653"/>
        <location filename="../src/interface/onglets/general/general.ui" line="898"/>
        <location filename="../src/interface/onglets/general/general.ui" line="1726"/>
        <source>Azimut (N) :</source>
        <extracomment>Azimuth (from the North)</extracomment>
        <translation>方位角 (北) :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.ui" line="666"/>
        <location filename="../src/interface/onglets/general/general.ui" line="911"/>
        <location filename="../src/interface/onglets/general/general.ui" line="1739"/>
        <source>Distance :</source>
        <translation>距離 :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.ui" line="743"/>
        <location filename="../src/interface/onglets/general/general.cpp" line="920"/>
        <source>Coordonnées de la Lune :</source>
        <translation>月の座標 :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.ui" line="996"/>
        <source>Phase :</source>
        <extracomment>Moon phase</extracomment>
        <translation>月相 :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.ui" line="1012"/>
        <source>Magn. (Illum.) :</source>
        <extracomment>Magnitude, illumination</extracomment>
        <translation>等級 (照射) :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.ui" line="1103"/>
        <source>Aube astronomique :</source>
        <translation>天文明け方 :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.ui" line="1116"/>
        <source>Aube nautique :</source>
        <translation>航海明け方 :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.ui" line="1129"/>
        <source>Aube civile :</source>
        <translation>市民明け方 :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.ui" line="1303"/>
        <location filename="../src/interface/onglets/general/general.cpp" line="906"/>
        <source>Évènements Soleil :</source>
        <translation>太陽関連のイベント :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.ui" line="1337"/>
        <source>Lever :</source>
        <comment>Sun</comment>
        <translation>日の出 :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.ui" line="1350"/>
        <source>Méridien :</source>
        <comment>Sun</comment>
        <translation>子午線越え :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.ui" line="1363"/>
        <source>Coucher :</source>
        <comment>Sun</comment>
        <translation>日の入り :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.ui" line="1202"/>
        <source>Crépuscule civil :</source>
        <translation>市民薄暮 :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.ui" line="587"/>
        <source>Diam. apparent :</source>
        <extracomment>Angular diameter</extracomment>
        <translation>見かけの直径 :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.ui" line="1009"/>
        <source>Magnitude (Illumination)</source>
        <translation>等級 (照射)</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.ui" line="584"/>
        <location filename="../src/interface/onglets/general/general.ui" line="1025"/>
        <source>Diamètre apparent</source>
        <translation>見かけの直径</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.ui" line="1028"/>
        <source>Diam. app. :</source>
        <extracomment>Angular diameter</extracomment>
        <translation>見かけの直径 :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.ui" line="1231"/>
        <source>Crépuscule nautique :</source>
        <translation>航海薄暮 :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.ui" line="1260"/>
        <source>Crépuscule astronomique :</source>
        <translation>天文薄暮 :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.ui" line="1437"/>
        <location filename="../src/interface/onglets/general/general.cpp" line="931"/>
        <source>Évènements Lune :</source>
        <translation>月関連のイベント :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.ui" line="1471"/>
        <source>Lever :</source>
        <comment>Moon</comment>
        <translation>月の出 :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.ui" line="1484"/>
        <source>Méridien :</source>
        <comment>Moon</comment>
        <translation>子午線越え :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.ui" line="1497"/>
        <source>Coucher :</source>
        <comment>Moon</comment>
        <translation>月の入り :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.ui" line="1567"/>
        <source>Nouvelle Lune :</source>
        <translation>ニュームーン :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.ui" line="1581"/>
        <source>Premier quartier :</source>
        <translation>ファーストクォーター :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.ui" line="1595"/>
        <source>Pleine Lune :</source>
        <translation>フルムーン :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.ui" line="1609"/>
        <source>Dernier quartier :</source>
        <translation>サードクォーター :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.ui" line="1879"/>
        <source>Altitude :</source>
        <comment>Altitude of satellite</comment>
        <translation>高度 :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.ui" line="2049"/>
        <source>Direction :</source>
        <translation>方向 :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.ui" line="2062"/>
        <source>Vitesse orbitale :</source>
        <translation>軌道速度 :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.ui" line="2075"/>
        <source>Variation distance :</source>
        <translation>レンジレート :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.ui" line="2107"/>
        <location filename="../src/interface/onglets/general/general.ui" line="2129"/>
        <source>Double-cliquer pour changer d&apos;unités</source>
        <translation>ダブルクリックして単位を変更します</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.ui" line="2182"/>
        <location filename="../src/interface/onglets/general/general.ui" line="2321"/>
        <source>Orbite n°</source>
        <translation>軌道 #</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.ui" line="2419"/>
        <source>Temps écoulé depuis l&apos;époque :</source>
        <translation>元期からの経過時間 :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.ui" line="2451"/>
        <source>Nom :</source>
        <translation>名称 :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="64"/>
        <source>Coordonnées du Soleil</source>
        <translation>太陽の座標</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="65"/>
        <source>Coordonnées de la Lune</source>
        <translation>月の座標</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="66"/>
        <source>Évènements Soleil</source>
        <translation>太陽関連のイベント</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="67"/>
        <source>Évènements Lune</source>
        <translation>月関連のイベント</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="240"/>
        <source>UTC %1 %2</source>
        <comment>Universal Time Coordinated</comment>
        <translation>UTC %1 %2</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="243"/>
        <source>UTC</source>
        <comment>Universal Time Coordinated</comment>
        <translation>UTC</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="267"/>
        <location filename="../src/interface/onglets/general/general.cpp" line="624"/>
        <source>km</source>
        <comment>Kilometer</comment>
        <translation>km</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="267"/>
        <location filename="../src/interface/onglets/general/general.cpp" line="624"/>
        <source>nmi</source>
        <comment>nautical mile</comment>
        <translation>nmi</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="291"/>
        <source>jours</source>
        <translation>日</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="294"/>
        <location filename="../src/interface/onglets/general/general.cpp" line="594"/>
        <location filename="../src/interface/onglets/general/general.cpp" line="791"/>
        <source>Ouest</source>
        <translation>西</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="294"/>
        <location filename="../src/interface/onglets/general/general.cpp" line="594"/>
        <location filename="../src/interface/onglets/general/general.cpp" line="791"/>
        <source>Est</source>
        <translation>東</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="296"/>
        <location filename="../src/interface/onglets/general/general.cpp" line="596"/>
        <location filename="../src/interface/onglets/general/general.cpp" line="792"/>
        <source>Nord</source>
        <translation>北</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="296"/>
        <location filename="../src/interface/onglets/general/general.cpp" line="596"/>
        <location filename="../src/interface/onglets/general/general.cpp" line="792"/>
        <source>Sud</source>
        <translation>南</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="319"/>
        <source>Ascendant</source>
        <translation>上昇軌道</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="319"/>
        <source>Descendant</source>
        <translation>下降軌道</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="323"/>
        <source>Orbite n°%1</source>
        <translation>軌道 #%1</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="329"/>
        <source>Lune</source>
        <translation>月</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="343"/>
        <location filename="../src/interface/onglets/general/general.cpp" line="661"/>
        <source>Satellite en éclipse totale%1</source>
        <translation>現在、衛星の皆既食中です%1</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="347"/>
        <source>Magnitude (Illumination) : %1 (%2%)</source>
        <translation>等級 (照射) : %1 (%2%)</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="406"/>
        <source>S</source>
        <comment>Sun</comment>
        <translation>太陽</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="406"/>
        <source>L</source>
        <comment>Moon</comment>
        <translation>月</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="409"/>
        <source>P</source>
        <comment>partial eclipse</comment>
        <translation>部分食</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="413"/>
        <source>A</source>
        <comment>annular eclipse</comment>
        <translation>金環食</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="440"/>
        <location filename="../src/interface/onglets/general/general.cpp" line="489"/>
        <source>Prochain %1 :</source>
        <translation>次の%1 :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="442"/>
        <source>N&gt;J</source>
        <comment>Night to day</comment>
        <translation>「夜&gt;昼」</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="442"/>
        <source>J&gt;N</source>
        <comment>Day to night</comment>
        <translation>「昼&gt;夜」</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="443"/>
        <source>Nuit &gt; Jour</source>
        <translation>夜間 &gt; 昼間</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="443"/>
        <source>Jour &gt; Nuit</source>
        <translation>昼間 &gt; 夜間</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="458"/>
        <location filename="../src/interface/onglets/general/general.cpp" line="511"/>
        <source>h</source>
        <comment>hour</comment>
        <translation>h</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="458"/>
        <location filename="../src/interface/onglets/general/general.cpp" line="462"/>
        <location filename="../src/interface/onglets/general/general.cpp" line="511"/>
        <location filename="../src/interface/onglets/general/general.cpp" line="515"/>
        <source>min</source>
        <comment>minute</comment>
        <translation>min</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="462"/>
        <location filename="../src/interface/onglets/general/general.cpp" line="515"/>
        <source>s</source>
        <comment>second</comment>
        <translation>s</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="492"/>
        <source>AOS</source>
        <translation>AOS</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="492"/>
        <source>Acquisition du signal</source>
        <translation>信号受信開始</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="492"/>
        <source>Perte du signal</source>
        <translation>信号受信終了</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="446"/>
        <location filename="../src/interface/onglets/general/general.cpp" line="496"/>
        <source>%1  (dans %2).</source>
        <comment>Delay in hours, minutes or seconds</comment>
        <translation>%1  (あと%2で).</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="525"/>
        <source>Azimut : %1</source>
        <translation>方位角 : %1</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="541"/>
        <source>Beta : %1</source>
        <comment>Beta angle (angle between orbit plane and direction of Sun)</comment>
        <translation>ベータ角 : %1</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="575"/>
        <source>Nuit</source>
        <translation>夜間</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="586"/>
        <source>%1 UA</source>
        <translation>%1 AU</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="664"/>
        <source>Satellite non éclipsé</source>
        <translation>現在、衛星に太陽光が当たっている</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="666"/>
        <source>Satellite en éclipse partielle%1</source>
        <translation>現在、衛星の部分食中です%1</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="669"/>
        <source>Satellite en éclipse annulaire%1</source>
        <translation>現在、衛星の金環食中です%1</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="705"/>
        <source>km/h</source>
        <comment>Kilometer per hour</comment>
        <translation>km/h</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="705"/>
        <source>km/s</source>
        <comment>Kilometer per second</comment>
        <translation>km/s</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="707"/>
        <source>kn</source>
        <comment>Knot</comment>
        <translation>kn</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="707"/>
        <source>nmi/s</source>
        <comment>Nautical mile per second</comment>
        <translation>nmi/s</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="800"/>
        <source>m</source>
        <comment>meter</comment>
        <translation>m</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="800"/>
        <source>ft</source>
        <comment>foot</comment>
        <translation>ft</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="828"/>
        <source>Problème de droits d&apos;écriture du fichier %1</source>
        <translation>ファイル「%1」の書き込み権限に問題があります</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="840"/>
        <source>Date :</source>
        <comment>Date and hour</comment>
        <translation>日時 :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="843"/>
        <source>Longitude  : %1	Latitude : %2	Altitude : %3</source>
        <comment>Observer coordinates</comment>
        <translation>経度   : %1	緯度 : %2	高さ : %3</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="845"/>
        <source>Conditions : %1</source>
        <comment>Conditions of observation</comment>
        <translation>状況   : %1</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="853"/>
        <source>Nom du satellite :</source>
        <translation>衛星名 :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="859"/>
        <source>Latitude  :  %1		Azimut (N) : %2	Déclinaison      : %3</source>
        <translation>緯度  :  %1		方位角 (北) : %2	赤緯 : %3</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="863"/>
        <source>Altitude  :  %1		Distance   : %2	Constellation    : %3</source>
        <comment>Altitude of satellite</comment>
        <translation>高度  :  %1		距離        : %2	星座 : %3</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="867"/>
        <source>Direction          : %1  	%2      		%3</source>
        <translation>方向     : %1      		%2      		%3</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="881"/>
        <source>Variation distance : %1  	%2</source>
        <comment>Range rate</comment>
        <translation>レンジレート : %1  		%2</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="894"/>
        <source>Hauteur    : %1		Ascension droite  :  %2 	Longitude                : %3</source>
        <translation>仰角        : %1	赤経 :  %2 		経度       : %3</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="898"/>
        <source>Azimut (N) : %1		Déclinaison       : %2 	Latitude                 : %3</source>
        <comment>Azimuth from the North</comment>
        <translation>方位角 (北) : %1 	赤緯 : %2 		緯度       : %3</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="902"/>
        <source>Distance   : %1   		Constellation     : %2			Diamètre apparent        : %3</source>
        <translation>距離        : %1   	星座 : %2			見かけの直径 : %3</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="907"/>
        <source>Lever    : %1			Aube astronomique : %2		Crépuscule civil         : %3</source>
        <comment>Sunrise</comment>
        <translation>日の出    : %1			天文明け方 : %2		市民薄暮 : %3</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="910"/>
        <source>Méridien : %1			Aube nautique     : %2		Crépuscule nautique      : %3</source>
        <comment>Meridian pass for the Sun</comment>
        <translation>子午線越え : %1		航海明け方 : %2		航海薄暮 : %3</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="914"/>
        <source>Coucher  : %1			Aube civile       : %2		Crépuscule astronomique  : %3</source>
        <comment>Sunset</comment>
        <translation>日の入り   : %1			市民明け方 : %2		天文薄暮 : %3</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="921"/>
        <source>Hauteur    : %1		Ascension droite :  %2 	Phase                    : %3</source>
        <comment>Moon phase</comment>
        <translation>仰角        : %1	赤経 :  %2 		月相        : %3</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="924"/>
        <source>Azimut (N) : %1		Déclinaison      : %2 	Magnitude (Illumination) : %3</source>
        <comment>Azimuth from the North</comment>
        <translation>方位角 (北) : %1 	赤緯 : %2 		等級 (照射) : %3</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="927"/>
        <source>Distance   : %1  		Constellation    : %2 			Diamètre apparent        : %3</source>
        <translation>距離        : %1   	星座 : %2			見かけの直径 : %3</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="932"/>
        <source>Lever    : %1</source>
        <comment>Moonrise</comment>
        <translation>月の出     : %1</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="933"/>
        <source>Méridien : %1</source>
        <comment>Meridian pass for the Moon</comment>
        <translation>子午線越え : %1</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="934"/>
        <source>Coucher  : %1</source>
        <comment>Moonset</comment>
        <translation>月の入り    : %1</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="567"/>
        <source>Jour</source>
        <comment>Sun is above horizon</comment>
        <translation>昼間</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="569"/>
        <source>Crépuscule civil</source>
        <comment>Sun is 6 degrees below horizon</comment>
        <translation>市民薄明</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="571"/>
        <source>Crépuscule nautique</source>
        <comment>Sun is 12 degrees below horizon</comment>
        <translation>航海薄明</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="573"/>
        <source>Crépuscule astronomique</source>
        <comment>Sun is 18 degrees below horizon</comment>
        <translation>天文薄明</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="855"/>
        <source>Longitude : %1  	Hauteur    : %2	Ascension droite :  %3</source>
        <translation>経度  : %1		仰角        : %2	赤経 :  %3</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="871"/>
        <source>Vitesse orbitale   : %1	%2  %3</source>
        <translation>軌道速度 : %1		%2 %3</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="936"/>
        <source>Nouvelle Lune    : %1</source>
        <translation>ニュームーン     : %1</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="937"/>
        <source>Premier quartier : %1</source>
        <translation>ファーストクォーター : %1</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="938"/>
        <source>Pleine Lune      : %1</source>
        <translation>フルムーン      : %1</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="939"/>
        <source>Dernier quartier : %1</source>
        <translation>サードクォーター   : %1</translation>
    </message>
</context>
<context>
    <name>Informations</name>
    <message>
        <location filename="../src/interface/informations/informations.ui" line="29"/>
        <source>Informations</source>
        <translation>情報</translation>
    </message>
    <message>
        <location filename="../src/interface/informations/informations.ui" line="45"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../src/interface/informations/informations.ui" line="71"/>
        <source>Dernière Information</source>
        <translation>最新情報</translation>
    </message>
    <message>
        <location filename="../src/interface/informations/informations.ui" line="91"/>
        <source>Informations plus anciennes</source>
        <translation>古い情報</translation>
    </message>
    <message>
        <location filename="../src/interface/informations/informations.ui" line="124"/>
        <source>Versions</source>
        <translation>バージョン</translation>
    </message>
    <message>
        <location filename="../src/interface/informations/informations.ui" line="166"/>
        <source>Afficher les informations au démarrage</source>
        <translation>起動時に情報を表示する</translation>
    </message>
</context>
<context>
    <name>InformationsISS</name>
    <message>
        <location filename="../src/interface/onglets/donnees/informationsiss.ui" line="68"/>
        <source>Évènement</source>
        <translation>事象</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/informationsiss.ui" line="78"/>
        <source>Date</source>
        <extracomment>Date and hour</extracomment>
        <translation>日時</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/informationsiss.ui" line="88"/>
        <source>ΔV</source>
        <translation>ΔV</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/informationsiss.ui" line="98"/>
        <source>Apogée</source>
        <translation>遠地点</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/informationsiss.ui" line="108"/>
        <source>Périgée</source>
        <translation>近地点</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/informationsiss.ui" line="127"/>
        <source>Mettre à jour les informations de l&apos;ISS...</source>
        <translation>ISSの情報を更新する...</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/informationsiss.ui" line="149"/>
        <source>Masse :</source>
        <translation>質量 :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/informationsiss.ui" line="163"/>
        <source>Surface de traînée :</source>
        <translation>抗力面積 :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/informationsiss.ui" line="189"/>
        <source>Coefficient de traînée :</source>
        <translation>抗力係数 :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/informationsiss.ui" line="212"/>
        <source>Fichier d&apos;informations ISS absent :
cliquer sur &apos;Mettre à jour les informations de l&apos;ISS&apos;</source>
        <translation>ISS情報ファイルがありません :
「ISSの情報を更新する」をクリックしてください</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/informationsiss.cpp" line="135"/>
        <source>km</source>
        <comment>Kilometer</comment>
        <translation>km</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/informationsiss.cpp" line="136"/>
        <source>m/s</source>
        <comment>meter per second</comment>
        <translation>m/s</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/informationsiss.cpp" line="137"/>
        <source>kg</source>
        <comment>Kilogram</comment>
        <translation>kg</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/informationsiss.cpp" line="138"/>
        <source>m^2</source>
        <comment>meter square</comment>
        <translation>m^2</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/informationsiss.cpp" line="145"/>
        <source>nmi</source>
        <comment>nautical mile</comment>
        <translation>nmi</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/informationsiss.cpp" line="146"/>
        <source>ft/s</source>
        <comment>foot per second</comment>
        <translation>ft/s</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/informationsiss.cpp" line="147"/>
        <source>lb</source>
        <comment>pound</comment>
        <translation>lb</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/informationsiss.cpp" line="148"/>
        <source>ft^2</source>
        <comment>foot square</comment>
        <translation>ft^2</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/informationsiss.cpp" line="171"/>
        <source>Aucun évènement contenu dans le fichier ISS</source>
        <translation>ISSファイルには事象が含まれていません</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/informationsiss.cpp" line="288"/>
        <source>Téléchargement du fichier d&apos;informations ISS...</source>
        <translation>ISS情報ファイルのダウンロード...</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/informationsiss.cpp" line="290"/>
        <source>Téléchargement terminé</source>
        <translation>ダウンロードが完了しました</translation>
    </message>
</context>
<context>
    <name>InformationsSatellite</name>
    <message>
        <location filename="../src/interface/onglets/donnees/informationssatellite.ui" line="65"/>
        <source>Nom :</source>
        <translation>名称 :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/informationssatellite.ui" line="193"/>
        <source>Inclinaison :</source>
        <translation>軌道傾斜角 :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/informationssatellite.ui" line="206"/>
        <source>AD noeud ascendant :</source>
        <extracomment>Right ascension of the ascending node</extracomment>
        <translation>昇交点赤経 :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/informationssatellite.ui" line="219"/>
        <source>Excentricité :</source>
        <translation>離心率 :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/informationssatellite.ui" line="232"/>
        <source>Argument du périgée :</source>
        <translation>近地点引数 :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/informationssatellite.ui" line="333"/>
        <source>Numéro NORAD :</source>
        <translation>NORAD番号 :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/informationssatellite.ui" line="346"/>
        <source>Désignation COSPAR :</source>
        <translation>国際衛星識別符号 :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/informationssatellite.ui" line="359"/>
        <source>Époque (UTC) :</source>
        <extracomment>Universal Time Coordinated</extracomment>
        <translation>元期 (UTC) :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/informationssatellite.ui" line="372"/>
        <source>Coeff pseudo-balistique :</source>
        <extracomment>Pseudo-ballistic coefficient</extracomment>
        <translation>大気抵抗係数項 :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/informationssatellite.ui" line="458"/>
        <source>Date de lancement :</source>
        <translation>打ち上げ日 :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/informationssatellite.ui" line="468"/>
        <source>Catégorie d&apos;orbite :</source>
        <translation>軌道種類 :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/informationssatellite.ui" line="478"/>
        <source>Pays/Organisation :</source>
        <translation>国/組織 :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/informationssatellite.ui" line="488"/>
        <source>Site de lancement :</source>
        <translation>発射場 :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/informationssatellite.ui" line="576"/>
        <location filename="../src/interface/onglets/donnees/informationssatellite.ui" line="676"/>
        <location filename="../src/interface/onglets/donnees/informationssatellite.ui" line="702"/>
        <source>rev/jour</source>
        <extracomment>revolution per day</extracomment>
        <translation>回転/day</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/informationssatellite.ui" line="650"/>
        <source>n&quot; / 6 :</source>
        <extracomment>second derivative of the mean motion divided by six (in revolution per day cube)</extracomment>
        <translation>n&quot; / 6 :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/informationssatellite.ui" line="663"/>
        <source>Moyen mouvement :</source>
        <translation>平均運動 :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/informationssatellite.ui" line="689"/>
        <source>Nb orbites à l&apos;époque :</source>
        <translation>元期での周回数 :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/informationssatellite.ui" line="715"/>
        <source>n&apos; / 2 :</source>
        <extracomment>derivative of the mean motion divided by two (in revolution per day square)</extracomment>
        <translation>n&apos; / 2 :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/informationssatellite.ui" line="795"/>
        <source>Anomalie moyenne :</source>
        <translation>平均近点角 :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/informationssatellite.ui" line="824"/>
        <source>Magnitude std/max :</source>
        <extracomment>Standard/maximum magnitude</extracomment>
        <translation>標準・最大等級 :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/informationssatellite.ui" line="850"/>
        <source>Modèle orbital :</source>
        <translation>軌道モデル :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/informationssatellite.ui" line="876"/>
        <source>Dimensions/Section :</source>
        <translation>大きさ・断面積 :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/informationssatellite.cpp" line="144"/>
        <location filename="../src/interface/onglets/donnees/informationssatellite.cpp" line="147"/>
        <source>Inconnue</source>
        <translation>不明</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/informationssatellite.cpp" line="150"/>
        <location filename="../src/interface/onglets/donnees/informationssatellite.cpp" line="153"/>
        <source>Inconnu</source>
        <translation>不明</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/informationssatellite.cpp" line="192"/>
        <source>Modèle haute orbite</source>
        <translation>高軌道モデル</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/informationssatellite.cpp" line="192"/>
        <source>Modèle basse orbite</source>
        <translation>低軌道モデル</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/informationssatellite.cpp" line="199"/>
        <source>m</source>
        <comment>meter</comment>
        <translation>m</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/informationssatellite.cpp" line="199"/>
        <source>ft</source>
        <comment>foot</comment>
        <translation>ft</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/informationssatellite.cpp" line="209"/>
        <source>Sphérique. R=%1 %2</source>
        <comment>R = radius</comment>
        <translation>球形。 R=%1 %2</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/informationssatellite.cpp" line="213"/>
        <source>Cylindrique. L=%1 %2, R=%3 %2</source>
        <comment>L = height; R = radius</comment>
        <translation>円筒形。 L=%1 %2, R=%3 %2</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/informationssatellite.cpp" line="217"/>
        <source>Boîte. %1 x %2 x %3 %4</source>
        <translation>直方体。 %1 x %2 x %3 %4</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/informationssatellite.cpp" line="221"/>
        <location filename="../src/interface/onglets/donnees/informationssatellite.cpp" line="298"/>
        <source>Inconnues</source>
        <translation>不明</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/informationssatellite.cpp" line="255"/>
        <source>Problème de droits d&apos;écriture du fichier %1</source>
        <translation>ファイル「%1」の書き込み権限に問題があります</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/informationssatellite.cpp" line="267"/>
        <source>Nom du satellite :</source>
        <translation>衛星名 :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/informationssatellite.cpp" line="269"/>
        <source>Numéro NORAD            : %1 		Moyen mouvement       : %2 rev/jour	 Date de lancement  : %3</source>
        <comment>revolution per day</comment>
        <translation>NORAD番号       : %1 				平均運動     : %2 回転/day	打ち上げ日  : %3</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/informationssatellite.cpp" line="273"/>
        <source>Désignation COSPAR      : %1		n&apos;/2                  : %2 rev/jour^2	 Catégorie d&apos;orbite : %3</source>
        <comment>n&apos;/2 = derivative of the mean motion divided by two (in revolution per day square)</comment>
        <translation>国際衛星識別符号 : %1				n&apos;/2         : %2 回転/day^2	軌道種類 : %3</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/informationssatellite.cpp" line="285"/>
        <source>Inclinaison             : %1		Anomalie moyenne      : %2</source>
        <translation>軌道傾斜角       : %1				平均近点角     : %2</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/informationssatellite.cpp" line="289"/>
        <source>AD noeud ascendant      : %1		Magnitude std/max     : %2</source>
        <comment>Right ascension of the ascending node, Standard/Maximum magnitude</comment>
        <translation>昇交点赤経       : %1				標準・最大等級  : %2</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/informationssatellite.cpp" line="293"/>
        <source>Excentricité            : %1		Modèle orbital        : %2</source>
        <translation>離心率           : %1				軌道モデル      : %2</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/informationssatellite.cpp" line="296"/>
        <source>Argument du périgée     : %1		Dimensions/Section    : %2%3</source>
        <translation>近地点引数       : %1				大きさ・断面積   : %2%3</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/informationssatellite.cpp" line="277"/>
        <source>Époque (UTC)            : %1	n&quot;/6                  : %2 rev/jour^3	 Pays/Organisation  : %3</source>
        <comment>n&quot;/6 = second derivative of the mean motion divided by six (in revolution per day cube)</comment>
        <translation>元期 (UTC)      : %1		n&quot;/6         : %2 回転/day^3	国/組織  : %3</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/informationssatellite.cpp" line="281"/>
        <source>Coeff pseudo-balistique : %1 (1/Re)	Nb orbites à l&apos;époque : %2			 Site de lancement  : %3</source>
        <comment>Pseudo-ballistic coefficient in 1/Earth radius</comment>
        <translation>大気抵抗係数項   : %1 (1/Re)			元期での周回数 : %2			発射場  : %3</translation>
    </message>
</context>
<context>
    <name>Logging</name>
    <message>
        <location filename="../src/interface/logging/logging.ui" line="29"/>
        <source>Gestion des fichiers de log</source>
        <translation>ログファイルマネージャー</translation>
    </message>
    <message>
        <location filename="../src/interface/logging/logging.ui" line="86"/>
        <source>Nom du fichier</source>
        <translation>ファイル名</translation>
    </message>
    <message>
        <location filename="../src/interface/logging/logging.ui" line="91"/>
        <source>Niveau</source>
        <extracomment>Error level</extracomment>
        <translation>レベル</translation>
    </message>
    <message>
        <location filename="../src/interface/logging/logging.ui" line="96"/>
        <source>Nombre de messages</source>
        <translation>メッセージ数</translation>
    </message>
    <message>
        <location filename="../src/interface/logging/logging.cpp" line="157"/>
        <source>FATAL</source>
        <translation>致命的なエラー</translation>
    </message>
    <message>
        <location filename="../src/interface/logging/logging.cpp" line="163"/>
        <source>ERREUR</source>
        <translation>エラー</translation>
    </message>
    <message>
        <location filename="../src/interface/logging/logging.cpp" line="169"/>
        <source>WARNING</source>
        <translation>ワーニング</translation>
    </message>
    <message>
        <location filename="../src/interface/logging/logging.cpp" line="175"/>
        <source>DEBUG</source>
        <translation>デバッグ</translation>
    </message>
    <message>
        <location filename="../src/interface/logging/logging.cpp" line="180"/>
        <source>INFO</source>
        <translation>情報</translation>
    </message>
    <message>
        <location filename="../src/interface/logging/logging.cpp" line="215"/>
        <source>Exporter ...</source>
        <translation>エクスポートする...</translation>
    </message>
    <message>
        <location filename="../src/interface/logging/logging.cpp" line="275"/>
        <source>Enregistrer sous...</source>
        <translation>名前を付けて保存...</translation>
    </message>
    <message>
        <location filename="../src/interface/logging/logging.cpp" line="275"/>
        <source>Fichiers log (*.log)</source>
        <translation>ログファイル (*.log)</translation>
    </message>
</context>
<context>
    <name>Onglets</name>
    <message>
        <location filename="../src/interface/onglets/onglets.ui" line="48"/>
        <source>Général</source>
        <translation>一般</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/onglets.ui" line="267"/>
        <source>Antenne</source>
        <translation>アンテナ</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/onglets.ui" line="56"/>
        <location filename="../src/interface/onglets/onglets.cpp" line="278"/>
        <source>Éléments osculateurs</source>
        <translation>接触軌道要素</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/onglets.ui" line="64"/>
        <location filename="../src/interface/onglets/onglets.cpp" line="395"/>
        <location filename="../src/interface/onglets/onglets.cpp" line="416"/>
        <location filename="../src/interface/onglets/onglets.cpp" line="432"/>
        <source>Informations satellite</source>
        <translation>衛星情報</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/onglets.ui" line="157"/>
        <location filename="../src/interface/onglets/onglets.cpp" line="70"/>
        <source>Prévisions</source>
        <translation>予報</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/onglets.cpp" line="71"/>
        <source>Flashs</source>
        <translation>フレア</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/onglets.cpp" line="72"/>
        <source>Transits</source>
        <translation>太陽面・月面通過</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/onglets.cpp" line="74"/>
        <source>Starlink</source>
        <translation>Starlink</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/onglets.cpp" line="397"/>
        <location filename="../src/interface/onglets/onglets.cpp" line="410"/>
        <location filename="../src/interface/onglets/onglets.cpp" line="425"/>
        <source>Informations ISS</source>
        <translation>ISS情報</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/onglets.ui" line="259"/>
        <source>Télescope</source>
        <translation>望遠鏡</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/onglets.cpp" line="73"/>
        <source>Évènements orbitaux</source>
        <translation>軌道上事象</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/onglets.cpp" line="399"/>
        <location filename="../src/interface/onglets/onglets.cpp" line="404"/>
        <location filename="../src/interface/onglets/onglets.cpp" line="409"/>
        <location filename="../src/interface/onglets/onglets.cpp" line="426"/>
        <source>Recherche données</source>
        <translation>データを検索する</translation>
    </message>
</context>
<context>
    <name>Options</name>
    <message>
        <location filename="../src/interface/options/options.ui" line="26"/>
        <source>Options</source>
        <translation>オプション</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="58"/>
        <location filename="../src/interface/options/options.cpp" line="1138"/>
        <source>Lieu d&apos;observation</source>
        <translation>観測地</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="61"/>
        <source>Gestion et sélection des lieux d&apos;observation</source>
        <translation>観測地管理と選択</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="70"/>
        <source>Configuration</source>
        <translation>設定</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="73"/>
        <source>Configuration de l&apos;interface graphique</source>
        <translation>GUIセットアップ</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="82"/>
        <source>Wall Command Center</source>
        <translation>ウォールコマンドセンター</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="85"/>
        <source>Paramètres d&apos;affichage du Wall Command Center</source>
        <translation>ウォールコマンドセンターの表示設定</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="162"/>
        <source>Lieux d&apos;observation :</source>
        <translation>観測地 :</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="191"/>
        <source>Lieu :</source>
        <translation>場所 :</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="222"/>
        <location filename="../src/interface/options/options.ui" line="456"/>
        <source>Longitude :</source>
        <translation>経度 :</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="232"/>
        <location filename="../src/interface/options/options.ui" line="504"/>
        <source>Latitude :</source>
        <translation>緯度 :</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="242"/>
        <location filename="../src/interface/options/options.ui" line="574"/>
        <source>Altitude :</source>
        <comment>Altitude of observer</comment>
        <translation>高さ :</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="293"/>
        <location filename="../src/interface/options/options.ui" line="390"/>
        <source>Valider</source>
        <translation>確定する</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="309"/>
        <location filename="../src/interface/options/options.ui" line="374"/>
        <location filename="../src/interface/options/options.cpp" line="981"/>
        <location filename="../src/interface/options/options.cpp" line="1143"/>
        <source>Annuler</source>
        <translation>キャンセル</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="337"/>
        <location filename="../src/interface/options/options.ui" line="418"/>
        <source>Nom :</source>
        <translation>名称 :</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="515"/>
        <location filename="../src/interface/options/options.cpp" line="419"/>
        <source>Nord</source>
        <translation>北</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="520"/>
        <location filename="../src/interface/options/options.cpp" line="419"/>
        <location filename="../src/interface/options/options.cpp" line="1529"/>
        <source>Sud</source>
        <translation>南</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="532"/>
        <location filename="../src/interface/options/options.cpp" line="418"/>
        <location filename="../src/interface/options/options.cpp" line="1527"/>
        <source>Est</source>
        <translation>東</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="537"/>
        <location filename="../src/interface/options/options.cpp" line="418"/>
        <source>Ouest</source>
        <translation>西</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="617"/>
        <source>m</source>
        <extracomment>Meter</extracomment>
        <translation>m</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="649"/>
        <source>Ajouter dans :</source>
        <translation>追加先 :</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="680"/>
        <source>Lieux sélectionnés :</source>
        <translation>選択した観測地 :</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="702"/>
        <location filename="../src/interface/options/options.ui" line="743"/>
        <source>Cliquer droit pour afficher le menu contextuel</source>
        <translation>右クリックしてコンテキストメニューを表示します</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="721"/>
        <source>Sélection de la catégorie :</source>
        <translation>カテゴリーの選択 :</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="840"/>
        <location filename="../src/interface/options/options.ui" line="862"/>
        <source>Filtre</source>
        <translation>フィルター</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="893"/>
        <source>Satellites</source>
        <translation>人工衛星</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="926"/>
        <source>Notification sonore</source>
        <translation>通知音</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="958"/>
        <source>Trace dans le ciel</source>
        <translation>星図上に飛行経路を表示する</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="971"/>
        <source>Affichage du numéro NORAD dans les listes</source>
        <translation>リスト内のNORAD番号を表示する</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="1008"/>
        <source>Trace au sol</source>
        <translation>グラウンドトラック</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="1055"/>
        <source>Rotation de l&apos;icône ISS</source>
        <translation>ISSアイコンの回転</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="1071"/>
        <source>Zone de visibilité</source>
        <translation>可視範囲</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="1090"/>
        <source>Icône des satellites</source>
        <translation>衛星のアイコン</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="1106"/>
        <source>Nom des satellites</source>
        <translation>衛星名</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="1118"/>
        <source>Système solaire / étoiles</source>
        <translation>太陽系 / 星</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="1133"/>
        <source>Nom des étoiles</source>
        <translation>星の名前</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="1167"/>
        <source>Zone d&apos;ombre</source>
        <translation>夜部分に影</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="1214"/>
        <source>Affichage des constellations</source>
        <translation>星座を表示する</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="1233"/>
        <source>Rotation de la Lune pour l&apos;hémisphère Sud</source>
        <translation>南半球のために月を回転させる</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="1249"/>
        <source>Soleil</source>
        <translation>太陽</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="1268"/>
        <source>Affichage des planètes</source>
        <translation>惑星を表示する</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="1302"/>
        <source>Magnitude limite des étoiles :</source>
        <translation>星の最小等級 :</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="1346"/>
        <source>Phase de la Lune</source>
        <translation>月相</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="1362"/>
        <source>Lune</source>
        <translation>月</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="1371"/>
        <location filename="../src/interface/options/options.ui" line="2138"/>
        <source>Affichage</source>
        <translation>表示</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="1386"/>
        <source>Prise en compte des éclipses produites par la Lune</source>
        <translation>月によって生成された衛星の食を考慮に入れる</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="1402"/>
        <source>Prise en compte de la réfraction atmosphérique</source>
        <translation>大気差を考慮に入れる</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="1418"/>
        <source>Inversion Nord/Sud sur le radar</source>
        <translation>レーダーの南北を入れ替え</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="1434"/>
        <source>Nom des lieux d&apos;observation</source>
        <translation>観測地の名前</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="1453"/>
        <source>Prise en compte des éclipses partielles sur la magnitude</source>
        <translation>等級の計算には衛星の部分食を考慮に入れる</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="1469"/>
        <source>Affichage du jour julien</source>
        <translation>ユリウス通日を表示する</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="1500"/>
        <source>Carte du monde :</source>
        <translation>世界地図 :</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="1532"/>
        <source>Grille</source>
        <translation>グリッド</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="1548"/>
        <source>Affichage de la SAA</source>
        <translation>SAAを表示する</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="1564"/>
        <source>Radar</source>
        <translation>レーダー</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="1583"/>
        <source>Prise en compte de l&apos;extinction atmosphérique</source>
        <translation>大気減光を考慮に入れる</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="1599"/>
        <source>Affichage des frontières</source>
        <translation>国境線を表示する</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="1615"/>
        <source>Inversion Est/Ouest sur le radar</source>
        <translation>レーダーの東西を入れ替え</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="1631"/>
        <source>Affichage des coordonnées</source>
        <translation>座標を表示する</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="1659"/>
        <source>Valeur du zoom pour les cartes du monde dans le navigateur :</source>
        <translation>ウェブブラウザでの世界地図のズームレベル :</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="1694"/>
        <source>Mode sombre</source>
        <translation>ダークモード</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="1703"/>
        <source>Système</source>
        <translation>システム設定</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="1718"/>
        <source>Vérification des mises à jour au démarrage</source>
        <translation>起動時に更新を確認する</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="1734"/>
        <source>Écart Heure locale - UTC</source>
        <translation>現地時間とUTCの差</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="1749"/>
        <source>Temps Universel Coordonné (UTC)</source>
        <translation>協定世界時 (UTC)</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="1777"/>
        <source>Heure locale = </source>
        <translation>現地時間 = </translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="1825"/>
        <source>Auto</source>
        <extracomment>Automatic</extracomment>
        <translation>自動</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="1848"/>
        <source>Unités</source>
        <translation>単位</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="1863"/>
        <source>Système métrique</source>
        <translation>メートル法</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="1882"/>
        <source>Système anglo-saxon</source>
        <translation>ヤードポンド法</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="1902"/>
        <source>Système horaire</source>
        <translation>時刻の表示形式</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="1917"/>
        <source>24 heures</source>
        <translation>24時間制</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="1936"/>
        <source>12 heures (AM/PM)</source>
        <translation>12時間制 (AM/PM)</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="1968"/>
        <source>Nombre de fichiers log :</source>
        <translation>ログファイル数 :</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="2014"/>
        <source>Langue :</source>
        <extracomment>Name of language</extracomment>
        <translation>言語 :</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="2057"/>
        <source>Préférences d&apos;affichage :</source>
        <translation>表示設定 :</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="2094"/>
        <source>Enregistrer les préférences d&apos;affichage</source>
        <translation>表示設定を保存する</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="2119"/>
        <source>Style &quot;Wall Command Center&quot;</source>
        <translation>「ウォールコマンドセンター」スタイル</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="2150"/>
        <source>Nombre d&apos;orbites</source>
        <translation>周回数</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="2166"/>
        <source>Anomalie Atlantique Sud et Zone d&apos;exclusion</source>
        <translation>南大西洋異常帯と不可視域</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="2169"/>
        <source>SAA et ZOE</source>
        <extracomment>South Atlantic Anomaly and Zone of Exclusion</extracomment>
        <translation>SAAとZOE</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="2191"/>
        <source>Cercles d&apos;acquisition</source>
        <translation>受信可能範囲</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="2210"/>
        <source>Angle entre le plan de l&apos;orbite et la direction du Soleil</source>
        <translation>軌道面と太陽方向がなす角</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="2213"/>
        <source>Angle beta</source>
        <extracomment>Beta angle (angle between orbit plane and direction of Sun)</extracomment>
        <translation>ベータ角</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="2239"/>
        <source>Choix de la police :</source>
        <translation>フォント選択 :</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="2278"/>
        <source>Couleurs</source>
        <translation>色選択</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="2302"/>
        <source>Greenwich Mean Time</source>
        <translation>グリニッジ標準時</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="2305"/>
        <source>GMT :</source>
        <extracomment>Greenwich Mean Time</extracomment>
        <translation>GMT :</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="2312"/>
        <source>Zone d&apos;exclusion</source>
        <translation>不可視域</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="2315"/>
        <source>ZOE :</source>
        <extracomment>Zone of exclusion</extracomment>
        <translation>ZOE :</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="2322"/>
        <source>Équateur :</source>
        <translation>赤道 :</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="2329"/>
        <source>Terminateur :</source>
        <translation>明暗境界線 :</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="2336"/>
        <source>Cercle de visibilité :</source>
        <translation>可視範囲 :</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="2344"/>
        <location filename="../src/interface/options/options.ui" line="2377"/>
        <location filename="../src/interface/options/options.ui" line="2410"/>
        <source>Rouge</source>
        <translation>赤</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="2349"/>
        <location filename="../src/interface/options/options.ui" line="2368"/>
        <location filename="../src/interface/options/options.ui" line="2382"/>
        <location filename="../src/interface/options/options.ui" line="2405"/>
        <source>Blanc</source>
        <translation>白</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="2354"/>
        <source>Cyan</source>
        <translation>シアン</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="2363"/>
        <source>Noir</source>
        <translation>黒</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="2391"/>
        <source>Brun</source>
        <translation>茶色</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="2396"/>
        <source>Jaune</source>
        <translation>黄色</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="2428"/>
        <source>Stations :</source>
        <translation>ステーション :</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.cpp" line="170"/>
        <location filename="../src/interface/options/options.cpp" line="544"/>
        <source>Créer une catégorie</source>
        <translation>カテゴリーを作成する</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.cpp" line="172"/>
        <location filename="../src/interface/options/options.cpp" line="559"/>
        <source>Créer un nouveau lieu</source>
        <translation>新しい観測地を作成する</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.cpp" line="182"/>
        <location filename="../src/interface/options/options.cpp" line="426"/>
        <location filename="../src/interface/options/options.cpp" line="1080"/>
        <location filename="../src/interface/options/options.cpp" line="1216"/>
        <source>m</source>
        <comment>meter</comment>
        <translation>m</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.cpp" line="182"/>
        <location filename="../src/interface/options/options.cpp" line="426"/>
        <location filename="../src/interface/options/options.cpp" line="1080"/>
        <location filename="../src/interface/options/options.cpp" line="1216"/>
        <source>ft</source>
        <comment>foot</comment>
        <translation>ft</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.cpp" line="193"/>
        <source>L&apos;altitude doit être comprise entre %1%2 et %3%2</source>
        <comment>Observer altitude</comment>
        <translation>高さは%1%2から%3%2の間でなければなりません</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.cpp" line="412"/>
        <source>Lieu : %1</source>
        <translation>場所 : %1</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.cpp" line="548"/>
        <location filename="../src/interface/options/options.cpp" line="567"/>
        <source>Renommer</source>
        <translation>名称変更</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.cpp" line="551"/>
        <location filename="../src/interface/options/options.cpp" line="574"/>
        <source>Supprimer</source>
        <translation>削除する</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.cpp" line="555"/>
        <location filename="../src/interface/options/options.cpp" line="675"/>
        <location filename="../src/interface/options/options.cpp" line="782"/>
        <source>Télécharger...</source>
        <translation>ダウンロード...</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.cpp" line="563"/>
        <source>Ajouter à Mes Préférés</source>
        <translation>お気に入りに追加</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.cpp" line="570"/>
        <source>Modifier</source>
        <translation>編集する</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.cpp" line="637"/>
        <source>Langue</source>
        <comment>Translate by the name of language, for example : English, Français, Español</comment>
        <translation>日本語</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.cpp" line="664"/>
        <location filename="../src/interface/options/options.cpp" line="745"/>
        <location filename="../src/interface/options/options.cpp" line="776"/>
        <source>* Défaut</source>
        <translation>* デフォルト</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.cpp" line="705"/>
        <source>Mes Préférés</source>
        <translation>お気に入り</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.cpp" line="751"/>
        <source>Enregistrer sous...</source>
        <translation>名前を付けて保存...</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.cpp" line="752"/>
        <source>Supprimer...</source>
        <translation>削除する...</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.cpp" line="879"/>
        <source>Problème de droits d&apos;écriture du fichier %1</source>
        <translation>ファイル「%1」の書き込み権限に問題があります</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.cpp" line="976"/>
        <source>Catégorie</source>
        <translation>カテゴリー</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.cpp" line="977"/>
        <source>Nouveau nom de la catégorie :</source>
        <translation>新しいカテゴリー名 :</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.cpp" line="980"/>
        <location filename="../src/interface/options/options.cpp" line="1142"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.cpp" line="996"/>
        <location filename="../src/interface/options/options.cpp" line="1028"/>
        <source>Information</source>
        <translation>情報</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.cpp" line="996"/>
        <source>La catégorie existe déjà. Voulez-vous l&apos;écraser ?</source>
        <translation>このカテゴリーは既に存在します。置き換えますか ?</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.cpp" line="997"/>
        <location filename="../src/interface/options/options.cpp" line="1029"/>
        <location filename="../src/interface/options/options.cpp" line="1240"/>
        <source>Oui</source>
        <translation>はい</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.cpp" line="998"/>
        <location filename="../src/interface/options/options.cpp" line="1030"/>
        <location filename="../src/interface/options/options.cpp" line="1241"/>
        <source>Non</source>
        <translation>いいえ</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.cpp" line="1028"/>
        <source>Voulez-vous vraiment supprimer la catégorie &lt;b&gt;%1&lt;/b&gt; ?</source>
        <translation>カテゴリー&lt;b&gt;%1&lt;/b&gt;を本当に削除しますか ?</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.cpp" line="1110"/>
        <source>Le lieu d&apos;observation &lt;b&gt;%1&lt;/b&gt; fait déjà partie de &lt;b&gt;Mes Préférés&lt;/b&gt;</source>
        <translation>観測地&lt;b&gt;%1&lt;/b&gt;は既に&lt;b&gt;お気に入り&lt;/b&gt;に存在します</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.cpp" line="1139"/>
        <source>Nouveau nom du lieu d&apos;observation :</source>
        <translation>新しい観測地の名前 :</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.cpp" line="1237"/>
        <source>Voulez-vous vraiment supprimer &lt;b&gt;%1&lt;/b&gt; de la catégorie &lt;b&gt;%2&lt;/b&gt; ?</source>
        <translation>&lt;b&gt;%1&lt;/b&gt;をカテゴリー&lt;b&gt;%2&lt;/b&gt;から本当に削除しますか ?</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.cpp" line="1239"/>
        <source>Avertissement</source>
        <translation>警告</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.cpp" line="1366"/>
        <source>Le nom de la catégorie n&apos;est pas spécifié</source>
        <translation>カテゴリー名が指定されていません</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.cpp" line="1386"/>
        <source>La catégorie spécifiée existe déjà</source>
        <translation>指定されたカテゴリーは既に存在します</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.cpp" line="1497"/>
        <source>Le nom du lieu d&apos;observation n&apos;est pas spécifié</source>
        <translation>観測地の名前が指定されていません</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.cpp" line="1506"/>
        <source>Le lieu existe déjà dans la catégorie &lt;b&gt;%1&lt;/b&gt;</source>
        <translation>この観測地は既にカテゴリー&lt;b&gt;%1&lt;/b&gt;に存在します</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.cpp" line="1567"/>
        <source>Lieu d&apos;observation déjà sélectionné</source>
        <translation>観測地はすでに選択されています</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.cpp" line="1672"/>
        <source>UTC</source>
        <comment>Universal Time Coordinated</comment>
        <translation>UTC</translation>
    </message>
</context>
<context>
    <name>Osculateurs</name>
    <message>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.ui" line="63"/>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.ui" line="115"/>
        <source>Date :</source>
        <extracomment>Date and hour</extracomment>
        <translation>日時 :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.ui" line="128"/>
        <source>dddd dd MMMM yyyy HH:mm:ss</source>
        <translation>yyyy年MM月dd日 (dddd)  HH:mm:ss</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.ui" line="189"/>
        <source>Nom :</source>
        <translation>名称 :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.ui" line="296"/>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.cpp" line="218"/>
        <source>Vecteur d&apos;état</source>
        <translation>状態ベクトル</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.ui" line="326"/>
        <source>x :</source>
        <extracomment>Component X of the position vector</extracomment>
        <translation>x :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.ui" line="336"/>
        <source>y :</source>
        <extracomment>Component Y of the position vector</extracomment>
        <translation>y :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.ui" line="346"/>
        <source>z :</source>
        <extracomment>Component Z of the position vector</extracomment>
        <translation>z :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.ui" line="419"/>
        <source>vx :</source>
        <extracomment>Component X of the velocity vector</extracomment>
        <translation>vx :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.ui" line="429"/>
        <source>vy :</source>
        <extracomment>Component Y of the velocity vector</extracomment>
        <translation>vy :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.ui" line="439"/>
        <source>vz :</source>
        <extracomment>Component Z of the velocity vector</extracomment>
        <translation>vz :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.ui" line="449"/>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.ui" line="465"/>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.ui" line="481"/>
        <source>Double-cliquer pour changer d&apos;unités</source>
        <translation>ダブルクリックして単位を変更します</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.ui" line="517"/>
        <source>ECI</source>
        <extracomment>Earth Centered Inertial</extracomment>
        <translation>ECI系</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.ui" line="522"/>
        <source>ECEF</source>
        <extracomment>Earth Centered Earth Fixed</extracomment>
        <translation>ECEF系</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.ui" line="546"/>
        <source>Paramètres képlériens</source>
        <translation>ケプラー軌道要素</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.ui" line="551"/>
        <source>Paramètres circulaires</source>
        <translation>円軌道の軌道要素</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.ui" line="556"/>
        <source>Paramètres équatoriaux</source>
        <translation>赤道軌道の軌道要素</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.ui" line="561"/>
        <source>Paramètres circulaires équatoriaux</source>
        <translation>赤道円軌道の軌道要素</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.ui" line="575"/>
        <source>Éléments osculateurs</source>
        <translation>接触軌道要素</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.ui" line="605"/>
        <source>Anomalie vraie :</source>
        <translation>真近点角 :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.ui" line="615"/>
        <source>Anomalie excentrique :</source>
        <translation>離心近点角 :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.ui" line="625"/>
        <source>Champ de vue :</source>
        <translation>視野 :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.ui" line="698"/>
        <source>Apogée (Altitude) :</source>
        <translation>遠地点 (高度) :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.ui" line="708"/>
        <source>Périgée (Altitude) :</source>
        <translation>近地点 (高度) :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.ui" line="718"/>
        <source>Période orbitale :</source>
        <translation>軌道周期 :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.ui" line="804"/>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.ui" line="1144"/>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.ui" line="1226"/>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.ui" line="1411"/>
        <source>Demi-grand axe :</source>
        <translation>軌道長半径 :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.ui" line="814"/>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.ui" line="1180"/>
        <source>Excentricité :</source>
        <translation>離心率 :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.ui" line="824"/>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.ui" line="1002"/>
        <source>Inclinaison :</source>
        <translation>軌道傾斜角 :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.ui" line="897"/>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.ui" line="992"/>
        <source>AD noeud ascendant :</source>
        <extracomment>Right ascension of the ascending node</extracomment>
        <translation>昇交点赤経 :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.ui" line="907"/>
        <source>Argument du périgée :</source>
        <translation>近地点引数 :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.ui" line="943"/>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.ui" line="1293"/>
        <source>Anomalie moyenne :</source>
        <translation>平均近点角 :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.ui" line="1203"/>
        <source>Longitude du périgée :</source>
        <translation>近地点黄経 :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.ui" line="1213"/>
        <source>Somme de l&apos;argument du périgée et de l&apos;ascension droite du noeud ascendant</source>
        <translation>近地点引数と昇交点赤経の和</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.ui" line="1273"/>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.ui" line="1458"/>
        <source>Ix :</source>
        <extracomment>Component X of inclination vector</extracomment>
        <translation>Ix :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.ui" line="1283"/>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.ui" line="1468"/>
        <source>Iy :</source>
        <extracomment>Component Y of inclination vector</extracomment>
        <translation>Iy :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.ui" line="1088"/>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.ui" line="1368"/>
        <source>Ex :</source>
        <extracomment>Component X of eccentricity vector</extracomment>
        <translation>Ex :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.ui" line="1111"/>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.ui" line="1378"/>
        <source>Ey :</source>
        <extracomment>Component Y of eccentricity vector</extracomment>
        <translation>Ey :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.ui" line="1012"/>
        <source>Position sur orbite :</source>
        <translation>軌道上の位置 :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.ui" line="1048"/>
        <source>Somme de l&apos;argument du périgée et de l&apos;anomalie moyenne</source>
        <translation>近地点引数と平均近点角の和</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.ui" line="1478"/>
        <source>Argument longitude vraie :</source>
        <translation>真黄経 :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.ui" line="1514"/>
        <source>Somme de l&apos;argument du périgée, de l&apos;ascension droite du noeud ascendant et de l&apos;anomalie vraie</source>
        <translation>近地点引数、昇交点赤経、真近点角の和</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.ui" line="1539"/>
        <source>Divers</source>
        <translation>雑多</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.ui" line="1566"/>
        <source>Doppler@100MHz :</source>
        <extracomment>Doppler effect at 100 Mega Hertz</extracomment>
        <translation>100MHzのドップラー :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.ui" line="1573"/>
        <source>Atténuation :</source>
        <translation>減衰 :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.ui" line="1580"/>
        <source>Délai :</source>
        <extracomment>Delay of signal at light speed</extracomment>
        <translation>遅延 :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.ui" line="1629"/>
        <source>Phasage :</source>
        <translation>準回帰パラメータ :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.cpp" line="133"/>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.cpp" line="351"/>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.cpp" line="430"/>
        <source>km</source>
        <comment>Kilometer</comment>
        <translation>km</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.cpp" line="133"/>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.cpp" line="351"/>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.cpp" line="430"/>
        <source>nmi</source>
        <comment>nautical mile</comment>
        <translation>nmi</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.cpp" line="176"/>
        <source>N/A</source>
        <comment>Not applicable</comment>
        <translation>該当なし</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.cpp" line="203"/>
        <source>Problème de droits d&apos;écriture du fichier %1</source>
        <translation>ファイル「%1」の書き込み権限に問題があります</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.cpp" line="213"/>
        <source>Date :</source>
        <comment>Date and hour</comment>
        <translation>日時 :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.cpp" line="216"/>
        <source>Nom du satellite :</source>
        <translation>衛星名 :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.cpp" line="219"/>
        <source>x : %1			vx : %2</source>
        <comment>Position, velocity</comment>
        <translation>x : %1 	vx : %2</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.cpp" line="222"/>
        <source>y : %1			vy : %2</source>
        <comment>Position, velocity</comment>
        <translation>y : %1 	vy : %2</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.cpp" line="225"/>
        <source>z : %1			vz : %2</source>
        <comment>Position, velocity</comment>
        <translation>z : %1 	vz : %2</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.cpp" line="228"/>
        <source>Éléments osculateurs :</source>
        <translation>接触軌道要素 :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.cpp" line="233"/>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.cpp" line="246"/>
        <source>Demi-grand axe       : %1	Ascension droite du noeud ascendant : %2</source>
        <translation>軌道長半径 : %1   	昇交点赤経    : %2</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.cpp" line="236"/>
        <source>Excentricité         : %1	Argument du périgée                 : %2</source>
        <translation>離心率     : %1	近地点引数    : %2</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.cpp" line="239"/>
        <source>Inclinaison          : %1	Anomalie moyenne                    : %2</source>
        <translation>軌道傾斜角 : %1	平均近点角    : %2</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.cpp" line="249"/>
        <source>Ex                   : %1	Inclinaison                         : %2</source>
        <comment>Ex = Component X of eccentricity vector</comment>
        <translation>Ex        : %1	軌道傾斜角    : %2</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.cpp" line="252"/>
        <source>Ey                   : %1	Position sur orbite                 : %2</source>
        <comment>Ey = Component Y of eccentricity vector</comment>
        <translation>Ey        : %1	軌道上の位置  : %2</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.cpp" line="259"/>
        <source>Demi-grand axe       : %1	Ix                 : %2</source>
        <comment>Ix = Component X of inclination vector</comment>
        <translation>軌道長半径 : %1   	Ix        : %2</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.cpp" line="262"/>
        <source>Excentricité         : %1	Iy                 : %2</source>
        <comment>Iy = Component Y of inclination vector</comment>
        <translation>離心率     : %1   Iy        : %2</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.cpp" line="265"/>
        <source>Longitude du périgée : %1	Anomalie moyenne   : %2</source>
        <translation>近地点黄経 : %1   	平均近点角 : %2</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.cpp" line="272"/>
        <source>Demi-grand axe       : %1	Ix                          : %2</source>
        <comment>Ix = Component X of inclination vector</comment>
        <translation>軌道長半径 : %1   	Ix     : %2</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.cpp" line="275"/>
        <source>Ex                   : %1	Iy                          : %2</source>
        <comment>Ex = Component X of eccentricity vector, Iy = Component Y of inclination vector</comment>
        <translation>Ex        : %1  Iy     : %2</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.cpp" line="279"/>
        <source>Ey                   : %1	Argument de longitude vraie : %2</source>
        <comment>Ey = Component Y of eccentricity vector</comment>
        <translation>Ey        : %1  真黄経 : %2</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.cpp" line="288"/>
        <source>Anomalie vraie       : %1	Apogée  (Altitude) : %2</source>
        <translation>真近点角   : %1	遠地点 (高度) : %2</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.cpp" line="291"/>
        <source>Anomalie excentrique : %1	Périgée (Altitude) : %2</source>
        <translation>離心近点角 : %1	近地点 (高度) : %2</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.cpp" line="294"/>
        <source>Champ de vue         : %1  	Période orbitale   : %2</source>
        <translation>視野       : %1  	軌道周期      : %2</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.cpp" line="298"/>
        <source>Divers :</source>
        <translation>雑多 :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.cpp" line="299"/>
        <source>Doppler @ 100 MHz    : %1</source>
        <comment>Doppler effect at 100 MegaHertz</comment>
        <translation>100MHzのドップラー : %1</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.cpp" line="302"/>
        <source>Atténuation          : %1</source>
        <translation>減衰           : %1</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.cpp" line="305"/>
        <source>Délai                : %1</source>
        <comment>Delay of signal at light speed</comment>
        <translation>遅延           : %1</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.cpp" line="308"/>
        <source>Phasage              : %1</source>
        <translation>準回帰パラメータ   : %1</translation>
    </message>
</context>
<context>
    <name>Outils</name>
    <message>
        <location filename="../src/interface/outils/outils.ui" line="26"/>
        <source>Outils</source>
        <translation>ツール</translation>
    </message>
    <message>
        <location filename="../src/interface/outils/outils.ui" line="58"/>
        <source>Gestionnaire éléments orbitaux</source>
        <translation>軌道要素マネージャー</translation>
    </message>
    <message>
        <location filename="../src/interface/outils/outils.ui" line="63"/>
        <source>Mise à jour TLE</source>
        <translation>TLEの更新</translation>
    </message>
    <message>
        <location filename="../src/interface/outils/outils.ui" line="68"/>
        <source>Gestionnaire TLE</source>
        <translation>TLEマネージャー</translation>
    </message>
    <message>
        <location filename="../src/interface/outils/outils.ui" line="107"/>
        <source>Age maximal des éléments orbitaux (en jours) :</source>
        <translation>軌道要素の最大年齢 (日数) :</translation>
    </message>
    <message>
        <location filename="../src/interface/outils/outils.ui" line="164"/>
        <source>Liste des fichiers d&apos;éléments orbitaux :</source>
        <translation>軌道要素ファイルのリスト :</translation>
    </message>
    <message>
        <location filename="../src/interface/outils/outils.ui" line="180"/>
        <source>Valider</source>
        <translation>確定する</translation>
    </message>
    <message>
        <location filename="../src/interface/outils/outils.ui" line="196"/>
        <source>Annuler</source>
        <translation>キャンセル</translation>
    </message>
    <message>
        <location filename="../src/interface/outils/outils.ui" line="234"/>
        <location filename="../src/interface/outils/outils.ui" line="441"/>
        <source>Domaine :</source>
        <translation>ドメイン :</translation>
    </message>
    <message>
        <location filename="../src/interface/outils/outils.ui" line="275"/>
        <source>Nom du groupe :</source>
        <translation>グループ名 :</translation>
    </message>
    <message>
        <location filename="../src/interface/outils/outils.ui" line="316"/>
        <source>Liste des satellites :</source>
        <translation>衛星リスト :</translation>
    </message>
    <message>
        <location filename="../src/interface/outils/outils.ui" line="335"/>
        <source>Filtre</source>
        <translation>フィルター</translation>
    </message>
    <message>
        <location filename="../src/interface/outils/outils.ui" line="375"/>
        <source>Fichiers d&apos;éléments orbitaux :</source>
        <translation>軌道要素ファイル :</translation>
    </message>
    <message>
        <location filename="../src/interface/outils/outils.ui" line="391"/>
        <source>Groupe d&apos;éléments orbitaux :</source>
        <translation>軌道要素グループ :</translation>
    </message>
    <message>
        <location filename="../src/interface/outils/outils.ui" line="413"/>
        <source>Cocher un groupe pour activer sa mise à jour automatique</source>
        <translation>グループをチェックして、自動更新を有効にします</translation>
    </message>
    <message>
        <location filename="../src/interface/outils/outils.ui" line="464"/>
        <source>Mettre à jour le groupe sélectionné</source>
        <translation>選択したグループを更新する</translation>
    </message>
    <message>
        <location filename="../src/interface/outils/outils.ui" line="607"/>
        <source>Mise à jour TLE auto</source>
        <translation>TLEの自動更新</translation>
    </message>
    <message>
        <location filename="../src/interface/outils/outils.ui" line="723"/>
        <source>Mettre à jour maintenant</source>
        <translation>今すぐ更新する</translation>
    </message>
    <message>
        <location filename="../src/interface/outils/outils.ui" line="752"/>
        <source>Affichage des messages informatifs</source>
        <translation>情報メッセージを表示する</translation>
    </message>
    <message>
        <location filename="../src/interface/outils/outils.ui" line="757"/>
        <source>Accepter ajout/suppression de TLE</source>
        <translation>TLEの追加・削除を受け入れる</translation>
    </message>
    <message>
        <location filename="../src/interface/outils/outils.ui" line="762"/>
        <source>Refuser ajout/suppression de TLE</source>
        <translation>TLEの追加・削除を断る</translation>
    </message>
    <message>
        <location filename="../src/interface/outils/outils.ui" line="772"/>
        <source>Mise à jour TLE manuelle</source>
        <translation>TLEの手動更新</translation>
    </message>
    <message>
        <location filename="../src/interface/outils/outils.ui" line="802"/>
        <location filename="../src/interface/outils/outils.ui" line="832"/>
        <source>Parcourir...</source>
        <translation>ブラウズ...</translation>
    </message>
    <message>
        <location filename="../src/interface/outils/outils.ui" line="812"/>
        <source>Fichier à mettre à jour :</source>
        <translation>更新するファイル :</translation>
    </message>
    <message>
        <location filename="../src/interface/outils/outils.ui" line="852"/>
        <source>Fichier TLE à lire :</source>
        <translation>読むTLEファイル :</translation>
    </message>
    <message>
        <location filename="../src/interface/outils/outils.ui" line="868"/>
        <source>Mettre à jour</source>
        <translation>更新する</translation>
    </message>
    <message>
        <location filename="../src/interface/outils/outils.ui" line="936"/>
        <source>Liste de fichiers TLE :</source>
        <translation>TLEファイルのリスト :</translation>
    </message>
    <message>
        <location filename="../src/interface/outils/outils.ui" line="949"/>
        <location filename="../src/interface/outils/outils.cpp" line="1071"/>
        <source>Importer TLE...</source>
        <translation>TLEをインポートする...</translation>
    </message>
    <message>
        <location filename="../src/interface/outils/outils.cpp" line="146"/>
        <source>Créer un groupe d&apos;éléments orbitaux</source>
        <translation>軌道要素グループを作成する</translation>
    </message>
    <message>
        <location filename="../src/interface/outils/outils.cpp" line="148"/>
        <source>Ajouter des fichiers d&apos;éléments orbitaux</source>
        <translation>軌道要素ファイルを追加する</translation>
    </message>
    <message>
        <location filename="../src/interface/outils/outils.cpp" line="163"/>
        <source>Aucun fichier TLE dans le répertoire d&apos;éléments orbitaux</source>
        <translation>軌道要素フォルダにTLEファイルがありません</translation>
    </message>
    <message>
        <location filename="../src/interface/outils/outils.cpp" line="219"/>
        <source>Copier dans le presse-papier</source>
        <translation>クリップボードにコピー</translation>
    </message>
    <message>
        <location filename="../src/interface/outils/outils.cpp" line="222"/>
        <source>Créer un groupe</source>
        <translation>グループを作成する</translation>
    </message>
    <message>
        <location filename="../src/interface/outils/outils.ui" line="923"/>
        <location filename="../src/interface/outils/outils.cpp" line="226"/>
        <location filename="../src/interface/outils/outils.cpp" line="234"/>
        <source>Supprimer</source>
        <translation>削除する</translation>
    </message>
    <message>
        <location filename="../src/interface/outils/outils.cpp" line="230"/>
        <source>Ajouter des fichiers</source>
        <translation>ファイルを追加する</translation>
    </message>
    <message>
        <location filename="../src/interface/outils/outils.cpp" line="263"/>
        <source>Fichier %1 :</source>
        <translation>ファイル「%1」 :</translation>
    </message>
    <message>
        <location filename="../src/interface/outils/outils.cpp" line="268"/>
        <source>TLE du satellite %1 (%2) non réactualisé</source>
        <translation>衛星「%1」(%2)のTLEは既に最新です</translation>
    </message>
    <message>
        <location filename="../src/interface/outils/outils.cpp" line="278"/>
        <source>Nombre de TLE(s) supprimés : %1</source>
        <translation>削除されたTLE数 : %1</translation>
    </message>
    <message>
        <location filename="../src/interface/outils/outils.cpp" line="283"/>
        <source>Nombre de TLE(s) ajoutés : %1</source>
        <translation>追加されたTLE数 : %1</translation>
    </message>
    <message>
        <location filename="../src/interface/outils/outils.cpp" line="288"/>
        <source>%1 TLE(s) sur %2 mis à jour</source>
        <translation>%1つのTLEのうち%2つが更新されました</translation>
    </message>
    <message>
        <location filename="../src/interface/outils/outils.cpp" line="293"/>
        <source>Mise à jour de tous les TLE effectuée (fichier de %1 satellite(s))</source>
        <translation>すべてのTLEが更新されました (%1基の衛星が含まれているファイル)</translation>
    </message>
    <message>
        <location filename="../src/interface/outils/outils.cpp" line="298"/>
        <source>Aucun TLE mis à jour</source>
        <translation>更新されたTLEはありません</translation>
    </message>
    <message>
        <location filename="../src/interface/outils/outils.cpp" line="401"/>
        <location filename="../src/interface/outils/outils.cpp" line="431"/>
        <source>Information</source>
        <translation>情報</translation>
    </message>
    <message>
        <location filename="../src/interface/outils/outils.cpp" line="401"/>
        <source>Voulez-vous vraiment supprimer ce fichier du groupe &lt;b&gt;%1&lt;/b&gt; ?</source>
        <translation>ファイルを&lt;b&gt;%1&lt;/b&gt;グループから本当に削除しますか ?</translation>
    </message>
    <message>
        <location filename="../src/interface/outils/outils.cpp" line="402"/>
        <location filename="../src/interface/outils/outils.cpp" line="432"/>
        <location filename="../src/interface/outils/outils.cpp" line="1140"/>
        <source>Oui</source>
        <translation>はい</translation>
    </message>
    <message>
        <location filename="../src/interface/outils/outils.cpp" line="403"/>
        <location filename="../src/interface/outils/outils.cpp" line="433"/>
        <location filename="../src/interface/outils/outils.cpp" line="1141"/>
        <source>Non</source>
        <translation>いいえ</translation>
    </message>
    <message>
        <location filename="../src/interface/outils/outils.cpp" line="431"/>
        <source>Voulez-vous vraiment supprimer le groupe &lt;b&gt;%1&lt;/b&gt; ?</source>
        <translation>&lt;b&gt;%1&lt;/b&gt;グループを本当に削除しますか ?</translation>
    </message>
    <message>
        <location filename="../src/interface/outils/outils.cpp" line="708"/>
        <source>Le nom du domaine n&apos;est pas spécifié</source>
        <translation>ドメイン名が指定されていません</translation>
    </message>
    <message>
        <location filename="../src/interface/outils/outils.cpp" line="714"/>
        <source>Le nom du groupe n&apos;est pas spécifié</source>
        <translation>グループ名が指定されていません</translation>
    </message>
    <message>
        <location filename="../src/interface/outils/outils.cpp" line="720"/>
        <source>La liste de fichiers est vide</source>
        <translation>ファイルリストが空です</translation>
    </message>
    <message>
        <location filename="../src/interface/outils/outils.cpp" line="938"/>
        <location filename="../src/interface/outils/outils.cpp" line="960"/>
        <source>Ouvrir fichier TLE</source>
        <translation>TLEファイルを開く</translation>
    </message>
    <message>
        <location filename="../src/interface/outils/outils.cpp" line="940"/>
        <location filename="../src/interface/outils/outils.cpp" line="962"/>
        <location filename="../src/interface/outils/outils.cpp" line="1073"/>
        <source>Fichiers TLE (*.txt *.tle);;Tous les fichiers (*.*)</source>
        <translation>TLEファイル (*.txt *.tle);;すべてのファイル (*.*)</translation>
    </message>
    <message>
        <location filename="../src/interface/outils/outils.cpp" line="987"/>
        <source>Le nom du fichier à mettre à jour n&apos;est pas spécifié</source>
        <translation>更新するファイル名が指定されていません</translation>
    </message>
    <message>
        <location filename="../src/interface/outils/outils.cpp" line="991"/>
        <source>Le nom du fichier à lire n&apos;est pas spécifié</source>
        <translation>読むファイルの名前が指定されていません</translation>
    </message>
    <message>
        <location filename="../src/interface/outils/outils.cpp" line="1001"/>
        <source>Le fichier %1 n&apos;existe pas</source>
        <translation>ファイル「%1」が存在しません</translation>
    </message>
    <message>
        <location filename="../src/interface/outils/outils.cpp" line="1090"/>
        <source>Le fichier %1 existe déjà</source>
        <translation>ファイル「%1」は既に存在します</translation>
    </message>
    <message>
        <location filename="../src/interface/outils/outils.cpp" line="1112"/>
        <source>Le fichier %1 ne contient pas d&apos;éléments orbitaux</source>
        <translation>ファイル「%1」に軌道要素が含まれていません</translation>
    </message>
    <message>
        <location filename="../src/interface/outils/outils.cpp" line="1139"/>
        <source>Avertissement</source>
        <translation>警告</translation>
    </message>
    <message>
        <location filename="../src/interface/outils/outils.cpp" line="1139"/>
        <source>Voulez-vous vraiment supprimer les fichiers TLE sélectionnés ?</source>
        <translation>選択したTLEファイルを本当に削除しますか ?</translation>
    </message>
</context>
<context>
    <name>PreviSat</name>
    <message>
        <location filename="../src/interface/previsat.ui" line="510"/>
        <location filename="../src/interface/previsat.ui" line="577"/>
        <source>1</source>
        <translation>1</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.ui" line="515"/>
        <location filename="../src/interface/previsat.ui" line="582"/>
        <source>5</source>
        <translation>5</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.ui" line="520"/>
        <location filename="../src/interface/previsat.ui" line="587"/>
        <source>10</source>
        <translation>10</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.ui" line="525"/>
        <location filename="../src/interface/previsat.ui" line="592"/>
        <source>20</source>
        <translation>20</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.ui" line="530"/>
        <location filename="../src/interface/previsat.ui" line="597"/>
        <source>30</source>
        <translation>30</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.ui" line="535"/>
        <location filename="../src/interface/previsat.ui" line="602"/>
        <source>60</source>
        <translation>60</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.ui" line="274"/>
        <source>ISS Live</source>
        <translation>ISSライブ</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.ui" line="357"/>
        <source>Fichier :</source>
        <translation>ファイル :</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.ui" line="543"/>
        <location filename="../src/interface/previsat.ui" line="617"/>
        <location filename="../src/interface/previsat.cpp" line="2966"/>
        <location filename="../src/interface/previsat.cpp" line="2983"/>
        <source>secondes</source>
        <translation>秒</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.ui" line="440"/>
        <source>Menu</source>
        <translation>メニュー</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.ui" line="287"/>
        <source>Configuration</source>
        <translation>設定</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.ui" line="316"/>
        <source>Dons</source>
        <translation>ご寄付</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.ui" line="379"/>
        <source>Météo du lieu d&apos;observation</source>
        <translation>観測地の天気予報</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.ui" line="408"/>
        <source>Météo des bases de la NASA</source>
        <translation>NASA施設の天気予報</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.ui" line="456"/>
        <location filename="../src/interface/previsat.ui" line="1071"/>
        <source>Outils</source>
        <translation>ツール</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.ui" line="485"/>
        <location filename="../src/interface/previsat.cpp" line="1098"/>
        <source>Mode de fonctionnement</source>
        <translation>モード</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.ui" line="553"/>
        <location filename="../src/interface/previsat.cpp" line="2658"/>
        <source>Temps réel</source>
        <translation>リアルタイム</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.ui" line="563"/>
        <location filename="../src/interface/previsat.cpp" line="2661"/>
        <source>Mode manuel</source>
        <translation>手動モード</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.ui" line="245"/>
        <source>Aide</source>
        <translation>ヘルプ</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.ui" line="411"/>
        <source>NASA</source>
        <translation>NASA</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.ui" line="622"/>
        <location filename="../src/interface/previsat.cpp" line="2984"/>
        <source>minutes</source>
        <translation>分</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.ui" line="627"/>
        <location filename="../src/interface/previsat.cpp" line="2985"/>
        <source>heures</source>
        <translation>時</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.ui" line="632"/>
        <location filename="../src/interface/previsat.cpp" line="2986"/>
        <source>jours</source>
        <translation>日</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.ui" line="849"/>
        <source>Cliquer pour activer
le flux vidéo</source>
        <translation>クリックしてビデオストリームを
アクティブになります</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.ui" line="998"/>
        <source>Enregistrer</source>
        <translation>保存する</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.ui" line="1039"/>
        <source>Fichier d&apos;aide</source>
        <translation>ヘルプファイル</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.ui" line="1042"/>
        <source>F1</source>
        <translation>F1キー</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.ui" line="1138"/>
        <source>Télécharger la mise à jour</source>
        <translation>更新をダウンロードする</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.ui" line="1098"/>
        <source>À propos</source>
        <translation>PreviSatについて</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.ui" line="656"/>
        <source>Filtre</source>
        <translation>フィルター</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.ui" line="975"/>
        <source>Ctrl+O</source>
        <translation>Ctrl+O</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.ui" line="984"/>
        <source>PayPal</source>
        <translation>ペイパル</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.ui" line="993"/>
        <source>Tipeee</source>
        <translation>Tipeee</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.ui" line="1001"/>
        <source>Ctrl+S</source>
        <translation>Ctrl+S</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.ui" line="1006"/>
        <source>Mettre à jour GP courant</source>
        <translation>現在のGPを更新する</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.ui" line="1014"/>
        <source>Mettre à jour GP communs</source>
        <translation>一般的なGPを更新する</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.ui" line="1022"/>
        <source>Mettre à jour tous les groupes de GP</source>
        <translation>すべてのGPグループを更新する</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.ui" line="1059"/>
        <source>Options</source>
        <translation>オプション</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.ui" line="1074"/>
        <source>F4</source>
        <translation>F4キー</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.ui" line="1083"/>
        <source>Dons ...</source>
        <translation>ご寄付 ...</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.ui" line="1088"/>
        <source>Contact</source>
        <translation>連絡先</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.ui" line="1093"/>
        <source>Partenaires ...</source>
        <translation>パートナー ...</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.ui" line="1103"/>
        <source>Exporter fichier log ...</source>
        <translation>ログファイルをエクスポートする...</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.ui" line="1108"/>
        <source>Sky-Watcher</source>
        <translation>Sky-Watcher</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.ui" line="1113"/>
        <source>PianetaRadio.it / CatRotator</source>
        <translation>PianetaRadio.it / CatRotator</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.ui" line="1118"/>
        <source>Éléments orbitaux ...</source>
        <translation>軌道要素 ...</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.ui" line="1123"/>
        <source>celestrak.org</source>
        <translation>celestrak.org</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.ui" line="1128"/>
        <source>space-track.org</source>
        <translation>space-track.org</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.ui" line="1133"/>
        <source>Définir par défaut</source>
        <translation>デフォルトとして設定する</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.ui" line="1146"/>
        <source>Mode sombre</source>
        <translation>ダークモード</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.ui" line="1149"/>
        <source>Ctrl+N</source>
        <translation>Ctrl+N</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.ui" line="1009"/>
        <source>F5</source>
        <translation>F5キー</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.ui" line="1017"/>
        <source>Ctrl+F5</source>
        <translation>Ctrl+F5</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.ui" line="1025"/>
        <source>Shift+F5</source>
        <translation>Shift+F5</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.ui" line="1047"/>
        <source>Informations</source>
        <translation>情報</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.ui" line="1050"/>
        <source>F2</source>
        <translation>F2キー</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.ui" line="1030"/>
        <source>Mettre à jour les fichiers de données</source>
        <translation>データファイルを更新する</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.cpp" line="1080"/>
        <source>Messages</source>
        <translation>メッセージ</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.cpp" line="1104"/>
        <location filename="../src/interface/previsat.cpp" line="2680"/>
        <source>Date</source>
        <translation>日付</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.cpp" line="1110"/>
        <location filename="../src/interface/previsat.cpp" line="2681"/>
        <source>Heure</source>
        <translation>時刻</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.cpp" line="347"/>
        <source>Vérification du fichier TLE %1 ...</source>
        <translation>TLEファイル「%1」をチェックしています...</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.cpp" line="1496"/>
        <location filename="../src/interface/previsat.cpp" line="1576"/>
        <source>Une mise à jour %1 est disponible. Souhaitez-vous la télécharger?</source>
        <translation>%1更新が利用可能です。ダウンロードしますか ?</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.cpp" line="1177"/>
        <location filename="../src/interface/previsat.cpp" line="1390"/>
        <location filename="../src/interface/previsat.cpp" line="1498"/>
        <location filename="../src/interface/previsat.cpp" line="1578"/>
        <source>Information</source>
        <translation>情報</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.cpp" line="293"/>
        <source>Mise à jour automatique des éléments orbitaux</source>
        <translation>軌道要素の自動更新</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.cpp" line="1173"/>
        <source>Cette version comporte %1 lignes de code,
ce qui représente environ %2 pages.</source>
        <translation>このバージョンには%1行のコードがあり、
これは約%2ページです。</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.cpp" line="2028"/>
        <location filename="../src/interface/previsat.cpp" line="2067"/>
        <location filename="../src/interface/previsat.cpp" line="2185"/>
        <source>dddd dd MMMM yyyy  HH:mm:ss</source>
        <translation>yyyy年MM月dd日 (dddd)  HH:mm:ss</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.cpp" line="2321"/>
        <source>Mise à jour du fichier GP %1 en cours...</source>
        <translation>GPファイル「%1」を更新しています...</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.cpp" line="2327"/>
        <source>Téléchargement terminé</source>
        <translation>ダウンロードが完了しました</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.cpp" line="1391"/>
        <location filename="../src/interface/previsat.cpp" line="1499"/>
        <location filename="../src/interface/previsat.cpp" line="1579"/>
        <source>Oui</source>
        <translation>はい</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.cpp" line="1498"/>
        <source>des fichiers internes</source>
        <translation>内部ファイルの</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.cpp" line="1181"/>
        <location filename="../src/interface/previsat.cpp" line="1392"/>
        <location filename="../src/interface/previsat.cpp" line="1500"/>
        <location filename="../src/interface/previsat.cpp" line="1581"/>
        <source>Non</source>
        <translation>いいえ</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.cpp" line="1388"/>
        <source>Les éléments orbitaux sont plus vieux que %1 jour(s). Souhaitez-vous les mettre à jour?</source>
        <translation>軌道要素の年齢は%1日以上です。それらを更新しますか ?</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.cpp" line="2670"/>
        <source>Jour julien</source>
        <translation>ユリウス通日</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.cpp" line="2671"/>
        <source>Jour</source>
        <translation>日</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.cpp" line="2677"/>
        <source>dd/MM/yyyy</source>
        <comment>date format</comment>
        <translation>yyyy年MM月dd日</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.cpp" line="2352"/>
        <source>Mise à jour du groupe d&apos;éléments orbitaux &quot;%1&quot;...</source>
        <translation>軌道要素グループ「%1」を更新しています...</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.cpp" line="2387"/>
        <source>Mise à jour du groupe d&apos;éléments orbitaux &quot;%1&quot; terminée</source>
        <translation>軌道要素グループ「%1」の更新が完了しました</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.cpp" line="2875"/>
        <source>Impossible d&apos;afficher l&apos;aide en ligne</source>
        <translation>オンラインヘルプを表示できません</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.ui" line="972"/>
        <location filename="../src/interface/previsat.cpp" line="3153"/>
        <source>Importer fichier GP / TLE...</source>
        <translation>GP・TLEファイルをインポートする...</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.cpp" line="3155"/>
        <source>Fichiers GP (*.xml);;Fichiers TLE (*.txt *.tle);;Tous les fichiers (*.*)</source>
        <translation>GPファイル (*.xml);;TLEファイル (*.txt *.tle);;すべてのファイル (*.*)</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.cpp" line="3173"/>
        <location filename="../src/interface/previsat.cpp" line="3233"/>
        <source>Le fichier %1 ne contient pas d&apos;éléments orbitaux</source>
        <translation>ファイル「%1」に軌道要素が含まれていません</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.cpp" line="3182"/>
        <location filename="../src/interface/previsat.cpp" line="3212"/>
        <source>Le fichier %1 existe déjà</source>
        <translation>ファイル「%1」は既に存在します</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.cpp" line="3265"/>
        <source>Fichiers texte (*.txt);;Tous les fichiers (*.*)</source>
        <translation>テキストファイル (*.txt);;すべてのファイル (*.*)</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.cpp" line="3439"/>
        <source>Commun</source>
        <comment>common orbital elements groups</comment>
        <translation>一般的</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.cpp" line="3444"/>
        <source>Tous</source>
        <comment>all orbital elements groups</comment>
        <translation>すべて</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.cpp" line="3529"/>
        <location filename="../src/interface/previsat.cpp" line="3532"/>
        <source>Devise</source>
        <translation>通貨</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.cpp" line="3530"/>
        <location filename="../src/interface/previsat.cpp" line="3532"/>
        <source>Choisissez la devise :</source>
        <translation>通貨を選んでください ：</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.cpp" line="3546"/>
        <source>Attention : Il est possible d&apos;effectuer un don PayPal via Tipeee, mais ceci induira des frais supplémentaires</source>
        <translation>ご注意 : Tipeeeでペイパルのご寄付をすることは可能ですが、これには追加料金がかかります</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.cpp" line="3613"/>
        <source>Ouverture du fichier d&apos;éléments orbitaux %1 ...</source>
        <translation>軌道要素ファイル「%1」を読み込んでいます ...</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.cpp" line="3258"/>
        <source>onglet_general</source>
        <comment>file name (without accent)</comment>
        <translation>main_tab</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.cpp" line="1176"/>
        <source>Vous venez de mettre à jour %1.
%2Souhaitez-vous faire un don pour soutenir son auteur ?</source>
        <translation>%1を更新しました。
%2作者を支援したい場合は寄付をご検討ください</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.cpp" line="1875"/>
        <source>Fichiers PNG (*.png);;Fichiers JPEG (*.jpg *.jpeg);;Fichiers BMP (*.bmp);;Tous les fichiers (*.*)</source>
        <translation>PNGファイル (*.png);;JPEGファイル (*.jpg *.jpeg);;BMPファイル (*.bmp);;すべてのファイル (*.*)</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.cpp" line="2966"/>
        <location filename="../src/interface/previsat.cpp" line="2983"/>
        <source>seconde</source>
        <translation>秒</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.cpp" line="2984"/>
        <source>minute</source>
        <translation>分</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.cpp" line="2985"/>
        <source>heure</source>
        <translation>時</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.cpp" line="2986"/>
        <source>jour</source>
        <translation>日</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.cpp" line="3003"/>
        <source>Agrandir</source>
        <translation>拡大する</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.cpp" line="3012"/>
        <source>Réduire</source>
        <translation>縮小する</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.cpp" line="3038"/>
        <source>Carte du monde</source>
        <translation>世界地図</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.cpp" line="3046"/>
        <source>Carte du ciel</source>
        <translation>星図</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.cpp" line="3259"/>
        <source>onglet_elements</source>
        <comment>file name (without accent)</comment>
        <translation>elements_tab</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.cpp" line="3259"/>
        <source>onglet_informations</source>
        <comment>file name (without accent)</comment>
        <translation>information_tab</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.cpp" line="1874"/>
        <location filename="../src/interface/previsat.cpp" line="3264"/>
        <source>Enregistrer sous...</source>
        <translation>名前を付けて保存...</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.cpp" line="1724"/>
        <source>&lt;font color=&apos;blue&apos;&gt;&lt;b&gt;%1&lt;/b&gt;&lt;/font&gt;&lt;br /&gt;NORAD : &lt;b&gt;%2&lt;/b&gt;&lt;br /&gt;COSPAR : &lt;b&gt;%3&lt;/b&gt;</source>
        <translation>&lt;font color=&apos;blue&apos;&gt;&lt;b&gt;%1&lt;/b&gt;&lt;/font&gt;&lt;br /&gt;NORAD : &lt;b&gt;%2&lt;/b&gt;&lt;br /&gt;COSPAR : &lt;b&gt;%3&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.cpp" line="3335"/>
        <source>Pas d&apos;informations à afficher</source>
        <translation>表示する情報がありません</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.cpp" line="3615"/>
        <source>Fichier d&apos;éléments orbitaux de %1 satellites</source>
        <translation>%1基衛星の軌道要素が含まれているファイル</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.cpp" line="3783"/>
        <source>&lt;b&gt;%1&lt;/b&gt; (numéro NORAD : &lt;b&gt;%2&lt;/b&gt;  -  COSPAR : %3)</source>
        <translation>&lt;b&gt;%1&lt;/b&gt; (NORAD番号 : &lt;b&gt;%2&lt;/b&gt;  -  COSPAR : %3)</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../src/configuration/fichierobs.cpp" line="166"/>
        <source>Le fichier ne contient pas de lieux d&apos;observation</source>
        <translation>ファイルには観測地が含まれていません</translation>
    </message>
    <message>
        <location filename="../src/configuration/configuration.cpp" line="864"/>
        <location filename="../src/configuration/configuration.cpp" line="903"/>
        <location filename="../src/configuration/gestionnairexml.cpp" line="366"/>
        <location filename="../src/configuration/gestionnairexml.cpp" line="459"/>
        <location filename="../src/configuration/gestionnairexml.cpp" line="593"/>
        <location filename="../src/configuration/gestionnairexml.cpp" line="705"/>
        <location filename="../src/configuration/gestionnairexml.cpp" line="909"/>
        <location filename="../src/configuration/gestionnairexml.cpp" line="1067"/>
        <location filename="../src/configuration/gestionnairexml.cpp" line="1161"/>
        <location filename="../src/configuration/gestionnairexml.cpp" line="1251"/>
        <location filename="../src/configuration/gestionnairexml.cpp" line="1345"/>
        <location filename="../src/librairies/corps/corps.cpp" line="680"/>
        <source>Le fichier %1 n&apos;existe pas ou est vide, veuillez réinstaller %2</source>
        <translation>ファイル「%1」が存在しないまたはファイルが空ので、「%2」を再インストールしてください</translation>
    </message>
    <message>
        <location filename="../src/configuration/configuration.cpp" line="876"/>
        <location filename="../src/configuration/configuration.cpp" line="917"/>
        <location filename="../src/configuration/evenementsstationspatiale.cpp" line="116"/>
        <location filename="../src/configuration/gestionnairexml.cpp" line="411"/>
        <location filename="../src/configuration/gestionnairexml.cpp" line="560"/>
        <location filename="../src/configuration/gestionnairexml.cpp" line="821"/>
        <location filename="../src/configuration/gestionnairexml.cpp" line="954"/>
        <location filename="../src/configuration/gestionnairexml.cpp" line="1124"/>
        <location filename="../src/configuration/gestionnairexml.cpp" line="1214"/>
        <location filename="../src/configuration/gestionnairexml.cpp" line="1308"/>
        <location filename="../src/configuration/gestionnairexml.cpp" line="1411"/>
        <location filename="../src/librairies/corps/corps.cpp" line="670"/>
        <location filename="../src/librairies/dates/date.cpp" line="445"/>
        <source>Erreur lors de la lecture du fichier %1, veuillez réinstaller %2</source>
        <translation>ファイル「%1」の読み込み中にエラーが発生しましたので、「%2」を再インストールしてください</translation>
    </message>
    <message>
        <location filename="../src/configuration/configuration.cpp" line="1016"/>
        <source>Le répertoire %1 n&apos;existe pas, veuillez réinstaller %2</source>
        <translation>フォルダ「%1」が存在しないので、「%2」を再インストールしてください</translation>
    </message>
    <message>
        <location filename="../src/configuration/configuration.cpp" line="1081"/>
        <location filename="../src/configuration/gestionnairexml.cpp" line="1443"/>
        <source>Le fichier %1 n&apos;existe pas, veuillez réinstaller %2</source>
        <translation>ファイル「%1」が存在しないので、「%2」を再インストールしてください</translation>
    </message>
    <message>
        <location filename="../src/configuration/configuration.cpp" line="1088"/>
        <source>Le fichier %1 est vide, veuillez réinstaller %2</source>
        <translation>ファイル「%1」が空なので、「%2」を再インストールしてください</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="648"/>
        <source>Lieu d&apos;observation              : %1     %2 %3   %4 %5   %6 %7</source>
        <translation>観測地        : %1     %2 %3   %4 %5   %6 %7</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="650"/>
        <source>Ouest</source>
        <translation>西</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="650"/>
        <source>Est</source>
        <translation>東</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="653"/>
        <source>Nord</source>
        <translation>北</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="653"/>
        <source>Sud</source>
        <translation>南</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="655"/>
        <location filename="../src/interface/afficherresultats.cpp" line="657"/>
        <source>km</source>
        <comment>kilometer</comment>
        <translation>km</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="657"/>
        <source>m</source>
        <comment>meter</comment>
        <translation>m</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="657"/>
        <source>ft</source>
        <comment>foot</comment>
        <translation>ft</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="664"/>
        <source>Fuseau horaire                  : %1</source>
        <translation>時間帯        : %1</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="679"/>
        <source>Conditions d&apos;observations       :</source>
        <translation>観測条件      :</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="665"/>
        <source>UTC</source>
        <comment>Universal Time Coordinated</comment>
        <translation>UTC</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="680"/>
        <source>Hauteur minimale du satellite = %1°</source>
        <translation>最小衛星仰角 = %1°</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="686"/>
        <source>Hauteur maximale du Soleil = %1°</source>
        <translation>最大太陽仰角 = %1°</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="888"/>
        <location filename="../src/interface/afficherresultats.cpp" line="1152"/>
        <source>W</source>
        <comment>West</comment>
        <translation>西</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="888"/>
        <location filename="../src/interface/afficherresultats.cpp" line="1152"/>
        <source>E</source>
        <comment>East</comment>
        <translation>東</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="889"/>
        <location filename="../src/interface/afficherresultats.cpp" line="1153"/>
        <source>N</source>
        <comment>North</comment>
        <translation>北</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="889"/>
        <location filename="../src/interface/afficherresultats.cpp" line="1153"/>
        <source>S</source>
        <comment>South</comment>
        <translation>南</translation>
    </message>
    <message>
        <location filename="../src/librairies/corps/corps.cpp" line="165"/>
        <source>Tableau de constellations vide</source>
        <translation>星座の表が空です</translation>
    </message>
    <message>
        <location filename="../src/librairies/corps/etoiles/constellation.cpp" line="101"/>
        <source>Le tableau de constellations n&apos;est pas initialisé</source>
        <translation>星座の表が初期化されていません</translation>
    </message>
    <message>
        <location filename="../src/librairies/corps/etoiles/etoile.cpp" line="103"/>
        <location filename="../src/librairies/corps/etoiles/ligneconstellation.cpp" line="92"/>
        <source>Le tableau d&apos;étoiles n&apos;est pas initialisé</source>
        <translation>星の表が初期化されていません</translation>
    </message>
    <message>
        <location filename="../src/librairies/corps/etoiles/ligneconstellation.cpp" line="88"/>
        <source>Le tableau de lignes de constellation n&apos;est pas initialisé</source>
        <translation>星座線の表が初期化されていません</translation>
    </message>
    <message>
        <location filename="../src/librairies/corps/satellite/tle.cpp" line="480"/>
        <source>Le fichier %1 n&apos;est pas valide</source>
        <translation>ファイル「%1」が無効です</translation>
    </message>
    <message>
        <location filename="../src/librairies/corps/satellite/tle.cpp" line="494"/>
        <source>Le fichier %1 n&apos;existe pas</source>
        <translation>ファイル「%1」が存在しません</translation>
    </message>
    <message>
        <location filename="../src/librairies/corps/satellite/tle.cpp" line="499"/>
        <source>Le fichier %1 est vide</source>
        <translation>ファイル「%1」が空です</translation>
    </message>
    <message>
        <location filename="../src/librairies/corps/satellite/gpformat.cpp" line="119"/>
        <location filename="../src/librairies/corps/satellite/gpformat.cpp" line="199"/>
        <location filename="../src/librairies/corps/satellite/tle.cpp" line="156"/>
        <source>Le fichier %1 n&apos;existe pas ou est vide</source>
        <translation>ファイル「%1」が存在しないまたはファイルが空です</translation>
    </message>
    <message>
        <location filename="../src/librairies/corps/satellite/gpformat.cpp" line="130"/>
        <location filename="../src/librairies/corps/satellite/gpformat.cpp" line="142"/>
        <location filename="../src/librairies/corps/satellite/gpformat.cpp" line="210"/>
        <location filename="../src/librairies/corps/satellite/gpformat.cpp" line="222"/>
        <location filename="../src/librairies/corps/satellite/tle.cpp" line="505"/>
        <source>Le fichier %1 ne contient aucun satellite</source>
        <translation>ファイル「%1」には衛星が含まれていません</translation>
    </message>
    <message>
        <location filename="../src/librairies/corps/satellite/tle.cpp" line="364"/>
        <source>Le satellite %1 (numéro NORAD : %2) n&apos;existe pas dans le fichier à mettre à jour.
Voulez-vous ajouter ce TLE dans le fichier à mettre à jour ?</source>
        <translation>衛星「%1」(NORAD番号 : %2)は更新するファイルに存在しません。
このTLEを更新するファイルに追加しますか ?</translation>
    </message>
    <message>
        <location filename="../src/librairies/corps/satellite/tle.cpp" line="367"/>
        <source>Ajout du nouveau TLE</source>
        <translation>新しいTLEを追加する</translation>
    </message>
    <message>
        <location filename="../src/configuration/gestionnairexml.cpp" line="449"/>
        <source>Le fichier de configuration de %1 a évolué.
Souhaitez-vous tenter de récupérer les lieux d&apos;observation ?</source>
        <translation>設定ファイル「%1」の形式が変更されました。
観測地の回復を試みますか ?</translation>
    </message>
    <message>
        <location filename="../src/configuration/gestionnairexml.cpp" line="1501"/>
        <location filename="../src/librairies/corps/satellite/tle.cpp" line="337"/>
        <location filename="../src/librairies/corps/satellite/tle.cpp" line="370"/>
        <source>Oui</source>
        <translation>はい</translation>
    </message>
    <message>
        <location filename="../src/librairies/corps/satellite/tle.cpp" line="338"/>
        <location filename="../src/librairies/corps/satellite/tle.cpp" line="371"/>
        <source>Oui à tout</source>
        <translation>すべてにはい</translation>
    </message>
    <message>
        <location filename="../src/configuration/gestionnairexml.cpp" line="1502"/>
        <location filename="../src/librairies/corps/satellite/tle.cpp" line="339"/>
        <location filename="../src/librairies/corps/satellite/tle.cpp" line="372"/>
        <source>Non</source>
        <translation>いいえ</translation>
    </message>
    <message>
        <location filename="../src/librairies/corps/satellite/tle.cpp" line="340"/>
        <location filename="../src/librairies/corps/satellite/tle.cpp" line="373"/>
        <source>Non à tout</source>
        <translation>すべてにいいえ</translation>
    </message>
    <message>
        <location filename="../src/librairies/corps/satellite/tle.cpp" line="331"/>
        <source>Le satellite %1 (numéro NORAD : %2) n&apos;existe pas dans le fichier de TLE récents.
Voulez-vous supprimer ce TLE du fichier à mettre à jour ?</source>
        <translation>衛星「%1」(NORAD番号 : %2)は最近のTLEファイルに存在しません。
更新するファイルからこのTLEを削除しますか ?</translation>
    </message>
    <message>
        <location filename="../src/librairies/corps/satellite/tle.cpp" line="266"/>
        <location filename="../src/librairies/corps/satellite/tle.cpp" line="276"/>
        <source>Le fichier %1 n&apos;est pas un TLE</source>
        <translation>ファイル「%1」はTLEファイルではありません</translation>
    </message>
    <message>
        <location filename="../src/librairies/corps/satellite/tle.cpp" line="334"/>
        <source>Suppression du TLE</source>
        <translation>TLEの削除</translation>
    </message>
    <message>
        <location filename="../src/librairies/corps/satellite/tle.cpp" line="582"/>
        <source>Une des lignes du TLE est vide</source>
        <translation>TLEの1行が空です</translation>
    </message>
    <message>
        <location filename="../src/librairies/corps/satellite/tle.cpp" line="589"/>
        <source>Les numéros de ligne du TLE du satellite %1 (numéro NORAD : %2 ) sont incorrects</source>
        <translation>衛星「%1」(NORAD番号 : %2)のTLEは行番号が正しくありません</translation>
    </message>
    <message>
        <location filename="../src/librairies/corps/satellite/tle.cpp" line="595"/>
        <source>La longueur des lignes du TLE du satellite %1 (numéro NORAD : %2) est incorrecte</source>
        <translation>衛星「%1」(NORAD番号 : %2)のTLEは行の長さが正しくありません</translation>
    </message>
    <message>
        <location filename="../src/librairies/corps/satellite/tle.cpp" line="605"/>
        <source>Erreur position des espaces du TLE :
Satellite %1 - numéro NORAD : %2</source>
        <translation>TLEの空白文字の位置によるエラー :
衛星「%1」 - NORAD番号 : %2</translation>
    </message>
    <message>
        <location filename="../src/librairies/corps/satellite/tle.cpp" line="613"/>
        <source>Erreur Ponctuation du TLE :
Satellite %1 - numéro NORAD : %2</source>
        <translation>TLEの約物によるエラー :
衛星「%1」 - NORAD番号 : %2</translation>
    </message>
    <message>
        <location filename="../src/librairies/corps/satellite/tle.cpp" line="620"/>
        <source>Les deux lignes du TLE du satellite %1 ont des numéros NORAD différents (%2 et %3)</source>
        <translation>衛星「%1」のTLEの2行はNORAD番号が異なります (%2と%3)</translation>
    </message>
    <message>
        <location filename="../src/librairies/corps/satellite/tle.cpp" line="626"/>
        <source>Erreur CheckSum ligne 1 :
Satellite %1 - numéro NORAD : %2</source>
        <translation>1行目のチェックサムが正しくありません :
衛星「%1」 - NORAD番号 : %2</translation>
    </message>
    <message>
        <location filename="../src/librairies/corps/satellite/tle.cpp" line="631"/>
        <source>Erreur CheckSum ligne 2 :
Satellite %1 - numéro NORAD : %2</source>
        <translation>2行目のチェックサムが正しくありません :
衛星「%1」 - NORAD番号 : %2</translation>
    </message>
    <message>
        <location filename="../src/librairies/corps/systemesolaire/lune.cpp" line="267"/>
        <location filename="../test/src/librairies/corps/systemesolaire/lunetest.cpp" line="119"/>
        <source>Nouvelle Lune</source>
        <translation>ニュームーン</translation>
    </message>
    <message>
        <location filename="../src/librairies/corps/systemesolaire/lune.cpp" line="271"/>
        <source>Premier croissant</source>
        <translation>クレセントムーン</translation>
    </message>
    <message>
        <location filename="../src/librairies/corps/systemesolaire/lune.cpp" line="271"/>
        <location filename="../test/src/librairies/corps/systemesolaire/lunetest.cpp" line="108"/>
        <source>Dernier croissant</source>
        <translation>バルサミックムーン</translation>
    </message>
    <message>
        <location filename="../src/librairies/corps/systemesolaire/lune.cpp" line="275"/>
        <location filename="../test/src/librairies/corps/systemesolaire/lunetest.cpp" line="86"/>
        <source>Premier quartier</source>
        <translation>ファーストクォーター</translation>
    </message>
    <message>
        <location filename="../src/librairies/corps/systemesolaire/lune.cpp" line="275"/>
        <source>Dernier quartier</source>
        <translation>サードクォーター</translation>
    </message>
    <message>
        <location filename="../src/librairies/corps/systemesolaire/lune.cpp" line="279"/>
        <location filename="../test/src/librairies/corps/systemesolaire/lunetest.cpp" line="97"/>
        <source>Gibbeuse croissante</source>
        <translation>ギバウスムーン</translation>
    </message>
    <message>
        <location filename="../src/librairies/corps/systemesolaire/lune.cpp" line="279"/>
        <source>Gibbeuse décroissante</source>
        <translation>ディセミネイティングムーン</translation>
    </message>
    <message>
        <location filename="../src/librairies/corps/systemesolaire/lune.cpp" line="283"/>
        <location filename="../test/src/librairies/corps/systemesolaire/lunetest.cpp" line="130"/>
        <source>Pleine Lune</source>
        <translation>フルムーン</translation>
    </message>
    <message>
        <location filename="../src/librairies/dates/date.cpp" line="516"/>
        <source>dd/MM/yyyy</source>
        <comment>Date format</comment>
        <translation>yyyy年MM月dd日</translation>
    </message>
    <message>
        <location filename="../src/librairies/dates/date.cpp" line="295"/>
        <source>Date au format ISO vide</source>
        <translation>ISO形式の日時が空です</translation>
    </message>
    <message>
        <location filename="../src/librairies/dates/date.cpp" line="304"/>
        <source>Date au format ISO invalide</source>
        <translation>ISO形式の日時が無効です</translation>
    </message>
    <message>
        <location filename="../src/librairies/dates/date.cpp" line="342"/>
        <source>Date au format NASA vide</source>
        <translation>NASA形式の日時が空です</translation>
    </message>
    <message>
        <location filename="../src/librairies/dates/date.cpp" line="351"/>
        <location filename="../src/librairies/dates/date.cpp" line="360"/>
        <location filename="../src/librairies/dates/date.cpp" line="374"/>
        <source>Date au format NASA invalide</source>
        <translation>NASA形式の日時が無効です</translation>
    </message>
    <message>
        <location filename="../src/librairies/dates/date.cpp" line="409"/>
        <source>Le fichier taiutc.dat n&apos;existe pas</source>
        <translation>taiutc.datファイルが存在しません</translation>
    </message>
    <message>
        <location filename="../src/librairies/dates/date.cpp" line="474"/>
        <source>dddd dd MMMM yyyy HH:mm:ss</source>
        <comment>Date format</comment>
        <translation>yyyy年MM月dd日 (dddd)  HH:mm:ss</translation>
    </message>
    <message>
        <location filename="../src/librairies/dates/date.cpp" line="691"/>
        <source>Ecarts TAI-UTC non initialisés</source>
        <translation>TAIとUTCの差が初期化されていません</translation>
    </message>
    <message>
        <location filename="../src/librairies/exceptions/message.cpp" line="75"/>
        <source>Information</source>
        <translation>情報</translation>
    </message>
    <message>
        <location filename="../src/configuration/gestionnairexml.cpp" line="1500"/>
        <location filename="../src/librairies/exceptions/message.cpp" line="80"/>
        <source>Avertissement</source>
        <translation>警告</translation>
    </message>
    <message>
        <location filename="../src/librairies/exceptions/message.cpp" line="85"/>
        <source>Erreur</source>
        <translation>エラー</translation>
    </message>
    <message>
        <location filename="../src/librairies/maths/maths.cpp" line="201"/>
        <source>h</source>
        <comment>hour (angle)</comment>
        <translation>h</translation>
    </message>
    <message>
        <location filename="../src/librairies/maths/maths.cpp" line="202"/>
        <source>m</source>
        <comment>minute (angle)</comment>
        <translation>m</translation>
    </message>
    <message>
        <location filename="../src/librairies/maths/maths.cpp" line="203"/>
        <source>s</source>
        <comment>second (angle)</comment>
        <translation>s</translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="96"/>
        <source>Une instance de %1 est déjà ouverte</source>
        <translation>%1のインスタンスは既に実行中です</translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="151"/>
        <source>Initialisation de la configuration...</source>
        <translation>構成を初期化しています...</translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="167"/>
        <source>Ouverture du fichier GP...</source>
        <translation>GPファイルを読み込んでいます...</translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="173"/>
        <source>Mise à jour des éléments orbitaux...</source>
        <translation>軌道要素を更新しています...</translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="179"/>
        <source>Démarrage de l&apos;application...</source>
        <translation>アプリが起動しています...</translation>
    </message>
    <message>
        <location filename="../src/previsions/evenementsorbitaux.cpp" line="237"/>
        <source>Noeud Ascendant - PSO = 0°</source>
        <comment>In orbit position</comment>
        <translation>昇交点 (位置 = 0°)</translation>
    </message>
    <message>
        <location filename="../src/previsions/evenementsorbitaux.cpp" line="238"/>
        <source>Noeud Descendant - PSO = 180°</source>
        <comment>In orbit position</comment>
        <translation>降交点 (位置 = 180°)</translation>
    </message>
    <message>
        <location filename="../src/previsions/evenementsorbitaux.cpp" line="265"/>
        <source>Pénombre -&gt; Ombre</source>
        <translation>半影 -&gt; 本影</translation>
    </message>
    <message>
        <location filename="../src/previsions/evenementsorbitaux.cpp" line="265"/>
        <source>Ombre -&gt; Pénombre</source>
        <translation>本影 -&gt; 半影</translation>
    </message>
    <message>
        <location filename="../src/previsions/evenementsorbitaux.cpp" line="284"/>
        <source>Lumière -&gt; Pénombre</source>
        <translation>太陽光 -&gt; 半影</translation>
    </message>
    <message>
        <location filename="../src/previsions/evenementsorbitaux.cpp" line="284"/>
        <source>Pénombre -&gt; Lumière</source>
        <translation>半影 -&gt; 太陽光</translation>
    </message>
    <message>
        <location filename="../src/previsions/evenementsorbitaux.cpp" line="306"/>
        <source>Pénombre -&gt; Ombre (Lune)</source>
        <translation>半影 -&gt; 本影 (月)</translation>
    </message>
    <message>
        <location filename="../src/previsions/evenementsorbitaux.cpp" line="306"/>
        <source>Ombre -&gt; Pénombre (Lune)</source>
        <translation>本影 -&gt; 半影 (月)</translation>
    </message>
    <message>
        <location filename="../src/previsions/evenementsorbitaux.cpp" line="325"/>
        <source>Lumière -&gt; Pénombre (Lune)</source>
        <translation>太陽光 -&gt; 半影 (月)</translation>
    </message>
    <message>
        <location filename="../src/previsions/evenementsorbitaux.cpp" line="325"/>
        <source>Pénombre -&gt; Lumière (Lune)</source>
        <translation>半影 -&gt; 太陽光 (月)</translation>
    </message>
    <message>
        <location filename="../src/previsions/evenementsorbitaux.cpp" line="366"/>
        <source>nmi</source>
        <comment>nautical mile</comment>
        <translation>nmi</translation>
    </message>
    <message>
        <location filename="../src/previsions/evenementsorbitaux.cpp" line="371"/>
        <source>Périgée : %1%2 (%3%2)</source>
        <translation>近地点 : %1%2 (%3%2)</translation>
    </message>
    <message>
        <location filename="../src/previsions/evenementsorbitaux.cpp" line="371"/>
        <source>Apogée : %1%2 (%3%2)</source>
        <translation>遠地点 : %1%2 (%3%2)</translation>
    </message>
    <message>
        <location filename="../src/previsions/evenementsorbitaux.cpp" line="392"/>
        <source>Passage terminateur Jour -&gt; Nuit</source>
        <translation>明暗境界線の通過 (昼 -&gt; 夜)</translation>
    </message>
    <message>
        <location filename="../src/previsions/evenementsorbitaux.cpp" line="393"/>
        <source>Passage terminateur Nuit -&gt; Jour</source>
        <translation>明暗境界線の通過 (夜 -&gt; 昼)</translation>
    </message>
    <message>
        <location filename="../src/previsions/evenementsorbitaux.cpp" line="417"/>
        <source>Passage à PSO = %1°</source>
        <comment>In orbit position</comment>
        <translation>位置が%1°のとき通過</translation>
    </message>
    <message>
        <location filename="../src/previsions/flashs.cpp" line="76"/>
        <source>FCB</source>
        <comment>Front, Central, Backward panels of MetOp satellite</comment>
        <translation>FCB</translation>
    </message>
    <message>
        <location filename="../src/librairies/corps/satellite/evenements.cpp" line="75"/>
        <location filename="../test/src/librairies/corps/satellite/evenementstest.cpp" line="102"/>
        <location filename="../test/src/librairies/corps/satellite/evenementstest.cpp" line="114"/>
        <source>LOS</source>
        <comment>Loss of signal</comment>
        <translation>LOS</translation>
    </message>
    <message>
        <location filename="../src/librairies/corps/satellite/evenements.cpp" line="107"/>
        <location filename="../src/librairies/corps/satellite/evenementsconst.h" line="73"/>
        <location filename="../test/src/librairies/corps/satellite/evenementstest.cpp" line="83"/>
        <location filename="../test/src/librairies/corps/satellite/evenementstest.cpp" line="95"/>
        <source>AOS</source>
        <comment>Acquisition of signal</comment>
        <translation>AOS</translation>
    </message>
    <message>
        <location filename="../src/librairies/corps/satellite/donnees.cpp" line="86"/>
        <source>N/A</source>
        <translation>該当なし</translation>
    </message>
</context>
<context>
    <name>Radar</name>
    <message>
        <location filename="../src/interface/radar/radar.ui" line="58"/>
        <location filename="../src/interface/radar/radar.cpp" line="347"/>
        <location filename="../src/interface/radar/radar.cpp" line="350"/>
        <source>Nord</source>
        <translation>北</translation>
    </message>
    <message>
        <location filename="../src/interface/radar/radar.ui" line="73"/>
        <location filename="../src/interface/radar/radar.cpp" line="357"/>
        <location filename="../src/interface/radar/radar.cpp" line="360"/>
        <source>Ouest</source>
        <translation>西</translation>
    </message>
    <message>
        <location filename="../src/interface/radar/radar.ui" line="114"/>
        <location filename="../src/interface/radar/radar.cpp" line="356"/>
        <location filename="../src/interface/radar/radar.cpp" line="361"/>
        <source>Est</source>
        <translation>東</translation>
    </message>
    <message>
        <location filename="../src/interface/radar/radar.ui" line="126"/>
        <location filename="../src/interface/radar/radar.cpp" line="346"/>
        <location filename="../src/interface/radar/radar.cpp" line="351"/>
        <source>Sud</source>
        <translation>南</translation>
    </message>
    <message>
        <location filename="../src/interface/radar/radar.cpp" line="139"/>
        <source>Azimut : %1°</source>
        <translation>方位角 : %1°</translation>
    </message>
    <message>
        <location filename="../src/interface/radar/radar.cpp" line="140"/>
        <source>Hauteur : %1°</source>
        <translation>仰角 : %1°</translation>
    </message>
    <message>
        <location filename="../src/interface/radar/radar.cpp" line="158"/>
        <source>&lt;font color=&apos;blue&apos;&gt;&lt;b&gt;%1&lt;/b&gt;&lt;/font&gt;&lt;br /&gt;NORAD : &lt;b&gt;%2&lt;/b&gt;&lt;br /&gt;COSPAR : &lt;b&gt;%3&lt;/b&gt;</source>
        <translation>&lt;font color=&apos;blue&apos;&gt;&lt;b&gt;%1&lt;/b&gt;&lt;/font&gt;&lt;br /&gt;NORAD : &lt;b&gt;%2&lt;/b&gt;&lt;br /&gt;COSPAR : &lt;b&gt;%3&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../src/interface/radar/radar.cpp" line="161"/>
        <source>&lt;b&gt;%1&lt;/b&gt; (numéro NORAD : &lt;b&gt;%2&lt;/b&gt;  -  COSPAR : &lt;b&gt;%3&lt;/b&gt;)</source>
        <translation>&lt;b&gt;%1&lt;/b&gt; (NORAD番号 : &lt;b&gt;%2&lt;/b&gt;  -  COSPAR : &lt;b&gt;%3&lt;/b&gt;)</translation>
    </message>
    <message>
        <location filename="../src/interface/radar/radar.cpp" line="188"/>
        <location filename="../src/interface/radar/radar.cpp" line="189"/>
        <source>Soleil</source>
        <translation>太陽</translation>
    </message>
    <message>
        <location filename="../src/interface/radar/radar.cpp" line="215"/>
        <location filename="../src/interface/radar/radar.cpp" line="216"/>
        <source>Lune</source>
        <translation>月</translation>
    </message>
</context>
<context>
    <name>RechercheSatellite</name>
    <message>
        <location filename="../src/interface/onglets/donnees/recherchesatellite.ui" line="54"/>
        <location filename="../src/interface/onglets/donnees/recherchesatellite.cpp" line="228"/>
        <source>Objets trouvés :</source>
        <translation>見つかったオブジェクト :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/recherchesatellite.ui" line="82"/>
        <source>Entrez au minimum 3 lettres du nom de l&apos;objet</source>
        <translation>オブジェクト名を3文字以上入力してください</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/recherchesatellite.ui" line="95"/>
        <location filename="../src/interface/onglets/donnees/recherchesatellite.ui" line="187"/>
        <source>Nom :</source>
        <translation>名称 :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/recherchesatellite.ui" line="221"/>
        <source>Numéro NORAD :</source>
        <translation>NORAD番号 :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/recherchesatellite.ui" line="234"/>
        <source>Désignation COSPAR :</source>
        <translation>国際衛星識別符号 :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/recherchesatellite.ui" line="302"/>
        <source>Magnitude std/max :</source>
        <translation>標準・最大等級 :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/recherchesatellite.ui" line="315"/>
        <source>Modèle orbital :</source>
        <translation>軌道モデル :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/recherchesatellite.ui" line="328"/>
        <source>Dimensions/Section :</source>
        <translation>大きさ・断面積 :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/recherchesatellite.ui" line="435"/>
        <source>Date de lancement :</source>
        <translation>打ち上げ日 :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/recherchesatellite.ui" line="445"/>
        <source>Date de rentrée :</source>
        <translation>再突入日付 :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/recherchesatellite.ui" line="455"/>
        <source>Catégorie d&apos;orbite :</source>
        <translation>軌道種類 :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/recherchesatellite.ui" line="465"/>
        <source>Pays/Organisation :</source>
        <translation>国/組織 :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/recherchesatellite.ui" line="475"/>
        <source>Site de lancement :</source>
        <translation>発射場 :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/recherchesatellite.ui" line="568"/>
        <source>Apogée (Altitude) :</source>
        <translation>遠地点 (高度) :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/recherchesatellite.ui" line="578"/>
        <source>Périgée (Altitude) :</source>
        <translation>近地点 (高度) :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/recherchesatellite.ui" line="588"/>
        <source>Période orbitale :</source>
        <translation>軌道周期 :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/recherchesatellite.ui" line="601"/>
        <source>Inclinaison :</source>
        <translation>軌道傾斜角 :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/recherchesatellite.ui" line="681"/>
        <source>Fichiers :</source>
        <translation>ファイル :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/recherchesatellite.ui" line="739"/>
        <source>COSPAR :</source>
        <translation>COSPAR :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/recherchesatellite.ui" line="783"/>
        <source>NORAD :</source>
        <translation>NORAD :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/recherchesatellite.cpp" line="116"/>
        <source>Problème de droits d&apos;écriture du fichier %1</source>
        <translation>ファイル「%1」の書き込み権限に問題があります</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/recherchesatellite.cpp" line="128"/>
        <source>Nom                :</source>
        <translation>名称 :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/recherchesatellite.cpp" line="130"/>
        <source>Numéro NORAD       : %1		Magnitude std/max  : %2</source>
        <comment>Standard/Maximum magnitude</comment>
        <translation>NORAD番号       : %1			標準・最大等級  : %2</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/recherchesatellite.cpp" line="133"/>
        <source>Désignation COSPAR : %1		Modèle orbital     : %2</source>
        <translation>国際衛星識別符号 : %1		軌道モデル      : %2</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/recherchesatellite.cpp" line="136"/>
        <source>Dimensions/Section : %1%2</source>
        <translation>大きさ・断面積     : %1%2</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/recherchesatellite.cpp" line="137"/>
        <location filename="../src/interface/onglets/donnees/recherchesatellite.cpp" line="495"/>
        <source>Inconnues</source>
        <translation>不明</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/recherchesatellite.cpp" line="140"/>
        <source>Date de lancement  : %1		Apogée  (Altitude) : %2</source>
        <translation>打ち上げ日 : %1			遠地点 (高度) : %2</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/recherchesatellite.cpp" line="143"/>
        <source>Date de rentrée    : %1		</source>
        <translation>再突入日付    : %1			</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/recherchesatellite.cpp" line="144"/>
        <location filename="../src/interface/onglets/donnees/recherchesatellite.cpp" line="147"/>
        <source>Catégorie d&apos;orbite : %1		</source>
        <translation>軌道種類  : %1				</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/recherchesatellite.cpp" line="145"/>
        <source>Périgée (Altitude) : %1</source>
        <translation>近地点 (高度) : %1</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/recherchesatellite.cpp" line="148"/>
        <location filename="../src/interface/onglets/donnees/recherchesatellite.cpp" line="151"/>
        <source>Pays/Organisation  : %1		</source>
        <translation>国/組織   : %1				</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/recherchesatellite.cpp" line="149"/>
        <source>Période orbitale   : %1</source>
        <translation>軌道周期      : %1</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/recherchesatellite.cpp" line="152"/>
        <source>Site de lancement  : %1		</source>
        <translation>発射場    : %1			</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/recherchesatellite.cpp" line="153"/>
        <source>Inclinaison        : %1</source>
        <translation>軌道傾斜角    : %1</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/recherchesatellite.cpp" line="156"/>
        <source>Site de lancement  : %1</source>
        <translation>発射場  : %1</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/recherchesatellite.cpp" line="231"/>
        <source>Objets trouvés (%1) :</source>
        <translation>見つかったオブジェクト (%1) :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/recherchesatellite.cpp" line="417"/>
        <location filename="../src/interface/onglets/donnees/recherchesatellite.cpp" line="512"/>
        <location filename="../src/interface/onglets/donnees/recherchesatellite.cpp" line="518"/>
        <location filename="../src/interface/onglets/donnees/recherchesatellite.cpp" line="561"/>
        <location filename="../src/interface/onglets/donnees/recherchesatellite.cpp" line="564"/>
        <source>Inconnu</source>
        <translation>不明</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/recherchesatellite.cpp" line="423"/>
        <location filename="../src/interface/onglets/donnees/recherchesatellite.cpp" line="525"/>
        <location filename="../src/interface/onglets/donnees/recherchesatellite.cpp" line="530"/>
        <location filename="../src/interface/onglets/donnees/recherchesatellite.cpp" line="533"/>
        <location filename="../src/interface/onglets/donnees/recherchesatellite.cpp" line="558"/>
        <source>Inconnue</source>
        <translation>不明</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/recherchesatellite.cpp" line="448"/>
        <source>SGP4 (DS)</source>
        <comment>Orbital model SGP4 (deep space)</comment>
        <translation>SGP4 (深宇宙)</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/recherchesatellite.cpp" line="448"/>
        <source>SGP4 (NE)</source>
        <comment>Orbital model SGP4 (near Earth)</comment>
        <translation>SGP4 (近地球域)</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/recherchesatellite.cpp" line="449"/>
        <source>Non applicable</source>
        <translation>該当なし</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/recherchesatellite.cpp" line="453"/>
        <source>Modèle haute orbite</source>
        <translation>高軌道モデル</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/recherchesatellite.cpp" line="453"/>
        <source>Modèle basse orbite</source>
        <translation>低軌道モデル</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/recherchesatellite.cpp" line="461"/>
        <source>m</source>
        <comment>meter</comment>
        <translation>m</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/recherchesatellite.cpp" line="462"/>
        <source>km</source>
        <comment>kilometer</comment>
        <translation>km</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/recherchesatellite.cpp" line="474"/>
        <source>ft</source>
        <comment>foot</comment>
        <translation>ft</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/recherchesatellite.cpp" line="475"/>
        <source>nmi</source>
        <comment>nautical mile</comment>
        <translation>nmi</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/recherchesatellite.cpp" line="480"/>
        <source>Sphérique. R=%1 %2</source>
        <comment>R = radius</comment>
        <translation>球形。 R=%1 %2</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/recherchesatellite.cpp" line="485"/>
        <source>Cylindrique. L=%1 %2, R=%3 %2</source>
        <comment>L = height, R = radius</comment>
        <translation>円筒形。 L=%1 %2, R=%3 %2</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/recherchesatellite.cpp" line="490"/>
        <source>Boîte. %1 x %2 x %3 %4</source>
        <translation>直方体。 %1 x %2 x %3 %4</translation>
    </message>
</context>
<context>
    <name>SuiviTelescope</name>
    <message>
        <location filename="../src/interface/onglets/telescope/suivitelescope.ui" line="50"/>
        <source>Lieu d&apos;observation :</source>
        <translation>観測地 :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/telescope/suivitelescope.ui" line="576"/>
        <source>Paramétrage par défaut</source>
        <translation>デフォルト設定</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/telescope/suivitelescope.ui" line="180"/>
        <source>Générer les positions</source>
        <translation>位置を生成する</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/telescope/suivitelescope.ui" line="205"/>
        <source>Lever du satellite :</source>
        <translation>衛星が地平線から出る日時 :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/telescope/suivitelescope.ui" line="219"/>
        <source>Hauteur maximale :</source>
        <translation>最大仰角 :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/telescope/suivitelescope.ui" line="233"/>
        <source>Coucher du satellite :</source>
        <translation>衛星が地平線に沈む日時 :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/telescope/suivitelescope.ui" line="279"/>
        <source>Hauteur minimale du satellite :</source>
        <translation>最小衛星仰角 :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/telescope/suivitelescope.ui" line="302"/>
        <source>0°</source>
        <translation>0°</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/telescope/suivitelescope.ui" line="307"/>
        <source>5°</source>
        <translation>5°</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/telescope/suivitelescope.ui" line="312"/>
        <source>10°</source>
        <translation>10°</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/telescope/suivitelescope.ui" line="317"/>
        <source>15°</source>
        <translation>15°</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/telescope/suivitelescope.ui" line="322"/>
        <source>20°</source>
        <translation>20°</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/telescope/suivitelescope.ui" line="327"/>
        <source>Autre...</source>
        <translation>その他...</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/telescope/suivitelescope.ui" line="404"/>
        <source>Pas de génération :</source>
        <translation>出力データ時間間隔 :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/telescope/suivitelescope.ui" line="453"/>
        <source>ms</source>
        <extracomment>milliseconde</extracomment>
        <translation>ms</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/telescope/suivitelescope.ui" line="470"/>
        <location filename="../src/interface/onglets/telescope/suivitelescope.cpp" line="973"/>
        <source>Ouvrir Satellite Tracker</source>
        <translation>Satellite Trackerを開く</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/telescope/suivitelescope.ui" line="492"/>
        <source>s</source>
        <translation>s</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/telescope/suivitelescope.ui" line="512"/>
        <source>Délai :</source>
        <comment>telescope</comment>
        <translation>遅延 :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/telescope/suivitelescope.ui" line="531"/>
        <source>Afficher</source>
        <translation>表示する</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/telescope/suivitelescope.ui" line="544"/>
        <source>Envoyer les données satellite à la monture</source>
        <translation>架台へ衛星データを送信する</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/telescope/suivitelescope.ui" line="547"/>
        <source>Démarrer</source>
        <translation>開始する</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/telescope/suivitelescope.ui" line="560"/>
        <source>Ajuster les dates...</source>
        <translation>日時を調整する...</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/telescope/suivitelescope.ui" line="590"/>
        <source>Le satellite n&apos;est pas visible depuis le lieu d&apos;observation</source>
        <translation>衛星はこの観測地から見えません</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/telescope/suivitelescope.ui" line="621"/>
        <source>Nom :</source>
        <translation>名称 :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/telescope/suivitelescope.ui" line="740"/>
        <source>Filtre</source>
        <translation>フィルター</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/telescope/suivitelescope.cpp" line="329"/>
        <source>Satellite en éclipse</source>
        <translation>現在、衛星の食中です</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/telescope/suivitelescope.cpp" line="329"/>
        <source>Satellite éclairé</source>
        <translation>現在、衛星に太陽光が当たっている</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/telescope/suivitelescope.cpp" line="342"/>
        <source>%1 (dans %2). Azimut : %3</source>
        <comment>Delay in hour, minutes, seconds</comment>
        <translation>%1 (あと%2で)。方位角 : %3</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/telescope/suivitelescope.cpp" line="344"/>
        <source>AOS</source>
        <comment>Acquisition of signal</comment>
        <translation>AOS</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/telescope/suivitelescope.cpp" line="363"/>
        <source>%1%2. Azimut : %3</source>
        <translation>%1%2。 方位角 : %3</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/telescope/suivitelescope.cpp" line="368"/>
        <location filename="../src/interface/onglets/telescope/suivitelescope.cpp" line="428"/>
        <source>h</source>
        <comment>hour</comment>
        <translation>h</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/telescope/suivitelescope.cpp" line="368"/>
        <location filename="../src/interface/onglets/telescope/suivitelescope.cpp" line="373"/>
        <location filename="../src/interface/onglets/telescope/suivitelescope.cpp" line="428"/>
        <location filename="../src/interface/onglets/telescope/suivitelescope.cpp" line="432"/>
        <source>min</source>
        <comment>minute</comment>
        <translation>min</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/telescope/suivitelescope.cpp" line="373"/>
        <location filename="../src/interface/onglets/telescope/suivitelescope.cpp" line="432"/>
        <source>s</source>
        <comment>second</comment>
        <translation>s</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/telescope/suivitelescope.cpp" line="404"/>
        <location filename="../src/interface/onglets/telescope/suivitelescope.cpp" line="482"/>
        <source>Satellite dans le ciel. Hauteur actuelle : %1. Azimut : %2. %3</source>
        <translation>衛星が上空を通過している. 現在の仰角 : %1. 方位角 : %2. %3</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/telescope/suivitelescope.cpp" line="620"/>
        <source>Aucun satellite n&apos;est sélectionné dans la liste</source>
        <translation>リストで衛星が選択されていません</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/telescope/suivitelescope.cpp" line="645"/>
        <source>LOS</source>
        <comment>Loss of signal</comment>
        <translation>LOS</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/telescope/suivitelescope.cpp" line="704"/>
        <source>Calculs en cours...</source>
        <translation>進行中の計算...</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/telescope/suivitelescope.cpp" line="705"/>
        <source>Annuler</source>
        <translation>キャンセル</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/telescope/suivitelescope.cpp" line="734"/>
        <source>Calculs terminés</source>
        <translation>計算が完了しました</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/telescope/suivitelescope.cpp" line="974"/>
        <source>Fichiers exécutables (*.exe)</source>
        <translation>実行ファイル (*.exe)</translation>
    </message>
</context>
<context>
    <name>Telechargement</name>
    <message>
        <location filename="../src/librairies/systeme/telechargement.cpp" line="98"/>
        <source>Erreur lors du téléchargement du fichier %1</source>
        <translation>ファイル「%1」のダウンロード中にエラーが発生しました</translation>
    </message>
    <message>
        <location filename="../src/librairies/systeme/telechargement.cpp" line="124"/>
        <source>Impossible d&apos;écrire le fichier %1 dans le répertoire %2</source>
        <translation>ファイル「%1」をディレクトリ「%2」に書き込めません</translation>
    </message>
    <message>
        <location filename="../src/librairies/systeme/telechargement.cpp" line="209"/>
        <source>o/s</source>
        <translation>B/s</translation>
    </message>
    <message>
        <location filename="../src/librairies/systeme/telechargement.cpp" line="212"/>
        <source>ko/s</source>
        <translation>KB/s</translation>
    </message>
    <message>
        <location filename="../src/librairies/systeme/telechargement.cpp" line="215"/>
        <source>Mo/s</source>
        <translation>MB/s</translation>
    </message>
</context>
<context>
    <name>TelechargementOptions</name>
    <message>
        <location filename="../src/interface/options/telechargementoptions.ui" line="61"/>
        <source>Télécharger...</source>
        <translation>ダウンロード...</translation>
    </message>
    <message>
        <location filename="../src/interface/options/telechargementoptions.ui" line="77"/>
        <source>Fermer</source>
        <translation>閉じる</translation>
    </message>
    <message>
        <location filename="../src/interface/options/telechargementoptions.ui" line="118"/>
        <source>Filtre</source>
        <translation>フィルター</translation>
    </message>
    <message>
        <location filename="../src/interface/options/telechargementoptions.cpp" line="132"/>
        <source>Télécharger des lieux d&apos;observation...</source>
        <translation>観測地をダウンロードする...</translation>
    </message>
    <message>
        <location filename="../src/interface/options/telechargementoptions.cpp" line="136"/>
        <source>Télécharger des cartes du monde...</source>
        <translation>世界地図をダウンロードする...</translation>
    </message>
    <message>
        <location filename="../src/interface/options/telechargementoptions.cpp" line="140"/>
        <source>Télécharger des fichiers de notification sonore...</source>
        <translation>通知音ファイルをダウンロードする...</translation>
    </message>
    <message>
        <location filename="../src/interface/options/telechargementoptions.cpp" line="283"/>
        <source>Information</source>
        <translation>情報</translation>
    </message>
    <message>
        <location filename="../src/interface/options/telechargementoptions.cpp" line="283"/>
        <source>Veuillez redémarrer %1 pour prendre en compte la mise à jour</source>
        <translation>更新を完了するために「%1」の再起動が必要です</translation>
    </message>
</context>
<context>
    <name>cardinal point</name>
    <message>
        <location filename="../src/librairies/observateur/observateur.cpp" line="50"/>
        <source>N</source>
        <translation>北</translation>
    </message>
    <message>
        <location filename="../src/librairies/observateur/observateur.cpp" line="51"/>
        <source>NNE</source>
        <translation>北北東</translation>
    </message>
    <message>
        <location filename="../src/librairies/observateur/observateur.cpp" line="52"/>
        <source>NE</source>
        <translation>北東</translation>
    </message>
    <message>
        <location filename="../src/librairies/observateur/observateur.cpp" line="53"/>
        <source>ENE</source>
        <translation>東北東</translation>
    </message>
    <message>
        <location filename="../src/librairies/observateur/observateur.cpp" line="54"/>
        <source>E</source>
        <translation>東</translation>
    </message>
    <message>
        <location filename="../src/librairies/observateur/observateur.cpp" line="55"/>
        <source>ESE</source>
        <translation>東南東</translation>
    </message>
    <message>
        <location filename="../src/librairies/observateur/observateur.cpp" line="56"/>
        <source>SE</source>
        <translation>南東</translation>
    </message>
    <message>
        <location filename="../src/librairies/observateur/observateur.cpp" line="57"/>
        <source>SSE</source>
        <translation>南南東</translation>
    </message>
    <message>
        <location filename="../src/librairies/observateur/observateur.cpp" line="58"/>
        <source>S</source>
        <translation>南</translation>
    </message>
    <message>
        <location filename="../src/librairies/observateur/observateur.cpp" line="59"/>
        <source>SSW</source>
        <translation>南南西</translation>
    </message>
    <message>
        <location filename="../src/librairies/observateur/observateur.cpp" line="60"/>
        <source>SW</source>
        <translation>南西</translation>
    </message>
    <message>
        <location filename="../src/librairies/observateur/observateur.cpp" line="61"/>
        <source>WSW</source>
        <translation>西南西</translation>
    </message>
    <message>
        <location filename="../src/librairies/observateur/observateur.cpp" line="62"/>
        <source>W</source>
        <translation>西</translation>
    </message>
    <message>
        <location filename="../src/librairies/observateur/observateur.cpp" line="63"/>
        <source>WNW</source>
        <translation>西北西</translation>
    </message>
    <message>
        <location filename="../src/librairies/observateur/observateur.cpp" line="64"/>
        <source>NW</source>
        <translation>北西</translation>
    </message>
    <message>
        <location filename="../src/librairies/observateur/observateur.cpp" line="65"/>
        <source>NNW</source>
        <translation>北北西</translation>
    </message>
</context>
<context>
    <name>planet</name>
    <message>
        <location filename="../src/librairies/corps/systemesolaire/planeteconst.h" line="73"/>
        <source>Mercure</source>
        <translation>水星</translation>
    </message>
    <message>
        <location filename="../src/librairies/corps/systemesolaire/planeteconst.h" line="74"/>
        <source>Vénus</source>
        <translation>金星</translation>
    </message>
    <message>
        <location filename="../src/librairies/corps/systemesolaire/planeteconst.h" line="75"/>
        <source>Mars</source>
        <translation>火星</translation>
    </message>
    <message>
        <location filename="../src/librairies/corps/systemesolaire/planeteconst.h" line="76"/>
        <source>Jupiter</source>
        <translation>木星</translation>
    </message>
    <message>
        <location filename="../src/librairies/corps/systemesolaire/planeteconst.h" line="77"/>
        <source>Saturne</source>
        <translation>土星</translation>
    </message>
    <message>
        <location filename="../src/librairies/corps/systemesolaire/planeteconst.h" line="78"/>
        <source>Uranus</source>
        <translation>天王星</translation>
    </message>
    <message>
        <location filename="../src/librairies/corps/systemesolaire/planeteconst.h" line="79"/>
        <source>Neptune</source>
        <translation>海王星</translation>
    </message>
</context>
</TS>
