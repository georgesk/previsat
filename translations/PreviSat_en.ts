<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
<context>
    <name>AfficherResultats</name>
    <message>
        <location filename="../src/interface/afficherresultats.ui" line="128"/>
        <source>Afficher la carte</source>
        <translation>Show the map</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.ui" line="204"/>
        <location filename="../src/interface/afficherresultats.ui" line="207"/>
        <source>Enregistrer</source>
        <translation>Save</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.ui" line="216"/>
        <location filename="../src/interface/afficherresultats.ui" line="219"/>
        <source>Enregistrer fichier texte</source>
        <translation>Save text file</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="127"/>
        <source>Prévisions de passage</source>
        <translation>Predictions of passes</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="128"/>
        <location filename="../src/interface/afficherresultats.cpp" line="139"/>
        <location filename="../src/interface/afficherresultats.cpp" line="147"/>
        <location filename="../src/interface/afficherresultats.cpp" line="157"/>
        <location filename="../src/interface/afficherresultats.cpp" line="1194"/>
        <location filename="../src/interface/afficherresultats.cpp" line="1204"/>
        <location filename="../src/interface/afficherresultats.cpp" line="1216"/>
        <source>Satellite</source>
        <translation>Satellite</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="128"/>
        <location filename="../src/interface/afficherresultats.cpp" line="139"/>
        <location filename="../src/interface/afficherresultats.cpp" line="157"/>
        <source>Date de début</source>
        <comment>Date and hour</comment>
        <translation>Start date</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="128"/>
        <location filename="../src/interface/afficherresultats.cpp" line="139"/>
        <location filename="../src/interface/afficherresultats.cpp" line="157"/>
        <source>Date de fin</source>
        <comment>Date and hour</comment>
        <translation>End date</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="129"/>
        <location filename="../src/interface/afficherresultats.cpp" line="158"/>
        <source>Hauteur max</source>
        <comment>Maximum elevation</comment>
        <translation>Max elevation</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="129"/>
        <location filename="../src/interface/afficherresultats.cpp" line="158"/>
        <location filename="../src/interface/afficherresultats.cpp" line="191"/>
        <location filename="../src/interface/afficherresultats.cpp" line="1247"/>
        <location filename="../src/interface/afficherresultats.cpp" line="1255"/>
        <source>Magnitude</source>
        <translation>Magnitude</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="129"/>
        <location filename="../src/interface/afficherresultats.cpp" line="148"/>
        <location filename="../src/interface/afficherresultats.cpp" line="158"/>
        <location filename="../src/interface/afficherresultats.cpp" line="193"/>
        <location filename="../src/interface/afficherresultats.cpp" line="1249"/>
        <location filename="../src/interface/afficherresultats.cpp" line="1258"/>
        <location filename="../src/interface/afficherresultats.cpp" line="1272"/>
        <source>Hauteur Soleil</source>
        <translation>Sun elevation</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="133"/>
        <source>Évènements orbitaux</source>
        <translation>Orbital events</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="137"/>
        <source>Flashs</source>
        <translation>Flares</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="140"/>
        <source>Hauteur Max</source>
        <comment>Maximum elevation</comment>
        <translation>Max elevation</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="140"/>
        <location filename="../src/interface/afficherresultats.cpp" line="1197"/>
        <location filename="../src/interface/afficherresultats.cpp" line="1208"/>
        <source>Magn</source>
        <comment>Magnitude</comment>
        <translation>Magn</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="140"/>
        <location filename="../src/interface/afficherresultats.cpp" line="1207"/>
        <source>Mir</source>
        <comment>Mirror</comment>
        <translation>Mir</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="140"/>
        <location filename="../src/interface/afficherresultats.cpp" line="1199"/>
        <location filename="../src/interface/afficherresultats.cpp" line="1209"/>
        <location filename="../src/interface/afficherresultats.cpp" line="1221"/>
        <source>Haut Soleil</source>
        <comment>Solar elevation</comment>
        <translation>Sun elev</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="144"/>
        <source>Transits</source>
        <translation>Transits</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="147"/>
        <location filename="../src/interface/afficherresultats.cpp" line="1219"/>
        <source>Cst</source>
        <comment>Constellation</comment>
        <translation>Cst</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="147"/>
        <location filename="../src/interface/afficherresultats.cpp" line="1253"/>
        <location filename="../src/interface/afficherresultats.cpp" line="1266"/>
        <source>Angle</source>
        <translation>Angle</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="148"/>
        <location filename="../src/interface/afficherresultats.cpp" line="1219"/>
        <source>Type</source>
        <comment>Transit or conjunction</comment>
        <translation>Type</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="148"/>
        <location filename="../src/interface/afficherresultats.cpp" line="1220"/>
        <source>Corps</source>
        <translation>Body</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="1220"/>
        <source>Ill</source>
        <comment>Illumination</comment>
        <translation>Ill</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="148"/>
        <location filename="../src/interface/afficherresultats.cpp" line="1220"/>
        <source>Durée</source>
        <translation>Duration</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="147"/>
        <source>Date du maximum</source>
        <comment>Date and hour</comment>
        <translation>Date of maximum</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="148"/>
        <source>Illum</source>
        <comment>Illumination</comment>
        <translation>Illum</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="156"/>
        <source>Passages des Starlink</source>
        <translation>Starlink passes</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="190"/>
        <source>Hauteur maximale</source>
        <translation>Maximum elevation</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="192"/>
        <location filename="../src/interface/afficherresultats.cpp" line="1254"/>
        <source>Miroir</source>
        <translation>Mirror</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="198"/>
        <location filename="../src/interface/afficherresultats.cpp" line="1241"/>
        <source>Constellation</source>
        <translation>Constellation</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="199"/>
        <location filename="../src/interface/afficherresultats.cpp" line="1267"/>
        <source>Illumination</source>
        <translation>Illumination</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="200"/>
        <location filename="../src/interface/afficherresultats.cpp" line="1268"/>
        <source>secondes</source>
        <translation>seconds</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="691"/>
        <source>Unité de distance               : %1</source>
        <translation>Range unit                     : %1</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="699"/>
        <source>Age de l&apos;élément                : %1 jours (au %2)</source>
        <translation>Age of the element             : %1 days (at %2)</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="1194"/>
        <location filename="../src/interface/afficherresultats.cpp" line="1204"/>
        <location filename="../src/interface/afficherresultats.cpp" line="1216"/>
        <source>Date</source>
        <comment>Date and hour</comment>
        <translation>Date</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="1195"/>
        <location filename="../src/interface/afficherresultats.cpp" line="1205"/>
        <location filename="../src/interface/afficherresultats.cpp" line="1217"/>
        <source>Azimut Sat</source>
        <comment>Satellite azimuth</comment>
        <translation>Sat azimuth</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="1195"/>
        <location filename="../src/interface/afficherresultats.cpp" line="1205"/>
        <location filename="../src/interface/afficherresultats.cpp" line="1217"/>
        <source>Hauteur Sat</source>
        <comment>Satellite elevation</comment>
        <translation>Sat elevation</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="1196"/>
        <location filename="../src/interface/afficherresultats.cpp" line="1206"/>
        <location filename="../src/interface/afficherresultats.cpp" line="1218"/>
        <source>AD Sat</source>
        <comment>Satellite right ascension</comment>
        <translation>Sat RA</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="1196"/>
        <location filename="../src/interface/afficherresultats.cpp" line="1206"/>
        <location filename="../src/interface/afficherresultats.cpp" line="1218"/>
        <source>Decl Sat</source>
        <comment>Satellite declination</comment>
        <translation>Sat Decl</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="1197"/>
        <location filename="../src/interface/afficherresultats.cpp" line="1207"/>
        <source>Const</source>
        <comment>Constellation</comment>
        <translation>Const</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="1198"/>
        <location filename="../src/interface/afficherresultats.cpp" line="1208"/>
        <location filename="../src/interface/afficherresultats.cpp" line="1269"/>
        <source>Altitude</source>
        <comment>Altitude of satellite</comment>
        <translation>Altitude</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="1198"/>
        <location filename="../src/interface/afficherresultats.cpp" line="1211"/>
        <location filename="../src/interface/afficherresultats.cpp" line="1223"/>
        <location filename="../src/interface/afficherresultats.cpp" line="1256"/>
        <location filename="../src/interface/afficherresultats.cpp" line="1270"/>
        <source>Distance</source>
        <translation>Range</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="1198"/>
        <location filename="../src/interface/afficherresultats.cpp" line="1209"/>
        <location filename="../src/interface/afficherresultats.cpp" line="1221"/>
        <source>Az Soleil</source>
        <comment>Solar azimuth</comment>
        <translation>Sun az</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="1207"/>
        <location filename="../src/interface/afficherresultats.cpp" line="1219"/>
        <source>Ang</source>
        <comment>Angle</comment>
        <translation>Ang</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="1208"/>
        <location filename="../src/interface/afficherresultats.cpp" line="1221"/>
        <source>Dist</source>
        <comment>Range</comment>
        <translation>Rng</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="1210"/>
        <location filename="../src/interface/afficherresultats.cpp" line="1222"/>
        <source>Long Max</source>
        <comment>Longitude of the maximum</comment>
        <translation>Max Long</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="1210"/>
        <location filename="../src/interface/afficherresultats.cpp" line="1222"/>
        <source>Lat Max</source>
        <comment>Latitude of the maximum</comment>
        <translation>Max Lat</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="1211"/>
        <source>Magn Max</source>
        <comment>Magnitude at the maximum</comment>
        <translation>Max Magn</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="1220"/>
        <source>Alt</source>
        <comment>Altitude of satellite</comment>
        <translation>Alt</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="1237"/>
        <source>Azimut satellite</source>
        <translation>Satellite azimuth</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="1238"/>
        <source>Hauteur satellite</source>
        <translation>Satellite elevation</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="1239"/>
        <source>Ascension droite satellite</source>
        <translation>Satellite right ascension</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="1240"/>
        <source>Déclinaison satellite</source>
        <translation>Satellite declination</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="1248"/>
        <location filename="../src/interface/afficherresultats.cpp" line="1257"/>
        <location filename="../src/interface/afficherresultats.cpp" line="1271"/>
        <source>Azimut Soleil</source>
        <translation>Sun azimuth</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="1259"/>
        <location filename="../src/interface/afficherresultats.cpp" line="1273"/>
        <source>Longitude du maximum</source>
        <translation>Longitude of maximum</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="1260"/>
        <location filename="../src/interface/afficherresultats.cpp" line="1274"/>
        <source>Latitude du maximum</source>
        <translation>Latitude of maximum</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="1261"/>
        <source>Magnitude au maximum</source>
        <translation>Magnitude at the maximum</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="1262"/>
        <location filename="../src/interface/afficherresultats.cpp" line="1275"/>
        <source>Distance au maximum</source>
        <translation>Range to the maximum</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="1382"/>
        <source>Détail du passage</source>
        <translation>Pass details</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="1386"/>
        <source>Détail du flash</source>
        <translation>Flare details</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="1390"/>
        <source>Détail du transit ou conjonction</source>
        <translation>Transit or conjunction details</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="1434"/>
        <source>Enregistrer sous</source>
        <translation>Save as</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="1435"/>
        <source>Fichiers PNG (*.png);;Fichiers JPEG (*.jpg *.jpeg);;Fichiers BMP (*.bmp);;Tous les fichiers (*.*)</source>
        <translation>PNG files (*.png);;JPEG files (*.jpg *.jpeg);;BMP files (*.bmp);;All files (*.*)</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="1465"/>
        <source>Fichiers texte (*.txt);;Tous les fichiers (*.*)</source>
        <translation>Text files (*.txt);;All files (*.*)</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="1531"/>
        <source>   Date      Heure      Azimut Sat Hauteur Sat  AD Sat    Decl Sat   Cst  Ang  Type Corps Ill Durée  Altitude  Distance  Az Soleil  Haut Soleil   Long Max    Lat Max     Distance</source>
        <comment>Date, Hour, Satellite azimuth, Satellite elevation, Satellite right ascension, Satellite declination, Constellation, Angle, Type, Body, Illumination, Duration, Altitude of satellite, Range, Solar azimuth, Solar elevation, Longitude of the maximum, Latitude of the maximum, Range from the maximum</comment>
        <translation>   Date        Hour    Sat Azimuth  Sat Elev    RA Sat    Decl Sat   Cst  Ang  Type Body  Ill  Dur  Altitude     Range   Sun Azim   Sun Elev     Max Long    Max Lat       Range</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="1464"/>
        <source>Enregistrer sous...</source>
        <translation>Save as...</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="1502"/>
        <source>(numéro NORAD : %1)</source>
        <translation>(NORAD number : %1)</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="1510"/>
        <location filename="../src/interface/afficherresultats.cpp" line="1541"/>
        <source>   Date      Heure    Azimut Sat Hauteur Sat  AD Sat    Decl Sat  Const Magn  Altitude  Distance  Az Soleil  Haut Soleil</source>
        <comment>Date, Hour, Satellite azimuth, Satellite elevation, Satellite right ascension, Satellite declination, Constellation, Magnitude, Altitude of satellite, Range, Solar azimuth, Solar elevation</comment>
        <translation>   Date      Hour    Sat Azimuth  Sat Elev    RA Sat    Decl Sat  Const Magn  Altitude     Range   Sun Azim   Sun Elev</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="1519"/>
        <source>Satellite     Date      Heure      Azimut Sat Hauteur Sat  AD Sat    Decl Sat   Cst  Ang   Mir Magn       Alt      Dist  Az Soleil  Haut Soleil   Long Max    Lat Max    Magn Max  Distance</source>
        <comment>Satellite, Date, Hour, Satellite azimuth, Satellite elevation, Satellite right ascension, Satellite declination, Constellation, Angle, Mirror, Magnitude, Altitude of satellite, Range, Solar azimuth, Solar elevation, Longitude of the maximum, Latitude of the maximum, Magnitude at the maximum, Range from the maximum</comment>
        <translation>Satellite     Date       Hour     Sat Azimuth  Sat Elev    RA Sat    Decl Sat   Cst  Ang   Mir Magn       Alt     Range  Sun Azim   Sun Elev     Max Long    Max Lat     Max Magn  Range</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="1550"/>
        <source>   Date      Heure     PSO    Longitude  Latitude  Évènements</source>
        <comment>Date, Hour, In orbit position, Longitude, Latitude, Events</comment>
        <translation>   Date      Hour    Position Longitude  Latitude  Events</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="1682"/>
        <source>Temps écoulé : %1s</source>
        <translation>Elapsed time : %1s</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="420"/>
        <source>km</source>
        <comment>Kilometer</comment>
        <translation>km</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="420"/>
        <location filename="../src/interface/afficherresultats.cpp" line="421"/>
        <source>m</source>
        <comment>meter</comment>
        <translation>m</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="420"/>
        <source>ft</source>
        <comment>foot</comment>
        <translation>ft</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="446"/>
        <source>Longitude</source>
        <translation>Longitude</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="447"/>
        <source>Latitude</source>
        <translation>Latitude</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="448"/>
        <source>Altitude</source>
        <comment>Altitude of observer</comment>
        <translation>Altitude</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="564"/>
        <source>Double-cliquez sur une ligne pour afficher plus de détails</source>
        <translation>Double clic on a line to show more details</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="703"/>
        <source>Age de l&apos;élément le plus récent : %1 jours (au %2)
Age de l&apos;élément le plus ancien : %3 jours</source>
        <translation>Age of the most recent element : %1 days (at %2)
Age of the oldest element      : %3 days</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="755"/>
        <source>W</source>
        <comment>West</comment>
        <translation>W</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="755"/>
        <source>E</source>
        <comment>East</comment>
        <translation>E</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="757"/>
        <source>N</source>
        <comment>North</comment>
        <translation>N</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="757"/>
        <source>S</source>
        <comment>South</comment>
        <translation>S</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="875"/>
        <location filename="../src/interface/afficherresultats.cpp" line="1021"/>
        <location filename="../src/interface/afficherresultats.cpp" line="1139"/>
        <source>nmi</source>
        <comment>nautical mile</comment>
        <translation>nmi</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="1063"/>
        <location filename="../src/interface/afficherresultats.cpp" line="1118"/>
        <source>T</source>
        <comment>transit</comment>
        <translation>T</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="1063"/>
        <location filename="../src/interface/afficherresultats.cpp" line="1118"/>
        <source>C</source>
        <comment>conjunction</comment>
        <translation>C</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="1064"/>
        <location filename="../src/interface/afficherresultats.cpp" line="1119"/>
        <source>S</source>
        <comment>Sun</comment>
        <translation>S</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="1064"/>
        <location filename="../src/interface/afficherresultats.cpp" line="1119"/>
        <source>L</source>
        <comment>Moon</comment>
        <translation>M</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="1067"/>
        <location filename="../src/interface/afficherresultats.cpp" line="1122"/>
        <source>Lum</source>
        <comment>Lit</comment>
        <translation>Ill</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="1069"/>
        <location filename="../src/interface/afficherresultats.cpp" line="1124"/>
        <source>Omb</source>
        <comment>Shadow</comment>
        <translation>Sha</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="1073"/>
        <location filename="../src/interface/afficherresultats.cpp" line="1128"/>
        <source>Pen</source>
        <comment>Penumbra</comment>
        <translation>Pen</translation>
    </message>
</context>
<context>
    <name>AjustementDates</name>
    <message>
        <location filename="../src/interface/onglets/telescope/ajustementdates.ui" line="29"/>
        <source>Ajuster les dates...</source>
        <translation>Adjust the dates...</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/telescope/ajustementdates.ui" line="93"/>
        <source>Azimut (N)</source>
        <translation>Azimuth (N)</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/telescope/ajustementdates.ui" line="112"/>
        <location filename="../src/interface/onglets/telescope/ajustementdates.ui" line="135"/>
        <source>dd/MM/yyyy HH:mm:ss</source>
        <translation>dd/MM/yyyy HH:mm:ss</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/telescope/ajustementdates.ui" line="142"/>
        <source>Date initiale :</source>
        <translation>Start date :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/telescope/ajustementdates.ui" line="156"/>
        <source>Hauteur</source>
        <translation>Elevation</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/telescope/ajustementdates.ui" line="76"/>
        <source>Date finale :</source>
        <translation>End date :</translation>
    </message>
</context>
<context>
    <name>Antenne</name>
    <message>
        <location filename="../src/interface/onglets/antenne/antenne.ui" line="41"/>
        <source>Adresse IP :</source>
        <translation>IP address :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/antenne/antenne.ui" line="58"/>
        <source>Port :</source>
        <translation>Port :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/antenne/antenne.ui" line="90"/>
        <source>Données transmises</source>
        <translation>Transmitted data</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/antenne/antenne.ui" line="123"/>
        <source>Hauteur :</source>
        <translation>Elevation :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/antenne/antenne.ui" line="136"/>
        <source>Azimut (N) :</source>
        <extracomment>Azimuth (from the North)</extracomment>
        <translation>Azimuth (N) :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/antenne/antenne.ui" line="149"/>
        <source>Variation distance :</source>
        <translation>Range rate :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/antenne/antenne.ui" line="194"/>
        <source>Double-cliquer pour changer d&apos;unités</source>
        <translation>Double clic to change units</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/antenne/antenne.ui" line="244"/>
        <source>Fréquence montante :</source>
        <translation>Uplink frequency :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/antenne/antenne.ui" line="254"/>
        <location filename="../src/interface/onglets/antenne/antenne.ui" line="401"/>
        <source>Fréquence réelle :</source>
        <translation>Real frequency :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/antenne/antenne.ui" line="271"/>
        <location filename="../src/interface/onglets/antenne/antenne.ui" line="418"/>
        <source>Doppler :</source>
        <extracomment>Doppler effect</extracomment>
        <translation>Doppler :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/antenne/antenne.ui" line="288"/>
        <location filename="../src/interface/onglets/antenne/antenne.ui" line="469"/>
        <source>Balise :</source>
        <translation>Beacon :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/antenne/antenne.ui" line="305"/>
        <location filename="../src/interface/onglets/antenne/antenne.ui" line="486"/>
        <source>Mode :</source>
        <extracomment>Beacon mode</extracomment>
        <translation>Mode :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/antenne/antenne.ui" line="322"/>
        <location filename="../src/interface/onglets/antenne/antenne.ui" line="503"/>
        <source>Signal d&apos;appel :</source>
        <extracomment>Callsign</extracomment>
        <translation>Callsign :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/antenne/antenne.ui" line="339"/>
        <location filename="../src/interface/onglets/antenne/antenne.ui" line="435"/>
        <source>Atténuation :</source>
        <extracomment>Free space path loss</extracomment>
        <translation>Free-space loss :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/antenne/antenne.ui" line="356"/>
        <location filename="../src/interface/onglets/antenne/antenne.ui" line="452"/>
        <source>Délai :</source>
        <translation>Delay :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/antenne/antenne.ui" line="391"/>
        <source>Fréquence descendante :</source>
        <translation>Downlink frequency :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/antenne/antenne.ui" line="530"/>
        <location filename="../src/interface/onglets/antenne/antenne.cpp" line="456"/>
        <source>Connecter</source>
        <translation>Connect</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/antenne/antenne.ui" line="552"/>
        <source>Paramétrage par défaut</source>
        <translation>Default settings</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/antenne/antenne.ui" line="565"/>
        <location filename="../src/interface/onglets/antenne/antenne.cpp" line="491"/>
        <source>Ouvrir CatRotator</source>
        <translation>Open CatRotator</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/antenne/antenne.ui" line="639"/>
        <source>Nom :</source>
        <translation>Name :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/antenne/antenne.cpp" line="277"/>
        <source>h</source>
        <comment>hour</comment>
        <translation>h</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/antenne/antenne.cpp" line="277"/>
        <location filename="../src/interface/onglets/antenne/antenne.cpp" line="281"/>
        <source>min</source>
        <comment>minute</comment>
        <translation>min</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/antenne/antenne.cpp" line="281"/>
        <source>s</source>
        <comment>second</comment>
        <translation>s</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/antenne/antenne.cpp" line="284"/>
        <source>Prochain %1 dans %2</source>
        <comment>Next AOS or LOS, and delay</comment>
        <translation>Next %1 in %2</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/antenne/antenne.cpp" line="286"/>
        <source>AOS</source>
        <translation>AOS</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/antenne/antenne.cpp" line="286"/>
        <source>Acquisition du signal</source>
        <translation>Acquisition of signal</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/antenne/antenne.cpp" line="286"/>
        <source>Perte du signal</source>
        <translation>Loss of signal</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/antenne/antenne.cpp" line="412"/>
        <source>Déconnecter</source>
        <translation>Disconnect</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/antenne/antenne.cpp" line="442"/>
        <source>Connexion en cours...</source>
        <translation>Connecting...</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/antenne/antenne.cpp" line="491"/>
        <source>Fichiers exécutables (*.exe)</source>
        <translation>Executable files (*.exe)</translation>
    </message>
</context>
<context>
    <name>Apropos</name>
    <message>
        <location filename="../src/interface/apropos/apropos.ui" line="146"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../src/interface/apropos/apropos.ui" line="162"/>
        <source>PreviSat est un logiciel de suivi des satellites artificiels afin de les observer.

Il permet d&apos;afficher la position des satellites en temps réel ou en mode manuel. PreviSat est capable d&apos;effectuer les prévisions de passage, de calculer les flashs de certains satellites, ainsi que plusieurs autres calculs de prévisions.

PreviSat est entièrement gratuit!</source>
        <translation>PreviSat is a satellite tracking software for observing purposes.

It shows positions of artificial satellites in real time or manual mode. PreviSat is able to make predictions of their passes, predictions of the flares of some satellites and several other calculations.

PreviSat is free !</translation>
    </message>
    <message>
        <location filename="../src/interface/apropos/apropos.ui" line="188"/>
        <source>Un immense merci à Michel Casabonne pour ses nombreuses suggestions pour PreviSat.</source>
        <translation>A great thank to Michel Casabonne for his many suggestions to PreviSat.</translation>
    </message>
    <message>
        <location filename="../src/interface/apropos/apropos.ui" line="210"/>
        <source>Vérifier les mises à jour...</source>
        <translation>Check software update...</translation>
    </message>
    <message>
        <location filename="../src/interface/apropos/apropos.cpp" line="110"/>
        <source>À propos de %1 %2</source>
        <translation>About %1 %2</translation>
    </message>
    <message>
        <location filename="../src/interface/apropos/apropos.cpp" line="129"/>
        <source>Version %1  (%2)</source>
        <translation>Version %1  (%2)</translation>
    </message>
    <message>
        <location filename="../src/interface/apropos/apropos.cpp" line="132"/>
        <source>d MMMM yyyy</source>
        <comment>Date format</comment>
        <translation>MMMM d yyyy</translation>
    </message>
    <message>
        <location filename="../src/interface/apropos/apropos.cpp" line="165"/>
        <source>Information</source>
        <translation>Information</translation>
    </message>
    <message>
        <location filename="../src/interface/apropos/apropos.cpp" line="165"/>
        <source>Vous utilisez la dernière version de %1</source>
        <translation>You have the last version of %1</translation>
    </message>
</context>
<context>
    <name>CalculsEvenementsOrbitaux</name>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsevenementsorbitaux.ui" line="68"/>
        <source>Évènements</source>
        <translation>Events</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsevenementsorbitaux.ui" line="83"/>
        <source>Passages aux noeuds</source>
        <translation>Passes to nodes</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsevenementsorbitaux.ui" line="102"/>
        <source>Passages à PSO = 90° et 270°</source>
        <translation>Passes to Position = 90° and 270°</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsevenementsorbitaux.ui" line="121"/>
        <source>Passages apogée/périgée</source>
        <translation>Passes to apogee/perigee</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsevenementsorbitaux.ui" line="140"/>
        <source>Passages ombre/pénombre/lumière</source>
        <translation>Passes to shadow/penumbra/light</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsevenementsorbitaux.ui" line="159"/>
        <source>Passages terminateur</source>
        <translation>Terminator passes</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsevenementsorbitaux.ui" line="188"/>
        <source>Date finale :</source>
        <translation>End date :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsevenementsorbitaux.ui" line="207"/>
        <location filename="../src/interface/onglets/previsions/calculsevenementsorbitaux.ui" line="239"/>
        <source>dd/MM/yyyy HH:mm:ss</source>
        <extracomment>Date format</extracomment>
        <translation>dd/MM/yyyy HH:mm:ss</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsevenementsorbitaux.ui" line="220"/>
        <source>Date initiale :</source>
        <translation>Start date :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsevenementsorbitaux.ui" line="258"/>
        <source>Effacer heures</source>
        <translation>Erase hours</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsevenementsorbitaux.ui" line="277"/>
        <source>Calculs</source>
        <translation>Run</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsevenementsorbitaux.ui" line="296"/>
        <source>Paramétrage par défaut</source>
        <translation>Default settings</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsevenementsorbitaux.ui" line="315"/>
        <source>Filtre</source>
        <translation>Filter</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsevenementsorbitaux.ui" line="353"/>
        <source>Ages des éléments orbitaux : [</source>
        <translation>Ages of orbital elements : [</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsevenementsorbitaux.ui" line="379"/>
        <source>] jours</source>
        <translation>] days</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsevenementsorbitaux.ui" line="395"/>
        <source>Mettre à jour les éléments orbitaux...</source>
        <translation>Update orbital elements...</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsevenementsorbitaux.cpp" line="315"/>
        <source>Aucun</source>
        <translation>None</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsevenementsorbitaux.cpp" line="318"/>
        <source>Tous</source>
        <translation>All</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsevenementsorbitaux.cpp" line="369"/>
        <source>Aucun satellite n&apos;est sélectionné dans la liste</source>
        <translation>None satellite selected in the list</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsevenementsorbitaux.cpp" line="399"/>
        <source>km</source>
        <comment>kilometer</comment>
        <translation>km</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsevenementsorbitaux.cpp" line="399"/>
        <source>nmi</source>
        <comment>nautical mile</comment>
        <translation>nmi</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsevenementsorbitaux.cpp" line="434"/>
        <source>evenements</source>
        <comment>file name (without accent)</comment>
        <translation>events</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsevenementsorbitaux.cpp" line="444"/>
        <source>Calculs en cours...</source>
        <translation>Calculating...</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsevenementsorbitaux.cpp" line="445"/>
        <source>Annuler</source>
        <translation>Cancel</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsevenementsorbitaux.cpp" line="468"/>
        <source>Calculs terminés</source>
        <translation>Done</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsevenementsorbitaux.cpp" line="471"/>
        <source>Aucun évènement n&apos;a été trouvé sur la période donnée</source>
        <translation>None event has been found during the given period</translation>
    </message>
</context>
<context>
    <name>CalculsFlashs</name>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsflashs.ui" line="49"/>
        <source>Lieu d&apos;observation :</source>
        <translation>Name of location :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsflashs.ui" line="69"/>
        <source>Hauteur du Soleil :</source>
        <translation>Sun elevation :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsflashs.ui" line="98"/>
        <source>Horizon (0°)</source>
        <translation>Horizon (0°)</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsflashs.ui" line="103"/>
        <source>Crépuscule civil (-6°)</source>
        <translation>Civil twilight (-6°)</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsflashs.ui" line="108"/>
        <source>Crépuscule nautique (-12°)</source>
        <translation>Nautical twilight (-12°)</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsflashs.ui" line="113"/>
        <source>Crépuscule astronomique (-18°)</source>
        <translation>Astronomical twilight (-18°)</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsflashs.ui" line="118"/>
        <source>Indifférent</source>
        <extracomment>Pas de hauteur limite pour le Soleil : les calculs de jour sont inclus</extracomment>
        <translation>Indifferent</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsflashs.ui" line="123"/>
        <location filename="../src/interface/onglets/previsions/calculsflashs.ui" line="398"/>
        <source>Autre...</source>
        <translation>Other...</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsflashs.ui" line="180"/>
        <source>Classer par ordre chronologique</source>
        <translation>Chronologically sorting</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsflashs.ui" line="199"/>
        <source>Paramétrage par défaut</source>
        <translation>Default settings</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsflashs.ui" line="224"/>
        <source>Date finale :</source>
        <translation>End date :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsflashs.ui" line="253"/>
        <location filename="../src/interface/onglets/previsions/calculsflashs.ui" line="285"/>
        <source>dd/MM/yyyy HH:mm:ss</source>
        <extracomment>Date format</extracomment>
        <translation>MM/dd/yyyy HH:mm:ss</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsflashs.ui" line="298"/>
        <source>Date initiale :</source>
        <translation>Start date :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsflashs.ui" line="314"/>
        <source>Effacer heures</source>
        <translation>Erase hours</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsflashs.ui" line="350"/>
        <source>Hauteur minimale du satellite :</source>
        <translation>Minimum elevation of satellite :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsflashs.ui" line="373"/>
        <source>0°</source>
        <translation>0°</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsflashs.ui" line="378"/>
        <source>5°</source>
        <translation>5°</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsflashs.ui" line="383"/>
        <source>10°</source>
        <translation>10°</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsflashs.ui" line="388"/>
        <source>15°</source>
        <translation>15°</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsflashs.ui" line="393"/>
        <source>20°</source>
        <translation>20°</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsflashs.ui" line="464"/>
        <source>Magnitude maximale :</source>
        <translation>Maximum magnitude :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsflashs.ui" line="505"/>
        <source>Calculs</source>
        <translation>Run</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsflashs.ui" line="543"/>
        <source>Ages des éléments orbitaux : [</source>
        <translation>Ages of orbital elements : [</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsflashs.ui" line="569"/>
        <source>] jours</source>
        <translation>] days</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsflashs.ui" line="585"/>
        <source>Mettre à jour flares-spctrk.xml...</source>
        <translation>Update flares-spctrk.xml...</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsflashs.cpp" line="348"/>
        <source>km</source>
        <comment>kilometer</comment>
        <translation>km</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsflashs.cpp" line="348"/>
        <source>nmi</source>
        <comment>nautical mile</comment>
        <translation>nmi</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsflashs.cpp" line="403"/>
        <source>Aucun satellite produisant des flashs n&apos;a été trouvé dans le fichier d&apos;éléments orbitaux</source>
        <translation>There is no satellites producing flares in the orbital element file</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsflashs.cpp" line="437"/>
        <source>flashs</source>
        <comment>file name (without accent)</comment>
        <translation>flares</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsflashs.cpp" line="447"/>
        <source>Calculs en cours...</source>
        <translation>Calculating...</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsflashs.cpp" line="448"/>
        <source>Annuler</source>
        <translation>Cancel</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsflashs.cpp" line="471"/>
        <source>Calculs terminés</source>
        <translation>Done</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsflashs.cpp" line="475"/>
        <source>Aucun flash n&apos;a été trouvé sur la période donnée</source>
        <translation>None flare has been found during the given period</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsflashs.cpp" line="583"/>
        <source>Mise à jour du fichier GP %1 en cours...</source>
        <translation>Updating GP file %1...</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsflashs.cpp" line="588"/>
        <source>Téléchargement terminé</source>
        <translation>Downloading finished</translation>
    </message>
</context>
<context>
    <name>CalculsPrevisions</name>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsprevisions.ui" line="53"/>
        <source>Illumination requise</source>
        <translation>Illumination required</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsprevisions.ui" line="75"/>
        <source>Magnitude maximale</source>
        <translation>Maximum magnitude</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsprevisions.ui" line="190"/>
        <location filename="../src/interface/onglets/previsions/calculsprevisions.ui" line="245"/>
        <source>dd/MM/yyyy HH:mm:ss</source>
        <extracomment>Date format</extracomment>
        <translation>MM/dd/yyyy HH:mm:ss</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsprevisions.ui" line="206"/>
        <source>Date finale :</source>
        <translation>End date :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsprevisions.ui" line="261"/>
        <source>Date initiale :</source>
        <translation>Start date :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsprevisions.ui" line="280"/>
        <source>Effacer heures</source>
        <translation>Erase hours</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsprevisions.ui" line="302"/>
        <source>Calculs</source>
        <translation>Run</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsprevisions.ui" line="338"/>
        <source>Pas de génération :</source>
        <translation>Output step :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsprevisions.ui" line="351"/>
        <source>Hauteur du Soleil :</source>
        <translation>Sun elevation :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsprevisions.ui" line="364"/>
        <source>Lieu d&apos;observation :</source>
        <translation>Name of location :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsprevisions.ui" line="393"/>
        <source>1 seconde</source>
        <translation>1 second</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsprevisions.ui" line="398"/>
        <source>5 secondes</source>
        <translation>5 seconds</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsprevisions.ui" line="403"/>
        <source>10 secondes</source>
        <translation>10 seconds</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsprevisions.ui" line="408"/>
        <source>20 secondes</source>
        <translation>20  seconds</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsprevisions.ui" line="413"/>
        <source>30 secondes</source>
        <translation>30 seconds</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsprevisions.ui" line="418"/>
        <source>1 minute</source>
        <translation>1 minute</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsprevisions.ui" line="423"/>
        <source>2 minutes</source>
        <translation>2 minutes</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsprevisions.ui" line="428"/>
        <source>5 minutes</source>
        <translation>5 minutes</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsprevisions.ui" line="480"/>
        <source>Horizon (0°)</source>
        <translation>Horizon (0°)</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsprevisions.ui" line="485"/>
        <source>Crépuscule civil (-6°)</source>
        <translation>Civil twilight (-6°)</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsprevisions.ui" line="490"/>
        <source>Crépuscule nautique (-12°)</source>
        <translation>Nautical twilight (-12°)</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsprevisions.ui" line="495"/>
        <source>Crépuscule astronomique (-18°)</source>
        <translation>Astronomical twilight (-18°)</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsprevisions.ui" line="500"/>
        <source>Indifférent</source>
        <extracomment>Pas de hauteur limite pour le Soleil : les calculs de jour sont inclus</extracomment>
        <translation>Indifferent</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsprevisions.ui" line="505"/>
        <location filename="../src/interface/onglets/previsions/calculsprevisions.ui" line="654"/>
        <source>Autre...</source>
        <translation>Other...</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsprevisions.ui" line="600"/>
        <source>Hauteur minimale du satellite :</source>
        <translation>Minimum elevation of satellite :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsprevisions.ui" line="629"/>
        <source>0°</source>
        <translation>0°</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsprevisions.ui" line="634"/>
        <source>5°</source>
        <translation>5°</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsprevisions.ui" line="639"/>
        <source>10°</source>
        <translation>10°</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsprevisions.ui" line="644"/>
        <source>15°</source>
        <translation>15°</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsprevisions.ui" line="649"/>
        <source>20°</source>
        <translation>20°</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsprevisions.ui" line="711"/>
        <source>Paramétrage par défaut</source>
        <translation>Default settings</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsprevisions.ui" line="730"/>
        <source>Filtre</source>
        <translation>Filter</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsprevisions.ui" line="768"/>
        <source>Ages des éléments orbitaux : [</source>
        <translation>Ages of orbital elements : [</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsprevisions.ui" line="794"/>
        <source>] jours</source>
        <translation>] days</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsprevisions.ui" line="810"/>
        <source>Mettre à jour les éléments orbitaux...</source>
        <translation>Update orbital elements...</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsprevisions.cpp" line="348"/>
        <source>Aucun</source>
        <translation>None</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsprevisions.cpp" line="351"/>
        <source>Tous</source>
        <translation>All</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsprevisions.cpp" line="405"/>
        <source>Aucun satellite n&apos;est sélectionné dans la liste</source>
        <translation>None satellite selected in the list</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsprevisions.cpp" line="446"/>
        <source>km</source>
        <comment>kilometer</comment>
        <translation>km</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsprevisions.cpp" line="446"/>
        <source>nmi</source>
        <comment>nautical mile</comment>
        <translation>nmi</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsprevisions.cpp" line="517"/>
        <source>previsions</source>
        <comment>filename (without accent)</comment>
        <translation>predictions</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsprevisions.cpp" line="527"/>
        <source>Calculs en cours...</source>
        <translation>Calculating...</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsprevisions.cpp" line="528"/>
        <source>Annuler</source>
        <translation>Cancel</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsprevisions.cpp" line="551"/>
        <source>Calculs terminés</source>
        <translation>Done</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsprevisions.cpp" line="555"/>
        <source>Aucun passage n&apos;a été trouvé sur la période donnée</source>
        <translation>None pass has been found during the given period</translation>
    </message>
</context>
<context>
    <name>CalculsStarlink</name>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsstarlink.ui" line="35"/>
        <source>Groupe :</source>
        <extracomment>Starlink group</extracomment>
        <translation>Group :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsstarlink.ui" line="278"/>
        <source>Hauteur minimale du satellite :</source>
        <translation>Minimum elevation of satellite :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsstarlink.ui" line="338"/>
        <source>0°</source>
        <translation>0°</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsstarlink.ui" line="343"/>
        <source>5°</source>
        <translation>5°</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsstarlink.ui" line="348"/>
        <source>10°</source>
        <translation>10°</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsstarlink.ui" line="353"/>
        <source>15°</source>
        <translation>15°</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsstarlink.ui" line="358"/>
        <source>20°</source>
        <translation>20°</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsstarlink.ui" line="363"/>
        <location filename="../src/interface/onglets/previsions/calculsstarlink.ui" line="468"/>
        <source>Autre...</source>
        <translation>Other...</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsstarlink.ui" line="72"/>
        <source>Paramétrage par défaut</source>
        <translation>Default settings</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsstarlink.ui" line="91"/>
        <source>Calculs</source>
        <translation>Run</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsstarlink.ui" line="127"/>
        <source>Pas de génération :</source>
        <translation>Output step :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsstarlink.ui" line="291"/>
        <source>Hauteur du Soleil :</source>
        <translation>Sun elevation :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsstarlink.ui" line="140"/>
        <source>Lieu d&apos;observation :</source>
        <translation>Name of location :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsstarlink.ui" line="169"/>
        <source>1 seconde</source>
        <translation>1 second</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsstarlink.ui" line="174"/>
        <source>5 secondes</source>
        <translation>5 seconds</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsstarlink.ui" line="179"/>
        <source>10 secondes</source>
        <translation>10 seconds</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsstarlink.ui" line="184"/>
        <source>20 secondes</source>
        <translation>20  seconds</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsstarlink.ui" line="189"/>
        <source>30 secondes</source>
        <translation>30 seconds</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsstarlink.ui" line="194"/>
        <source>1 minute</source>
        <translation>1 minute</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsstarlink.ui" line="199"/>
        <source>2 minutes</source>
        <translation>2 minutes</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsstarlink.ui" line="204"/>
        <source>5 minutes</source>
        <translation>5 minutes</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsstarlink.ui" line="443"/>
        <source>Horizon (0°)</source>
        <translation>Horizon (0°)</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsstarlink.ui" line="448"/>
        <source>Crépuscule civil (-6°)</source>
        <translation>Civil twilight (-6°)</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsstarlink.ui" line="453"/>
        <source>Crépuscule nautique (-12°)</source>
        <translation>Nautical twilight (-12°)</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsstarlink.ui" line="458"/>
        <source>Crépuscule astronomique (-18°)</source>
        <translation>Astronomical twilight (-18°)</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsstarlink.ui" line="463"/>
        <source>Indifférent</source>
        <extracomment>Pas de hauteur limite pour le Soleil : les calculs de jour sont inclus</extracomment>
        <translation>Indifferent</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsstarlink.ui" line="578"/>
        <source>Ouvrir RocketLaunch.Live</source>
        <translation>Open RocketLaunch.Live</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsstarlink.ui" line="591"/>
        <source>Vérifier les éléments orbitaux disponibles...</source>
        <translation>Check the available orbital elements...</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsstarlink.ui" line="212"/>
        <source>Nombre de jours de génération :</source>
        <translation>Number of generation days :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsstarlink.ui" line="541"/>
        <source>Lancement :</source>
        <extracomment>Starlink launch</extracomment>
        <translation>Launch :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsstarlink.ui" line="555"/>
        <source>Déploiement :</source>
        <extracomment>Starlink deployment</extracomment>
        <translation>Deployment :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsstarlink.cpp" line="210"/>
        <location filename="../src/interface/onglets/previsions/calculsstarlink.cpp" line="211"/>
        <location filename="../src/interface/onglets/previsions/calculsstarlink.cpp" line="309"/>
        <location filename="../src/interface/onglets/previsions/calculsstarlink.cpp" line="310"/>
        <source>UTC</source>
        <translation>UTC</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsstarlink.cpp" line="215"/>
        <source>Aucun groupe Starlink trouvé</source>
        <translation>No Starlink groups found</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsstarlink.cpp" line="454"/>
        <source>km</source>
        <comment>kilometer</comment>
        <translation>km</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsstarlink.cpp" line="454"/>
        <source>nmi</source>
        <comment>nautical mile</comment>
        <translation>nmi</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsstarlink.cpp" line="523"/>
        <source>starlink</source>
        <comment>filename (without accent)</comment>
        <translation>starlink</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsstarlink.cpp" line="533"/>
        <source>Calculs en cours...</source>
        <translation>Calculating...</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsstarlink.cpp" line="534"/>
        <source>Annuler</source>
        <translation>Cancel</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsstarlink.cpp" line="557"/>
        <source>Calculs terminés</source>
        <translation>Done</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsstarlink.cpp" line="561"/>
        <source>Aucun passage n&apos;a été trouvé sur la période donnée</source>
        <translation>None pass has been found during the given period</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsstarlink.cpp" line="588"/>
        <source>Mise à jour des éléments orbitaux...</source>
        <translation>Updating orbital elements...</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculsstarlink.cpp" line="594"/>
        <source>Téléchargement terminé</source>
        <translation>Downloading finished</translation>
    </message>
</context>
<context>
    <name>CalculsTransits</name>
    <message>
        <location filename="../src/interface/onglets/previsions/calculstransits.ui" line="32"/>
        <source>Inclure transits/conjonctions lunaires de jour</source>
        <translation>Include daytime lunar transits/conjunctions</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculstransits.ui" line="63"/>
        <source>Élongation maximale avec le corps :</source>
        <translation>Maximum elongation with the body :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculstransits.ui" line="107"/>
        <source>Calculs</source>
        <translation>Run</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculstransits.ui" line="143"/>
        <source>Hauteur minimale du satellite :</source>
        <translation>Minimum elevation of satellite :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculstransits.ui" line="160"/>
        <source>0°</source>
        <translation>0°</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculstransits.ui" line="165"/>
        <source>5°</source>
        <translation>5°</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculstransits.ui" line="170"/>
        <source>10°</source>
        <translation>10°</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculstransits.ui" line="175"/>
        <source>15°</source>
        <translation>15°</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculstransits.ui" line="180"/>
        <source>20°</source>
        <translation>20°</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculstransits.ui" line="185"/>
        <source>Autre...</source>
        <translation>Other...</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculstransits.ui" line="248"/>
        <source>Date initiale :</source>
        <translation>Start date :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculstransits.ui" line="258"/>
        <source>Date finale :</source>
        <translation>End date :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculstransits.ui" line="277"/>
        <location filename="../src/interface/onglets/previsions/calculstransits.ui" line="299"/>
        <source>dd/MM/yyyy HH:mm:ss</source>
        <extracomment>Date format</extracomment>
        <translation>MM/dd/yyyy HH:mm:ss</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculstransits.ui" line="318"/>
        <source>Effacer heures</source>
        <translation>Erase hours</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculstransits.ui" line="334"/>
        <source>Mettre à jour Éléments orbitaux de l&apos;ISS...</source>
        <translation>Update ISS orbital elements...</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculstransits.ui" line="544"/>
        <source>Ages des éléments orbitaux : [</source>
        <translation>Ages of orbital elements : [</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculstransits.ui" line="570"/>
        <source>] jours</source>
        <translation>] days</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculstransits.ui" line="586"/>
        <source>Mettre à jour...</source>
        <translation>Update...</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculstransits.ui" line="353"/>
        <source>Corps</source>
        <translation>Body</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculstransits.ui" line="368"/>
        <source>Soleil</source>
        <translation>Sun</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculstransits.ui" line="387"/>
        <source>Lune</source>
        <translation>Moon</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculstransits.ui" line="419"/>
        <source>Lieu d&apos;observation :</source>
        <translation>Name of location :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculstransits.ui" line="454"/>
        <source>Paramétrage par défaut</source>
        <translation>Default settings</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculstransits.ui" line="506"/>
        <source>Filtre</source>
        <translation>Filter</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculstransits.cpp" line="351"/>
        <source>Les éléments orbitaux de l&apos;ISS sont vieux, il est conseillé de les mettre à jour</source>
        <translation>The orbital elements of ISS are old, it is recommended to update them</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculstransits.cpp" line="389"/>
        <source>Aucun</source>
        <translation>None</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculstransits.cpp" line="392"/>
        <source>Tous</source>
        <translation>All</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculstransits.cpp" line="470"/>
        <source>km</source>
        <comment>kilometer</comment>
        <translation>km</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculstransits.cpp" line="470"/>
        <source>nmi</source>
        <comment>nautical mile</comment>
        <translation>nmi</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculstransits.cpp" line="526"/>
        <source>transits</source>
        <comment>file name (without accent)</comment>
        <translation>transits</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculstransits.cpp" line="536"/>
        <source>Calculs en cours...</source>
        <translation>Calculating...</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculstransits.cpp" line="537"/>
        <source>Annuler</source>
        <translation>Cancel</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculstransits.cpp" line="560"/>
        <source>Calculs terminés</source>
        <translation>Done</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculstransits.cpp" line="563"/>
        <source>Aucun transit ISS n&apos;a été trouvé sur la période donnée</source>
        <translation>None transit has been found during the given period</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculstransits.cpp" line="692"/>
        <source>Téléchargement du fichier d&apos;élements orbitaux de l&apos;ISS...</source>
        <translation>Downloading ISS orbital elements file...</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/previsions/calculstransits.cpp" line="696"/>
        <source>Téléchargement terminé</source>
        <translation>Downloading finished</translation>
    </message>
</context>
<context>
    <name>Carte</name>
    <message>
        <location filename="../src/interface/carte/carte.cpp" line="150"/>
        <source>Est</source>
        <translation>East</translation>
    </message>
    <message>
        <location filename="../src/interface/carte/carte.cpp" line="150"/>
        <source>Ouest</source>
        <translation>West</translation>
    </message>
    <message>
        <location filename="../src/interface/carte/carte.cpp" line="154"/>
        <source>Sud</source>
        <translation>South</translation>
    </message>
    <message>
        <location filename="../src/interface/carte/carte.cpp" line="154"/>
        <source>Nord</source>
        <translation>North</translation>
    </message>
    <message>
        <location filename="../src/interface/carte/carte.cpp" line="157"/>
        <source>Longitude : %1° %2</source>
        <translation>Longitude : %1° %2</translation>
    </message>
    <message>
        <location filename="../src/interface/carte/carte.cpp" line="158"/>
        <source>Latitude : %1° %2</source>
        <translation>Latitude : %1° %2</translation>
    </message>
    <message>
        <location filename="../src/interface/carte/carte.cpp" line="177"/>
        <source>&lt;font color=&apos;blue&apos;&gt;&lt;b&gt;%1&lt;/b&gt;&lt;/font&gt;&lt;br /&gt;NORAD : &lt;b&gt;%2&lt;/b&gt;&lt;br /&gt;COSPAR : &lt;b&gt;%3&lt;/b&gt;</source>
        <translation>&lt;font color=&apos;blue&apos;&gt;&lt;b&gt;%1&lt;/b&gt;&lt;/font&gt;&lt;br /&gt;NORAD : &lt;b&gt;%2&lt;/b&gt;&lt;br /&gt;COSPAR : &lt;b&gt;%3&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../src/interface/carte/carte.cpp" line="180"/>
        <source>&lt;b&gt;%1&lt;/b&gt; (numéro NORAD : &lt;b&gt;%2&lt;/b&gt;  -  COSPAR : &lt;b&gt;%3&lt;/b&gt;)</source>
        <translation>&lt;b&gt;%1&lt;/b&gt; (NORAD number : &lt;b&gt;%2&lt;/b&gt;  -  COSPAR : &lt;b&gt;%3&lt;/b&gt;)</translation>
    </message>
    <message>
        <location filename="../src/interface/carte/carte.cpp" line="205"/>
        <location filename="../src/interface/carte/carte.cpp" line="206"/>
        <source>Soleil</source>
        <translation>Sun</translation>
    </message>
    <message>
        <location filename="../src/interface/carte/carte.cpp" line="231"/>
        <location filename="../src/interface/carte/carte.cpp" line="232"/>
        <source>Lune</source>
        <translation>Moon</translation>
    </message>
    <message>
        <location filename="../src/interface/carte/carte.cpp" line="1446"/>
        <source>W</source>
        <comment>Symbol for West</comment>
        <translation>W</translation>
    </message>
    <message>
        <location filename="../src/interface/carte/carte.cpp" line="1452"/>
        <source>E</source>
        <comment>Symbol for East</comment>
        <translation>E</translation>
    </message>
    <message>
        <location filename="../src/interface/carte/carte.cpp" line="1480"/>
        <source>N</source>
        <comment>Symbol for North</comment>
        <translation>N</translation>
    </message>
    <message>
        <location filename="../src/interface/carte/carte.cpp" line="1486"/>
        <source>S</source>
        <comment>Symbol for South</comment>
        <translation>S</translation>
    </message>
    <message>
        <location filename="../src/interface/carte/carte.cpp" line="1782"/>
        <source>Le fichier %1 n&apos;existe pas, veuillez réinstaller %2</source>
        <translation>The file %1 does not exist, please re-install %2</translation>
    </message>
</context>
<context>
    <name>Ciel</name>
    <message>
        <location filename="../src/interface/ciel/ciel.ui" line="70"/>
        <source>Nord</source>
        <translation>North</translation>
    </message>
    <message>
        <location filename="../src/interface/ciel/ciel.ui" line="91"/>
        <source>Est</source>
        <translation>East</translation>
    </message>
    <message>
        <location filename="../src/interface/ciel/ciel.ui" line="132"/>
        <source>Ouest</source>
        <translation>West</translation>
    </message>
    <message>
        <location filename="../src/interface/ciel/ciel.ui" line="156"/>
        <source>Sud</source>
        <translation>South</translation>
    </message>
    <message>
        <location filename="../src/interface/ciel/ciel.cpp" line="1069"/>
        <source>Flash %1</source>
        <translation>Flare %1</translation>
    </message>
    <message>
        <location filename="../src/interface/ciel/ciel.cpp" line="387"/>
        <source>Ascension droite : %1</source>
        <translation>Right ascension : %1</translation>
    </message>
    <message>
        <location filename="../src/interface/ciel/ciel.cpp" line="389"/>
        <source>Déclinaison : %1</source>
        <translation>Declination : %1</translation>
    </message>
    <message>
        <location filename="../src/interface/ciel/ciel.cpp" line="406"/>
        <source>&lt;font color=&apos;blue&apos;&gt;&lt;b&gt;%1&lt;/b&gt;&lt;/font&gt;&lt;br /&gt;NORAD : &lt;b&gt;%2&lt;/b&gt;&lt;br /&gt;COSPAR : &lt;b&gt;%3&lt;/b&gt;</source>
        <translation>&lt;font color=&apos;blue&apos;&gt;&lt;b&gt;%1&lt;/b&gt;&lt;/font&gt;&lt;br /&gt;NORAD : &lt;b&gt;%2&lt;/b&gt;&lt;br /&gt;COSPAR : &lt;b&gt;%3&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../src/interface/ciel/ciel.cpp" line="409"/>
        <source>&lt;b&gt;%1&lt;/b&gt; (numéro NORAD : &lt;b&gt;%2&lt;/b&gt;  -  COSPAR : &lt;b&gt;%3&lt;/b&gt;)</source>
        <translation>&lt;b&gt;%1&lt;/b&gt; (NORAD number : &lt;b&gt;%2&lt;/b&gt;  -  COSPAR : &lt;b&gt;%3&lt;/b&gt;)</translation>
    </message>
    <message>
        <location filename="../src/interface/ciel/ciel.cpp" line="468"/>
        <location filename="../src/interface/ciel/ciel.cpp" line="469"/>
        <source>Soleil</source>
        <translation>Sun</translation>
    </message>
    <message>
        <location filename="../src/interface/ciel/ciel.cpp" line="495"/>
        <location filename="../src/interface/ciel/ciel.cpp" line="496"/>
        <source>Lune</source>
        <translation>Moon</translation>
    </message>
</context>
<context>
    <name>General</name>
    <message>
        <location filename="../src/interface/onglets/general/general.ui" line="84"/>
        <location filename="../src/interface/onglets/general/general.ui" line="139"/>
        <source>Date :</source>
        <extracomment>Date and hour</extracomment>
        <translation>Date :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.ui" line="152"/>
        <source>dddd dd MMMM yyyy HH:mm:ss</source>
        <translation>dddd, MMMM dd yyyy  HH:mm:ss</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.ui" line="189"/>
        <location filename="../src/interface/onglets/general/general.cpp" line="842"/>
        <source>Lieu d&apos;observation :</source>
        <translation>Name of location :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.ui" line="226"/>
        <location filename="../src/interface/onglets/general/general.ui" line="529"/>
        <location filename="../src/interface/onglets/general/general.ui" line="1824"/>
        <source>Longitude :</source>
        <translation>Longitude :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.ui" line="258"/>
        <location filename="../src/interface/onglets/general/general.ui" line="555"/>
        <location filename="../src/interface/onglets/general/general.ui" line="1850"/>
        <source>Latitude :</source>
        <translation>Latitude :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.ui" line="290"/>
        <source>Altitude :</source>
        <extracomment>altitude of observer</extracomment>
        <translation>Altitude :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.ui" line="328"/>
        <source>Conditions :</source>
        <extracomment>Conditions of observations</extracomment>
        <translation>Conditions :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.ui" line="335"/>
        <source>Crépuscule astronomique</source>
        <translation>Astronomical twilight</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.ui" line="495"/>
        <location filename="../src/interface/onglets/general/general.cpp" line="893"/>
        <source>Coordonnées du Soleil :</source>
        <translation>Sun coordinates :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.ui" line="388"/>
        <location filename="../src/interface/onglets/general/general.ui" line="777"/>
        <location filename="../src/interface/onglets/general/general.ui" line="1932"/>
        <source>Ascension droite :</source>
        <translation>Right ascension :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.ui" line="401"/>
        <location filename="../src/interface/onglets/general/general.ui" line="790"/>
        <location filename="../src/interface/onglets/general/general.ui" line="1945"/>
        <source>Déclinaison :</source>
        <translation>Declination :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.ui" line="414"/>
        <location filename="../src/interface/onglets/general/general.ui" line="803"/>
        <location filename="../src/interface/onglets/general/general.ui" line="1958"/>
        <source>Constellation :</source>
        <translation>Constellation :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.ui" line="640"/>
        <location filename="../src/interface/onglets/general/general.ui" line="885"/>
        <location filename="../src/interface/onglets/general/general.ui" line="1713"/>
        <source>Hauteur :</source>
        <translation>Elevation :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.ui" line="653"/>
        <location filename="../src/interface/onglets/general/general.ui" line="898"/>
        <location filename="../src/interface/onglets/general/general.ui" line="1726"/>
        <source>Azimut (N) :</source>
        <extracomment>Azimuth (from the North)</extracomment>
        <translation>Azimuth (N) :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.ui" line="666"/>
        <location filename="../src/interface/onglets/general/general.ui" line="911"/>
        <location filename="../src/interface/onglets/general/general.ui" line="1739"/>
        <source>Distance :</source>
        <translation>Range :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.ui" line="743"/>
        <location filename="../src/interface/onglets/general/general.cpp" line="920"/>
        <source>Coordonnées de la Lune :</source>
        <translation>Moon coordinates :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.ui" line="996"/>
        <source>Phase :</source>
        <extracomment>Moon phase</extracomment>
        <translation>Phase :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.ui" line="1012"/>
        <source>Magn. (Illum.) :</source>
        <extracomment>Magnitude, illumination</extracomment>
        <translation>Magn. (Illum.) :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.ui" line="1103"/>
        <source>Aube astronomique :</source>
        <translation>Astronomical dawn :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.ui" line="1116"/>
        <source>Aube nautique :</source>
        <translation>Nautical dawn :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.ui" line="1129"/>
        <source>Aube civile :</source>
        <translation>Civil dawn :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.ui" line="1303"/>
        <location filename="../src/interface/onglets/general/general.cpp" line="906"/>
        <source>Évènements Soleil :</source>
        <translation>Solar events :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.ui" line="1337"/>
        <source>Lever :</source>
        <comment>Sun</comment>
        <translation>Sunrise :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.ui" line="1350"/>
        <source>Méridien :</source>
        <comment>Sun</comment>
        <translation>Meridian :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.ui" line="1363"/>
        <source>Coucher :</source>
        <comment>Sun</comment>
        <translation>Sunset :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.ui" line="1202"/>
        <source>Crépuscule civil :</source>
        <translation>Civil twilight :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.ui" line="587"/>
        <source>Diam. apparent :</source>
        <extracomment>Angular diameter</extracomment>
        <translation>Apparent diam :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.ui" line="1009"/>
        <source>Magnitude (Illumination)</source>
        <translation>Magnitude (Illumination)</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.ui" line="584"/>
        <location filename="../src/interface/onglets/general/general.ui" line="1025"/>
        <source>Diamètre apparent</source>
        <translation>Apparent diameter</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.ui" line="1028"/>
        <source>Diam. app. :</source>
        <extracomment>Angular diameter</extracomment>
        <translation>App. diam. :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.ui" line="1231"/>
        <source>Crépuscule nautique :</source>
        <translation>Nautical twilight :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.ui" line="1260"/>
        <source>Crépuscule astronomique :</source>
        <translation>Astronomical twilight :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.ui" line="1437"/>
        <location filename="../src/interface/onglets/general/general.cpp" line="931"/>
        <source>Évènements Lune :</source>
        <translation>Lunar events :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.ui" line="1471"/>
        <source>Lever :</source>
        <comment>Moon</comment>
        <translation>Moonrise :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.ui" line="1484"/>
        <source>Méridien :</source>
        <comment>Moon</comment>
        <translation>Meridian :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.ui" line="1497"/>
        <source>Coucher :</source>
        <comment>Moon</comment>
        <translation>Moonset :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.ui" line="1567"/>
        <source>Nouvelle Lune :</source>
        <translation>New Moon :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.ui" line="1581"/>
        <source>Premier quartier :</source>
        <translation>First quarter :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.ui" line="1595"/>
        <source>Pleine Lune :</source>
        <translation>Full Moon :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.ui" line="1609"/>
        <source>Dernier quartier :</source>
        <translation>Last quarter :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.ui" line="1879"/>
        <source>Altitude :</source>
        <comment>Altitude of satellite</comment>
        <translation>Altitude :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.ui" line="2049"/>
        <source>Direction :</source>
        <translation>Direction :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.ui" line="2062"/>
        <source>Vitesse orbitale :</source>
        <translation>Orbital velocity :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.ui" line="2075"/>
        <source>Variation distance :</source>
        <translation>Range rate :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.ui" line="2107"/>
        <location filename="../src/interface/onglets/general/general.ui" line="2129"/>
        <source>Double-cliquer pour changer d&apos;unités</source>
        <translation>Double clic to change units</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.ui" line="2182"/>
        <location filename="../src/interface/onglets/general/general.ui" line="2321"/>
        <source>Orbite n°</source>
        <translation>Orbit #</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.ui" line="2419"/>
        <source>Temps écoulé depuis l&apos;époque :</source>
        <translation>Time elapsed since epoch :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.ui" line="2451"/>
        <source>Nom :</source>
        <translation>Name :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="64"/>
        <source>Coordonnées du Soleil</source>
        <translation>Sun coordinates</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="65"/>
        <source>Coordonnées de la Lune</source>
        <translation>Moon coordinates</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="66"/>
        <source>Évènements Soleil</source>
        <translation>Solar events</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="67"/>
        <source>Évènements Lune</source>
        <translation>Lunar events</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="240"/>
        <source>UTC %1 %2</source>
        <comment>Universal Time Coordinated</comment>
        <translation>UTC %1 %2</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="243"/>
        <source>UTC</source>
        <comment>Universal Time Coordinated</comment>
        <translation>UTC</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="267"/>
        <location filename="../src/interface/onglets/general/general.cpp" line="624"/>
        <source>km</source>
        <comment>Kilometer</comment>
        <translation>km</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="267"/>
        <location filename="../src/interface/onglets/general/general.cpp" line="624"/>
        <source>nmi</source>
        <comment>nautical mile</comment>
        <translation>nmi</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="291"/>
        <source>jours</source>
        <translation>days</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="294"/>
        <location filename="../src/interface/onglets/general/general.cpp" line="594"/>
        <location filename="../src/interface/onglets/general/general.cpp" line="791"/>
        <source>Ouest</source>
        <translation>West</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="294"/>
        <location filename="../src/interface/onglets/general/general.cpp" line="594"/>
        <location filename="../src/interface/onglets/general/general.cpp" line="791"/>
        <source>Est</source>
        <translation>East</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="296"/>
        <location filename="../src/interface/onglets/general/general.cpp" line="596"/>
        <location filename="../src/interface/onglets/general/general.cpp" line="792"/>
        <source>Nord</source>
        <translation>North</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="296"/>
        <location filename="../src/interface/onglets/general/general.cpp" line="596"/>
        <location filename="../src/interface/onglets/general/general.cpp" line="792"/>
        <source>Sud</source>
        <translation>South</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="319"/>
        <source>Ascendant</source>
        <translation>Ascending</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="319"/>
        <source>Descendant</source>
        <translation>Descending</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="323"/>
        <source>Orbite n°%1</source>
        <translation>Orbit #%1</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="329"/>
        <source>Lune</source>
        <translation>Moon</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="343"/>
        <location filename="../src/interface/onglets/general/general.cpp" line="661"/>
        <source>Satellite en éclipse totale%1</source>
        <translation>Satellite in%1 total eclipse</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="347"/>
        <source>Magnitude (Illumination) : %1 (%2%)</source>
        <translation>Magnitude (Illumination) : %1 (%2%)</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="406"/>
        <source>S</source>
        <comment>Sun</comment>
        <translation>S</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="406"/>
        <source>L</source>
        <comment>Moon</comment>
        <translation>M</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="409"/>
        <source>P</source>
        <comment>partial eclipse</comment>
        <translation>P</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="413"/>
        <source>A</source>
        <comment>annular eclipse</comment>
        <translation>A</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="440"/>
        <location filename="../src/interface/onglets/general/general.cpp" line="489"/>
        <source>Prochain %1 :</source>
        <translation>Next %1 :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="442"/>
        <source>N&gt;J</source>
        <comment>Night to day</comment>
        <translation>N&gt;D</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="442"/>
        <source>J&gt;N</source>
        <comment>Day to night</comment>
        <translation>D&gt;N</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="443"/>
        <source>Nuit &gt; Jour</source>
        <translation>Night &gt; Day</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="443"/>
        <source>Jour &gt; Nuit</source>
        <translation>Day &gt; Night</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="458"/>
        <location filename="../src/interface/onglets/general/general.cpp" line="511"/>
        <source>h</source>
        <comment>hour</comment>
        <translation>h</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="458"/>
        <location filename="../src/interface/onglets/general/general.cpp" line="462"/>
        <location filename="../src/interface/onglets/general/general.cpp" line="511"/>
        <location filename="../src/interface/onglets/general/general.cpp" line="515"/>
        <source>min</source>
        <comment>minute</comment>
        <translation>min</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="462"/>
        <location filename="../src/interface/onglets/general/general.cpp" line="515"/>
        <source>s</source>
        <comment>second</comment>
        <translation>s</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="492"/>
        <source>AOS</source>
        <translation>AOS</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="492"/>
        <source>Acquisition du signal</source>
        <translation>Acquisition of signal</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="492"/>
        <source>Perte du signal</source>
        <translation>Loss of signal</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="446"/>
        <location filename="../src/interface/onglets/general/general.cpp" line="496"/>
        <source>%1  (dans %2).</source>
        <comment>Delay in hours, minutes or seconds</comment>
        <translation>%1  (in %2).</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="525"/>
        <source>Azimut : %1</source>
        <translation>Azimuth : %1</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="541"/>
        <source>Beta : %1</source>
        <comment>Beta angle (angle between orbit plane and direction of Sun)</comment>
        <translation>Beta : %1</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="575"/>
        <source>Nuit</source>
        <translation>Night</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="586"/>
        <source>%1 UA</source>
        <translation>%1 AU</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="664"/>
        <source>Satellite non éclipsé</source>
        <translation>Satellite not eclipsed</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="666"/>
        <source>Satellite en éclipse partielle%1</source>
        <translation>Satellite in%1 partial eclipse</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="669"/>
        <source>Satellite en éclipse annulaire%1</source>
        <translation>Satellite in%1 annular eclipse</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="705"/>
        <source>km/h</source>
        <comment>Kilometer per hour</comment>
        <translation>km/h</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="705"/>
        <source>km/s</source>
        <comment>Kilometer per second</comment>
        <translation>km/s</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="707"/>
        <source>kn</source>
        <comment>Knot</comment>
        <translation>kn</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="707"/>
        <source>nmi/s</source>
        <comment>Nautical mile per second</comment>
        <translation>nmi/s</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="800"/>
        <source>m</source>
        <comment>meter</comment>
        <translation>m</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="800"/>
        <source>ft</source>
        <comment>foot</comment>
        <translation>ft</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="828"/>
        <source>Problème de droits d&apos;écriture du fichier %1</source>
        <translation>Unable to write file %1</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="840"/>
        <source>Date :</source>
        <comment>Date and hour</comment>
        <translation>Date :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="843"/>
        <source>Longitude  : %1	Latitude : %2	Altitude : %3</source>
        <comment>Observer coordinates</comment>
        <translation>Longitude  : %1	Latitude : %2	Altitude : %3</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="845"/>
        <source>Conditions : %1</source>
        <comment>Conditions of observation</comment>
        <translation>Conditions : %1</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="853"/>
        <source>Nom du satellite :</source>
        <translation>Name of satellite :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="871"/>
        <source>Vitesse orbitale   : %1	%2  %3</source>
        <translation>Orbital velocity : %1  	%2   %3</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="859"/>
        <source>Latitude  :  %1		Azimut (N) : %2	Déclinaison      : %3</source>
        <translation>Latitude  :  %1  	Azimuth (N) : %2		Declination      : %3</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="867"/>
        <source>Direction          : %1  	%2      		%3</source>
        <translation>Direction        : %1    	%2      		%3</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="881"/>
        <source>Variation distance : %1  	%2</source>
        <comment>Range rate</comment>
        <translation>Range rate       : %1  	%2</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="932"/>
        <source>Lever    : %1</source>
        <comment>Moonrise</comment>
        <translation>Moonrise : %1</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="933"/>
        <source>Méridien : %1</source>
        <comment>Meridian pass for the Moon</comment>
        <translation>Meridian : %1</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="934"/>
        <source>Coucher  : %1</source>
        <comment>Moonset</comment>
        <translation>Moonset  : %1</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="567"/>
        <source>Jour</source>
        <comment>Sun is above horizon</comment>
        <translation>Day</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="569"/>
        <source>Crépuscule civil</source>
        <comment>Sun is 6 degrees below horizon</comment>
        <translation>Civil twilight</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="571"/>
        <source>Crépuscule nautique</source>
        <comment>Sun is 12 degrees below horizon</comment>
        <translation>Nautical twilight</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="573"/>
        <source>Crépuscule astronomique</source>
        <comment>Sun is 18 degrees below horizon</comment>
        <translation>Astronomical twilight</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="855"/>
        <source>Longitude : %1  	Hauteur    : %2	Ascension droite :  %3</source>
        <translation>Longitude : %1   	Elevation   : %2		Right ascension  :  %3</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="863"/>
        <source>Altitude  :  %1		Distance   : %2	Constellation    : %3</source>
        <comment>Altitude of satellite</comment>
        <translation>Altitude  :  %1		Range       : %2		Constellation    : %3</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="894"/>
        <source>Hauteur    : %1		Ascension droite  :  %2 	Longitude                : %3</source>
        <translation>Elevation   : %1		Right ascension   :  %2 	Longitude                : %3</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="898"/>
        <source>Azimut (N) : %1		Déclinaison       : %2 	Latitude                 : %3</source>
        <comment>Azimuth from the North</comment>
        <translation>Azimuth (N) : %1 		Declination       : %2 	Latitude                 : %3</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="902"/>
        <source>Distance   : %1   		Constellation     : %2			Diamètre apparent        : %3</source>
        <translation>Range       : %1   		Constellation     : %2			Apparent diameter        : %3</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="907"/>
        <source>Lever    : %1			Aube astronomique : %2		Crépuscule civil         : %3</source>
        <comment>Sunrise</comment>
        <translation>Sunrise  : %1			Astronomical dawn : %2		Civil twilight           : %3</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="910"/>
        <source>Méridien : %1			Aube nautique     : %2		Crépuscule nautique      : %3</source>
        <comment>Meridian pass for the Sun</comment>
        <translation>Meridian : %1			Nautical dawn     : %2		Nautical twilight        : %3</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="914"/>
        <source>Coucher  : %1			Aube civile       : %2		Crépuscule astronomique  : %3</source>
        <comment>Sunset</comment>
        <translation>Sunset   : %1			Civil dawn        : %2		Astronomical twilight    : %3</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="921"/>
        <source>Hauteur    : %1		Ascension droite :  %2 	Phase                    : %3</source>
        <comment>Moon phase</comment>
        <translation>Elevation   : %1		Right ascension   :  %2 	Phase                    : %3</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="924"/>
        <source>Azimut (N) : %1		Déclinaison      : %2 	Magnitude (Illumination) : %3</source>
        <comment>Azimuth from the North</comment>
        <translation>Azimuth (N) : %1		Declination       : %2 	Magnitude (Illumination) : %3</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="927"/>
        <source>Distance   : %1  		Constellation    : %2 			Diamètre apparent        : %3</source>
        <translation>Range       : %1   		Constellation     : %2			Apparent diameter        : %3</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="936"/>
        <source>Nouvelle Lune    : %1</source>
        <translation>New Moon      : %1</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="937"/>
        <source>Premier quartier : %1</source>
        <translation>First quarter : %1</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="938"/>
        <source>Pleine Lune      : %1</source>
        <translation>Full Moon     : %1</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/general/general.cpp" line="939"/>
        <source>Dernier quartier : %1</source>
        <translation>Last quarter  : %1</translation>
    </message>
</context>
<context>
    <name>Informations</name>
    <message>
        <location filename="../src/interface/informations/informations.ui" line="29"/>
        <source>Informations</source>
        <translation>Information</translation>
    </message>
    <message>
        <location filename="../src/interface/informations/informations.ui" line="45"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../src/interface/informations/informations.ui" line="71"/>
        <source>Dernière Information</source>
        <translation>Latest information</translation>
    </message>
    <message>
        <location filename="../src/interface/informations/informations.ui" line="91"/>
        <source>Informations plus anciennes</source>
        <translation>Older information</translation>
    </message>
    <message>
        <location filename="../src/interface/informations/informations.ui" line="124"/>
        <source>Versions</source>
        <translation>Versions</translation>
    </message>
    <message>
        <location filename="../src/interface/informations/informations.ui" line="166"/>
        <source>Afficher les informations au démarrage</source>
        <translation>Display information at startup</translation>
    </message>
</context>
<context>
    <name>InformationsISS</name>
    <message>
        <location filename="../src/interface/onglets/donnees/informationsiss.ui" line="68"/>
        <source>Évènement</source>
        <translation>Event</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/informationsiss.ui" line="78"/>
        <source>Date</source>
        <extracomment>Date and hour</extracomment>
        <translation>Date</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/informationsiss.ui" line="88"/>
        <source>ΔV</source>
        <translation>ΔV</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/informationsiss.ui" line="98"/>
        <source>Apogée</source>
        <translation>Apogee</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/informationsiss.ui" line="108"/>
        <source>Périgée</source>
        <translation>Perigee</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/informationsiss.ui" line="127"/>
        <source>Mettre à jour les informations de l&apos;ISS...</source>
        <translation>Update ISS information...</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/informationsiss.ui" line="149"/>
        <source>Masse :</source>
        <translation>Mass :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/informationsiss.ui" line="163"/>
        <source>Surface de traînée :</source>
        <translation>Drag area :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/informationsiss.ui" line="189"/>
        <source>Coefficient de traînée :</source>
        <translation>Drag coefficient :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/informationsiss.ui" line="212"/>
        <source>Fichier d&apos;informations ISS absent :
cliquer sur &apos;Mettre à jour les informations de l&apos;ISS&apos;</source>
        <translation>Missing ISS information file :
click on &apos;Update ISS information&apos;</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/informationsiss.cpp" line="135"/>
        <source>km</source>
        <comment>Kilometer</comment>
        <translation>km</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/informationsiss.cpp" line="136"/>
        <source>m/s</source>
        <comment>meter per second</comment>
        <translation>m/s</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/informationsiss.cpp" line="137"/>
        <source>kg</source>
        <comment>Kilogram</comment>
        <translation>kg</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/informationsiss.cpp" line="138"/>
        <source>m^2</source>
        <comment>meter square</comment>
        <translation>m^2</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/informationsiss.cpp" line="145"/>
        <source>nmi</source>
        <comment>nautical mile</comment>
        <translation>nmi</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/informationsiss.cpp" line="146"/>
        <source>ft/s</source>
        <comment>foot per second</comment>
        <translation>ft/s</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/informationsiss.cpp" line="147"/>
        <source>lb</source>
        <comment>pound</comment>
        <translation>lb</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/informationsiss.cpp" line="148"/>
        <source>ft^2</source>
        <comment>foot square</comment>
        <translation>ft^2</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/informationsiss.cpp" line="171"/>
        <source>Aucun évènement contenu dans le fichier ISS</source>
        <translation>No events contained in the ISS file</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/informationsiss.cpp" line="288"/>
        <source>Téléchargement du fichier d&apos;informations ISS...</source>
        <translation>Downloading ISS information file...</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/informationsiss.cpp" line="290"/>
        <source>Téléchargement terminé</source>
        <translation>Downloading finished</translation>
    </message>
</context>
<context>
    <name>InformationsSatellite</name>
    <message>
        <location filename="../src/interface/onglets/donnees/informationssatellite.ui" line="65"/>
        <source>Nom :</source>
        <translation>Name :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/informationssatellite.ui" line="193"/>
        <source>Inclinaison :</source>
        <translation>Inclination :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/informationssatellite.ui" line="206"/>
        <source>AD noeud ascendant :</source>
        <extracomment>Right ascension of the ascending node</extracomment>
        <translation>RA of ascending node :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/informationssatellite.ui" line="219"/>
        <source>Excentricité :</source>
        <translation>Eccentricity :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/informationssatellite.ui" line="232"/>
        <source>Argument du périgée :</source>
        <translation>Argument of perigee :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/informationssatellite.ui" line="333"/>
        <source>Numéro NORAD :</source>
        <translation>NORAD number :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/informationssatellite.ui" line="346"/>
        <source>Désignation COSPAR :</source>
        <translation>COSPAR designation :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/informationssatellite.ui" line="359"/>
        <source>Époque (UTC) :</source>
        <extracomment>Universal Time Coordinated</extracomment>
        <translation>Epoch (UTC) :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/informationssatellite.ui" line="372"/>
        <source>Coeff pseudo-balistique :</source>
        <extracomment>Pseudo-ballistic coefficient</extracomment>
        <translation>Pseudo-ballistic coeff :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/informationssatellite.ui" line="458"/>
        <source>Date de lancement :</source>
        <translation>Launch date :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/informationssatellite.ui" line="468"/>
        <source>Catégorie d&apos;orbite :</source>
        <translation>Orbital category :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/informationssatellite.ui" line="478"/>
        <source>Pays/Organisation :</source>
        <translation>Country/Organization :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/informationssatellite.ui" line="488"/>
        <source>Site de lancement :</source>
        <translation>Launch site :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/informationssatellite.ui" line="576"/>
        <location filename="../src/interface/onglets/donnees/informationssatellite.ui" line="676"/>
        <location filename="../src/interface/onglets/donnees/informationssatellite.ui" line="702"/>
        <source>rev/jour</source>
        <extracomment>revolution per day</extracomment>
        <translation>rev/day</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/informationssatellite.ui" line="650"/>
        <source>n&quot; / 6 :</source>
        <extracomment>second derivative of the mean motion divided by six (in revolution per day cube)</extracomment>
        <translation>n&quot; / 6 :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/informationssatellite.ui" line="663"/>
        <source>Moyen mouvement :</source>
        <translation>Mean motion :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/informationssatellite.ui" line="689"/>
        <source>Nb orbites à l&apos;époque :</source>
        <translation>Orbit # at epoch :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/informationssatellite.ui" line="715"/>
        <source>n&apos; / 2 :</source>
        <extracomment>derivative of the mean motion divided by two (in revolution per day square)</extracomment>
        <translation>n&apos; / 2 :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/informationssatellite.ui" line="795"/>
        <source>Anomalie moyenne :</source>
        <translation>Mean anomaly :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/informationssatellite.ui" line="824"/>
        <source>Magnitude std/max :</source>
        <extracomment>Standard/maximum magnitude</extracomment>
        <translation>Std/Max magnitude :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/informationssatellite.ui" line="850"/>
        <source>Modèle orbital :</source>
        <translation>Propagation model :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/informationssatellite.ui" line="876"/>
        <source>Dimensions/Section :</source>
        <translation>Dimensions/Section :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/informationssatellite.cpp" line="144"/>
        <location filename="../src/interface/onglets/donnees/informationssatellite.cpp" line="147"/>
        <source>Inconnue</source>
        <translation>Unknown</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/informationssatellite.cpp" line="150"/>
        <location filename="../src/interface/onglets/donnees/informationssatellite.cpp" line="153"/>
        <source>Inconnu</source>
        <translation>Unknown</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/informationssatellite.cpp" line="192"/>
        <source>Modèle haute orbite</source>
        <translation>High orbit model</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/informationssatellite.cpp" line="192"/>
        <source>Modèle basse orbite</source>
        <translation>Low orbit model</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/informationssatellite.cpp" line="199"/>
        <source>m</source>
        <comment>meter</comment>
        <translation>m</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/informationssatellite.cpp" line="199"/>
        <source>ft</source>
        <comment>foot</comment>
        <translation>ft</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/informationssatellite.cpp" line="209"/>
        <source>Sphérique. R=%1 %2</source>
        <comment>R = radius</comment>
        <translation>Spherical. R=%1 %2</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/informationssatellite.cpp" line="213"/>
        <source>Cylindrique. L=%1 %2, R=%3 %2</source>
        <comment>L = height; R = radius</comment>
        <translation>Cylindrical. L=%1 %2, R=%3 %2</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/informationssatellite.cpp" line="217"/>
        <source>Boîte. %1 x %2 x %3 %4</source>
        <translation>Box. %1 x %2 x %3 %4</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/informationssatellite.cpp" line="221"/>
        <location filename="../src/interface/onglets/donnees/informationssatellite.cpp" line="298"/>
        <source>Inconnues</source>
        <translation>Unknown</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/informationssatellite.cpp" line="255"/>
        <source>Problème de droits d&apos;écriture du fichier %1</source>
        <translation>Unable to write file %1</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/informationssatellite.cpp" line="267"/>
        <source>Nom du satellite :</source>
        <translation>Name of satellite :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/informationssatellite.cpp" line="269"/>
        <source>Numéro NORAD            : %1 		Moyen mouvement       : %2 rev/jour	 Date de lancement  : %3</source>
        <comment>revolution per day</comment>
        <translation>NORAD number           : %1 			Mean motion        : %2 rev/day	Launch date          : %3</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/informationssatellite.cpp" line="273"/>
        <source>Désignation COSPAR      : %1		n&apos;/2                  : %2 rev/jour^2	 Catégorie d&apos;orbite : %3</source>
        <comment>n&apos;/2 = derivative of the mean motion divided by two (in revolution per day square)</comment>
        <translation>COSPAR designation     : %1			n&apos;/2               : %2 rev/day^2	Orbital category     : %3</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/informationssatellite.cpp" line="285"/>
        <source>Inclinaison             : %1		Anomalie moyenne      : %2</source>
        <translation>Inclination            : %1			Mean anomaly       : %2</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/informationssatellite.cpp" line="289"/>
        <source>AD noeud ascendant      : %1		Magnitude std/max     : %2</source>
        <comment>Right ascension of the ascending node, Standard/Maximum magnitude</comment>
        <translation>RA of ascending node   : %1			Std/max magnitude  : %2</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/informationssatellite.cpp" line="293"/>
        <source>Excentricité            : %1		Modèle orbital        : %2</source>
        <translation>Eccentricity           : %1			Propagation model  : %2</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/informationssatellite.cpp" line="296"/>
        <source>Argument du périgée     : %1		Dimensions/Section    : %2%3</source>
        <translation>Argument of perigee    : %1			Dimensions/Section : %2%3</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/informationssatellite.cpp" line="277"/>
        <source>Époque (UTC)            : %1	n&quot;/6                  : %2 rev/jour^3	 Pays/Organisation  : %3</source>
        <comment>n&quot;/6 = second derivative of the mean motion divided by six (in revolution per day cube)</comment>
        <translation>Epoch (UTC)            : %1		n&quot;/6               : %2 rev/day^3	Country/Organization : %3</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/informationssatellite.cpp" line="281"/>
        <source>Coeff pseudo-balistique : %1 (1/Re)	Nb orbites à l&apos;époque : %2			 Site de lancement  : %3</source>
        <comment>Pseudo-ballistic coefficient in 1/Earth radius</comment>
        <translation>Pseudo-ballistic coeff : %1 (1/Re)		Orbit # at epoch   : %2			Launch site          : %3</translation>
    </message>
</context>
<context>
    <name>Logging</name>
    <message>
        <location filename="../src/interface/logging/logging.ui" line="29"/>
        <source>Gestion des fichiers de log</source>
        <translation>Management of log files</translation>
    </message>
    <message>
        <location filename="../src/interface/logging/logging.ui" line="86"/>
        <source>Nom du fichier</source>
        <translation>Name of file</translation>
    </message>
    <message>
        <location filename="../src/interface/logging/logging.ui" line="91"/>
        <source>Niveau</source>
        <extracomment>Error level</extracomment>
        <translation>Level</translation>
    </message>
    <message>
        <location filename="../src/interface/logging/logging.ui" line="96"/>
        <source>Nombre de messages</source>
        <translation>Number of messages</translation>
    </message>
    <message>
        <location filename="../src/interface/logging/logging.cpp" line="157"/>
        <source>FATAL</source>
        <translation>FATAL</translation>
    </message>
    <message>
        <location filename="../src/interface/logging/logging.cpp" line="163"/>
        <source>ERREUR</source>
        <translation>ERROR</translation>
    </message>
    <message>
        <location filename="../src/interface/logging/logging.cpp" line="169"/>
        <source>WARNING</source>
        <translation>WARNING</translation>
    </message>
    <message>
        <location filename="../src/interface/logging/logging.cpp" line="175"/>
        <source>DEBUG</source>
        <translation>DEBUG</translation>
    </message>
    <message>
        <location filename="../src/interface/logging/logging.cpp" line="180"/>
        <source>INFO</source>
        <translation>INFO</translation>
    </message>
    <message>
        <location filename="../src/interface/logging/logging.cpp" line="215"/>
        <source>Exporter ...</source>
        <translation>Export ...</translation>
    </message>
    <message>
        <location filename="../src/interface/logging/logging.cpp" line="275"/>
        <source>Enregistrer sous...</source>
        <translation>Save as...</translation>
    </message>
    <message>
        <location filename="../src/interface/logging/logging.cpp" line="275"/>
        <source>Fichiers log (*.log)</source>
        <translation>Log files (*.log)</translation>
    </message>
</context>
<context>
    <name>Onglets</name>
    <message>
        <location filename="../src/interface/onglets/onglets.ui" line="48"/>
        <source>Général</source>
        <translation>Main</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/onglets.ui" line="267"/>
        <source>Antenne</source>
        <translation>Antenna</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/onglets.ui" line="56"/>
        <location filename="../src/interface/onglets/onglets.cpp" line="278"/>
        <source>Éléments osculateurs</source>
        <translation>Osculating elements</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/onglets.ui" line="64"/>
        <location filename="../src/interface/onglets/onglets.cpp" line="395"/>
        <location filename="../src/interface/onglets/onglets.cpp" line="416"/>
        <location filename="../src/interface/onglets/onglets.cpp" line="432"/>
        <source>Informations satellite</source>
        <translation>Satellite information</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/onglets.ui" line="157"/>
        <location filename="../src/interface/onglets/onglets.cpp" line="70"/>
        <source>Prévisions</source>
        <translation>Predictions</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/onglets.cpp" line="71"/>
        <source>Flashs</source>
        <translation>Flares</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/onglets.cpp" line="72"/>
        <source>Transits</source>
        <translation>Transits</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/onglets.cpp" line="74"/>
        <source>Starlink</source>
        <translation>Starlink</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/onglets.cpp" line="397"/>
        <location filename="../src/interface/onglets/onglets.cpp" line="410"/>
        <location filename="../src/interface/onglets/onglets.cpp" line="425"/>
        <source>Informations ISS</source>
        <translation>ISS information</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/onglets.ui" line="259"/>
        <source>Télescope</source>
        <translation>Telescope</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/onglets.cpp" line="73"/>
        <source>Évènements orbitaux</source>
        <translation>Orbital events</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/onglets.cpp" line="399"/>
        <location filename="../src/interface/onglets/onglets.cpp" line="404"/>
        <location filename="../src/interface/onglets/onglets.cpp" line="409"/>
        <location filename="../src/interface/onglets/onglets.cpp" line="426"/>
        <source>Recherche données</source>
        <translation>Data search</translation>
    </message>
</context>
<context>
    <name>Options</name>
    <message>
        <location filename="../src/interface/options/options.ui" line="26"/>
        <source>Options</source>
        <translation>Options</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="58"/>
        <location filename="../src/interface/options/options.cpp" line="1138"/>
        <source>Lieu d&apos;observation</source>
        <translation>Name of location</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="61"/>
        <source>Gestion et sélection des lieux d&apos;observation</source>
        <translation>Management and selection of places of location</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="70"/>
        <source>Configuration</source>
        <translation>Settings</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="73"/>
        <source>Configuration de l&apos;interface graphique</source>
        <translation>Graphical interface settings</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="82"/>
        <source>Wall Command Center</source>
        <translation>Wall Command Center</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="85"/>
        <source>Paramètres d&apos;affichage du Wall Command Center</source>
        <translation>Wall Command Center display settings</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="162"/>
        <source>Lieux d&apos;observation :</source>
        <translation>Locations :</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="191"/>
        <source>Lieu :</source>
        <translation>Location :</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="222"/>
        <location filename="../src/interface/options/options.ui" line="456"/>
        <source>Longitude :</source>
        <translation>Longitude :</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="232"/>
        <location filename="../src/interface/options/options.ui" line="504"/>
        <source>Latitude :</source>
        <translation>Latitude :</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="242"/>
        <location filename="../src/interface/options/options.ui" line="574"/>
        <source>Altitude :</source>
        <comment>Altitude of observer</comment>
        <translation>Altitude :</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="293"/>
        <location filename="../src/interface/options/options.ui" line="390"/>
        <source>Valider</source>
        <translation>Save</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="309"/>
        <location filename="../src/interface/options/options.ui" line="374"/>
        <location filename="../src/interface/options/options.cpp" line="981"/>
        <location filename="../src/interface/options/options.cpp" line="1143"/>
        <source>Annuler</source>
        <translation>Cancel</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="337"/>
        <location filename="../src/interface/options/options.ui" line="418"/>
        <source>Nom :</source>
        <translation>Name :</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="515"/>
        <location filename="../src/interface/options/options.cpp" line="419"/>
        <source>Nord</source>
        <translation>North</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="520"/>
        <location filename="../src/interface/options/options.cpp" line="419"/>
        <location filename="../src/interface/options/options.cpp" line="1529"/>
        <source>Sud</source>
        <translation>South</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="532"/>
        <location filename="../src/interface/options/options.cpp" line="418"/>
        <location filename="../src/interface/options/options.cpp" line="1527"/>
        <source>Est</source>
        <translation>East</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="537"/>
        <location filename="../src/interface/options/options.cpp" line="418"/>
        <source>Ouest</source>
        <translation>West</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="617"/>
        <source>m</source>
        <extracomment>Meter</extracomment>
        <translation>m</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="649"/>
        <source>Ajouter dans :</source>
        <translation>Add in :</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="680"/>
        <source>Lieux sélectionnés :</source>
        <translation>Selected locations :</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="702"/>
        <location filename="../src/interface/options/options.ui" line="743"/>
        <source>Cliquer droit pour afficher le menu contextuel</source>
        <translation>Right click to show the context menu</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="721"/>
        <source>Sélection de la catégorie :</source>
        <translation>Category selection :</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="840"/>
        <location filename="../src/interface/options/options.ui" line="862"/>
        <source>Filtre</source>
        <translation>Filter</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="893"/>
        <source>Satellites</source>
        <translation>Satellites</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="926"/>
        <source>Notification sonore</source>
        <translation>Sound notification</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="958"/>
        <source>Trace dans le ciel</source>
        <translation>Sky track</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="971"/>
        <source>Affichage du numéro NORAD dans les listes</source>
        <translation>Display of NORAD number in lists</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="1008"/>
        <source>Trace au sol</source>
        <translation>Ground track</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="1055"/>
        <source>Rotation de l&apos;icône ISS</source>
        <translation>Rotate ISS icon</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="1071"/>
        <source>Zone de visibilité</source>
        <translation>Satellite footprint</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="1090"/>
        <source>Icône des satellites</source>
        <translation>Satellite icons</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="1106"/>
        <source>Nom des satellites</source>
        <translation>Name of satellites</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="1118"/>
        <source>Système solaire / étoiles</source>
        <translation>Solar system /  stars</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="1133"/>
        <source>Nom des étoiles</source>
        <translation>Name of stars</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="1167"/>
        <source>Zone d&apos;ombre</source>
        <translation>Night shadow</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="1214"/>
        <source>Affichage des constellations</source>
        <translation>Display of constellations</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="1233"/>
        <source>Rotation de la Lune pour l&apos;hémisphère Sud</source>
        <translation>Moon rotation for southern hemisphere</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="1249"/>
        <source>Soleil</source>
        <translation>Sun</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="1268"/>
        <source>Affichage des planètes</source>
        <translation>Display of planets</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="1302"/>
        <source>Magnitude limite des étoiles :</source>
        <translation>Limiting magnitude of stars :</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="1346"/>
        <source>Phase de la Lune</source>
        <translation>Moon phase</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="1362"/>
        <source>Lune</source>
        <translation>Moon</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="1371"/>
        <location filename="../src/interface/options/options.ui" line="2138"/>
        <source>Affichage</source>
        <translation>Display</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="1386"/>
        <source>Prise en compte des éclipses produites par la Lune</source>
        <translation>Set eclipses produced by the Moon</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="1402"/>
        <source>Prise en compte de la réfraction atmosphérique</source>
        <translation>Set the atmospheric refraction</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="1418"/>
        <source>Inversion Nord/Sud sur le radar</source>
        <translation>North/South inversion on radar</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="1434"/>
        <source>Nom des lieux d&apos;observation</source>
        <translation>Name of locations</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="1453"/>
        <source>Prise en compte des éclipses partielles sur la magnitude</source>
        <translation>Set the effect of partial eclipses on the magnitude</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="1469"/>
        <source>Affichage du jour julien</source>
        <translation>Display of julian date</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="1500"/>
        <source>Carte du monde :</source>
        <translation>World map :</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="1532"/>
        <source>Grille</source>
        <translation>Grid</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="1548"/>
        <source>Affichage de la SAA</source>
        <translation>Display of SAA</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="1564"/>
        <source>Radar</source>
        <translation>Radar</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="1583"/>
        <source>Prise en compte de l&apos;extinction atmosphérique</source>
        <translation>Set the atmospheric extinction</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="1599"/>
        <source>Affichage des frontières</source>
        <translation>Display of boundaries</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="1615"/>
        <source>Inversion Est/Ouest sur le radar</source>
        <translation>East/West inversion on radar</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="1631"/>
        <source>Affichage des coordonnées</source>
        <translation>Display of coordinates</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="1659"/>
        <source>Valeur du zoom pour les cartes du monde dans le navigateur :</source>
        <translation>Zoom value for maps :</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="1694"/>
        <source>Mode sombre</source>
        <translation>Dark mode</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="1703"/>
        <source>Système</source>
        <translation>System</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="1718"/>
        <source>Vérification des mises à jour au démarrage</source>
        <translation>Check for updates at startup</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="1734"/>
        <source>Écart Heure locale - UTC</source>
        <translation>Local hour - UTC offset</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="1749"/>
        <source>Temps Universel Coordonné (UTC)</source>
        <translation>Universal Time Coordinated (UTC)</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="1777"/>
        <source>Heure locale = </source>
        <translation>Local hour = </translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="1825"/>
        <source>Auto</source>
        <extracomment>Automatic</extracomment>
        <translation>Auto</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="1848"/>
        <source>Unités</source>
        <translation>Units</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="1863"/>
        <source>Système métrique</source>
        <translation>Metric system</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="1882"/>
        <source>Système anglo-saxon</source>
        <translation>US customary units</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="1902"/>
        <source>Système horaire</source>
        <translation>Time convention</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="1917"/>
        <source>24 heures</source>
        <translation>24 hours</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="1936"/>
        <source>12 heures (AM/PM)</source>
        <translation>12 hours (AM/PM)</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="1968"/>
        <source>Nombre de fichiers log :</source>
        <translation>Number of log files :</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="2014"/>
        <source>Langue :</source>
        <extracomment>Name of language</extracomment>
        <translation>Language :</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="2057"/>
        <source>Préférences d&apos;affichage :</source>
        <translation>Display preferences :</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="2094"/>
        <source>Enregistrer les préférences d&apos;affichage</source>
        <translation>Save display preferences</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="2119"/>
        <source>Style &quot;Wall Command Center&quot;</source>
        <translation>&quot;Wall Command Center&quot; style</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="2150"/>
        <source>Nombre d&apos;orbites</source>
        <translation>Orbit number</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="2166"/>
        <source>Anomalie Atlantique Sud et Zone d&apos;exclusion</source>
        <translation>South Atlantic anomaly and Zone of exclusion</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="2169"/>
        <source>SAA et ZOE</source>
        <extracomment>South Atlantic Anomaly and Zone of Exclusion</extracomment>
        <translation>SAA and ZOE</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="2191"/>
        <source>Cercles d&apos;acquisition</source>
        <translation>Acquisition circles</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="2210"/>
        <source>Angle entre le plan de l&apos;orbite et la direction du Soleil</source>
        <translation>Angle between orbit plane and direction of Sun</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="2213"/>
        <source>Angle beta</source>
        <extracomment>Beta angle (angle between orbit plane and direction of Sun)</extracomment>
        <translation>Beta angle</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="2239"/>
        <source>Choix de la police :</source>
        <translation>Set the font :</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="2278"/>
        <source>Couleurs</source>
        <translation>Colors</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="2302"/>
        <source>Greenwich Mean Time</source>
        <translation>Greenwich Mean Time</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="2305"/>
        <source>GMT :</source>
        <extracomment>Greenwich Mean Time</extracomment>
        <translation>GMT :</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="2312"/>
        <source>Zone d&apos;exclusion</source>
        <translation>Zone of exclusion</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="2315"/>
        <source>ZOE :</source>
        <extracomment>Zone of exclusion</extracomment>
        <translation>ZOE :</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="2322"/>
        <source>Équateur :</source>
        <translation>Equator :</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="2329"/>
        <source>Terminateur :</source>
        <translation>Terminator :</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="2336"/>
        <source>Cercle de visibilité :</source>
        <translation>Footprint :</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="2344"/>
        <location filename="../src/interface/options/options.ui" line="2377"/>
        <location filename="../src/interface/options/options.ui" line="2410"/>
        <source>Rouge</source>
        <translation>Red</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="2349"/>
        <location filename="../src/interface/options/options.ui" line="2368"/>
        <location filename="../src/interface/options/options.ui" line="2382"/>
        <location filename="../src/interface/options/options.ui" line="2405"/>
        <source>Blanc</source>
        <translation>White</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="2354"/>
        <source>Cyan</source>
        <translation>Cyan</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="2363"/>
        <source>Noir</source>
        <translation>Black</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="2391"/>
        <source>Brun</source>
        <translation>Brown</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="2396"/>
        <source>Jaune</source>
        <translation>Yellow</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.ui" line="2428"/>
        <source>Stations :</source>
        <translation>Stations :</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.cpp" line="170"/>
        <location filename="../src/interface/options/options.cpp" line="544"/>
        <source>Créer une catégorie</source>
        <translation>Create a category</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.cpp" line="172"/>
        <location filename="../src/interface/options/options.cpp" line="559"/>
        <source>Créer un nouveau lieu</source>
        <translation>Create a new location</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.cpp" line="182"/>
        <location filename="../src/interface/options/options.cpp" line="426"/>
        <location filename="../src/interface/options/options.cpp" line="1080"/>
        <location filename="../src/interface/options/options.cpp" line="1216"/>
        <source>m</source>
        <comment>meter</comment>
        <translation>m</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.cpp" line="182"/>
        <location filename="../src/interface/options/options.cpp" line="426"/>
        <location filename="../src/interface/options/options.cpp" line="1080"/>
        <location filename="../src/interface/options/options.cpp" line="1216"/>
        <source>ft</source>
        <comment>foot</comment>
        <translation>ft</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.cpp" line="193"/>
        <source>L&apos;altitude doit être comprise entre %1%2 et %3%2</source>
        <comment>Observer altitude</comment>
        <translation>The altitude must be between %1%2 et %3%2</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.cpp" line="412"/>
        <source>Lieu : %1</source>
        <translation>Location : %1</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.cpp" line="548"/>
        <location filename="../src/interface/options/options.cpp" line="567"/>
        <source>Renommer</source>
        <translation>Rename</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.cpp" line="551"/>
        <location filename="../src/interface/options/options.cpp" line="574"/>
        <source>Supprimer</source>
        <translation>Remove</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.cpp" line="555"/>
        <location filename="../src/interface/options/options.cpp" line="675"/>
        <location filename="../src/interface/options/options.cpp" line="782"/>
        <source>Télécharger...</source>
        <translation>Download...</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.cpp" line="563"/>
        <source>Ajouter à Mes Préférés</source>
        <translation>Add in My Favorites</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.cpp" line="570"/>
        <source>Modifier</source>
        <translation>Modify</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.cpp" line="637"/>
        <source>Langue</source>
        <comment>Translate by the name of language, for example : English, Français, Español</comment>
        <translation>English</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.cpp" line="664"/>
        <location filename="../src/interface/options/options.cpp" line="745"/>
        <location filename="../src/interface/options/options.cpp" line="776"/>
        <source>* Défaut</source>
        <translation>* Default</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.cpp" line="705"/>
        <source>Mes Préférés</source>
        <translation>My Favorites</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.cpp" line="751"/>
        <source>Enregistrer sous...</source>
        <translation>Save as...</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.cpp" line="752"/>
        <source>Supprimer...</source>
        <translation>Remove...</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.cpp" line="879"/>
        <source>Problème de droits d&apos;écriture du fichier %1</source>
        <translation>Unable to write file %1</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.cpp" line="976"/>
        <source>Catégorie</source>
        <translation>Category</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.cpp" line="977"/>
        <source>Nouveau nom de la catégorie :</source>
        <translation>New name of the category :</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.cpp" line="980"/>
        <location filename="../src/interface/options/options.cpp" line="1142"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.cpp" line="996"/>
        <location filename="../src/interface/options/options.cpp" line="1028"/>
        <source>Information</source>
        <translation>Information</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.cpp" line="996"/>
        <source>La catégorie existe déjà. Voulez-vous l&apos;écraser ?</source>
        <translation>The category already exists. Do you want to overwrite it?</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.cpp" line="997"/>
        <location filename="../src/interface/options/options.cpp" line="1029"/>
        <location filename="../src/interface/options/options.cpp" line="1240"/>
        <source>Oui</source>
        <translation>Yes</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.cpp" line="998"/>
        <location filename="../src/interface/options/options.cpp" line="1030"/>
        <location filename="../src/interface/options/options.cpp" line="1241"/>
        <source>Non</source>
        <translation>No</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.cpp" line="1028"/>
        <source>Voulez-vous vraiment supprimer la catégorie &lt;b&gt;%1&lt;/b&gt; ?</source>
        <translation>Do you really want to remove the &lt;b&gt;%1&lt;/b&gt; category?</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.cpp" line="1110"/>
        <source>Le lieu d&apos;observation &lt;b&gt;%1&lt;/b&gt; fait déjà partie de &lt;b&gt;Mes Préférés&lt;/b&gt;</source>
        <translation>The location &lt;b&gt;%1&lt;/b&gt; is already in &lt;b&gt;My Favorites&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.cpp" line="1139"/>
        <source>Nouveau nom du lieu d&apos;observation :</source>
        <translation>New name of the location :</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.cpp" line="1237"/>
        <source>Voulez-vous vraiment supprimer &lt;b&gt;%1&lt;/b&gt; de la catégorie &lt;b&gt;%2&lt;/b&gt; ?</source>
        <translation>Do you really want to remove &lt;b&gt;%1&lt;/b&gt; from the category &lt;b&gt;%2&lt;/b&gt;?</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.cpp" line="1239"/>
        <source>Avertissement</source>
        <translation>Warning</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.cpp" line="1366"/>
        <source>Le nom de la catégorie n&apos;est pas spécifié</source>
        <translation>The name of the category is not given</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.cpp" line="1386"/>
        <source>La catégorie spécifiée existe déjà</source>
        <translation>The category already exists</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.cpp" line="1497"/>
        <source>Le nom du lieu d&apos;observation n&apos;est pas spécifié</source>
        <translation>The name of location is not given</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.cpp" line="1506"/>
        <source>Le lieu existe déjà dans la catégorie &lt;b&gt;%1&lt;/b&gt;</source>
        <translation>The location already exists in the category &lt;b&gt;%1&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.cpp" line="1567"/>
        <source>Lieu d&apos;observation déjà sélectionné</source>
        <translation>Location already selected</translation>
    </message>
    <message>
        <location filename="../src/interface/options/options.cpp" line="1672"/>
        <source>UTC</source>
        <comment>Universal Time Coordinated</comment>
        <translation>UTC</translation>
    </message>
</context>
<context>
    <name>Osculateurs</name>
    <message>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.ui" line="63"/>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.ui" line="115"/>
        <source>Date :</source>
        <extracomment>Date and hour</extracomment>
        <translation>Date :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.ui" line="128"/>
        <source>dddd dd MMMM yyyy HH:mm:ss</source>
        <translation>dddd, MMMM dd yyyy  HH:mm:ss</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.ui" line="189"/>
        <source>Nom :</source>
        <translation>Name :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.ui" line="296"/>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.cpp" line="218"/>
        <source>Vecteur d&apos;état</source>
        <translation>State vector</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.ui" line="326"/>
        <source>x :</source>
        <extracomment>Component X of the position vector</extracomment>
        <translation>x :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.ui" line="336"/>
        <source>y :</source>
        <extracomment>Component Y of the position vector</extracomment>
        <translation>y :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.ui" line="346"/>
        <source>z :</source>
        <extracomment>Component Z of the position vector</extracomment>
        <translation>z :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.ui" line="419"/>
        <source>vx :</source>
        <extracomment>Component X of the velocity vector</extracomment>
        <translation>vx :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.ui" line="429"/>
        <source>vy :</source>
        <extracomment>Component Y of the velocity vector</extracomment>
        <translation>vy :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.ui" line="439"/>
        <source>vz :</source>
        <extracomment>Component Z of the velocity vector</extracomment>
        <translation>vz :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.ui" line="449"/>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.ui" line="465"/>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.ui" line="481"/>
        <source>Double-cliquer pour changer d&apos;unités</source>
        <translation>Double clic to change units</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.ui" line="517"/>
        <source>ECI</source>
        <extracomment>Earth Centered Inertial</extracomment>
        <translation>ECI</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.ui" line="522"/>
        <source>ECEF</source>
        <extracomment>Earth Centered Earth Fixed</extracomment>
        <translation>ECEF</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.ui" line="546"/>
        <source>Paramètres képlériens</source>
        <translation>Keplerian parameters</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.ui" line="551"/>
        <source>Paramètres circulaires</source>
        <translation>Circular parameters</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.ui" line="556"/>
        <source>Paramètres équatoriaux</source>
        <translation>Equatorial parameters</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.ui" line="561"/>
        <source>Paramètres circulaires équatoriaux</source>
        <translation>Circular equatorial parameters</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.ui" line="575"/>
        <source>Éléments osculateurs</source>
        <translation>Osculating elements</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.ui" line="605"/>
        <source>Anomalie vraie :</source>
        <translation>True anomaly :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.ui" line="615"/>
        <source>Anomalie excentrique :</source>
        <translation>Eccentric anomaly :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.ui" line="625"/>
        <source>Champ de vue :</source>
        <translation>Field of view :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.ui" line="698"/>
        <source>Apogée (Altitude) :</source>
        <translation>Apogee (Altitude) :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.ui" line="708"/>
        <source>Périgée (Altitude) :</source>
        <translation>Perigee (Altitude) :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.ui" line="718"/>
        <source>Période orbitale :</source>
        <translation>Orbital period :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.ui" line="804"/>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.ui" line="1144"/>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.ui" line="1226"/>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.ui" line="1411"/>
        <source>Demi-grand axe :</source>
        <translation>Semi-major axis :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.ui" line="814"/>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.ui" line="1180"/>
        <source>Excentricité :</source>
        <translation>Eccentricity :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.ui" line="824"/>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.ui" line="1002"/>
        <source>Inclinaison :</source>
        <translation>Inclination :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.ui" line="897"/>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.ui" line="992"/>
        <source>AD noeud ascendant :</source>
        <extracomment>Right ascension of the ascending node</extracomment>
        <translation>RA of ascending node :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.ui" line="907"/>
        <source>Argument du périgée :</source>
        <translation>Argument of perigee :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.ui" line="943"/>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.ui" line="1293"/>
        <source>Anomalie moyenne :</source>
        <translation>Mean anomaly :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.ui" line="1203"/>
        <source>Longitude du périgée :</source>
        <translation>Longitude of perigee :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.ui" line="1213"/>
        <source>Somme de l&apos;argument du périgée et de l&apos;ascension droite du noeud ascendant</source>
        <translation>Sum of argument of perigee and right ascension of the ascending node</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.ui" line="1273"/>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.ui" line="1458"/>
        <source>Ix :</source>
        <extracomment>Component X of inclination vector</extracomment>
        <translation>Ix :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.ui" line="1283"/>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.ui" line="1468"/>
        <source>Iy :</source>
        <extracomment>Component Y of inclination vector</extracomment>
        <translation>Iy :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.ui" line="1088"/>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.ui" line="1368"/>
        <source>Ex :</source>
        <extracomment>Component X of eccentricity vector</extracomment>
        <translation>Ex :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.ui" line="1111"/>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.ui" line="1378"/>
        <source>Ey :</source>
        <extracomment>Component Y of eccentricity vector</extracomment>
        <translation>Ey :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.ui" line="1012"/>
        <source>Position sur orbite :</source>
        <translation>In orbit position :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.ui" line="1048"/>
        <source>Somme de l&apos;argument du périgée et de l&apos;anomalie moyenne</source>
        <translation>Sum of argument of perigee and mean anomaly</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.ui" line="1478"/>
        <source>Argument longitude vraie :</source>
        <translation>True longitude argument :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.ui" line="1514"/>
        <source>Somme de l&apos;argument du périgée, de l&apos;ascension droite du noeud ascendant et de l&apos;anomalie vraie</source>
        <translation>Sum of argument of perigee, right ascension of the ascending node and true anomaly</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.ui" line="1539"/>
        <source>Divers</source>
        <translation>Miscellaneous</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.ui" line="1566"/>
        <source>Doppler@100MHz :</source>
        <extracomment>Doppler effect at 100 Mega Hertz</extracomment>
        <translation>Doppler @ 100MHz :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.ui" line="1573"/>
        <source>Atténuation :</source>
        <translation>Free-space loss :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.ui" line="1580"/>
        <source>Délai :</source>
        <extracomment>Delay of signal at light speed</extracomment>
        <translation>Delay :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.ui" line="1629"/>
        <source>Phasage :</source>
        <translation>Phasing :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.cpp" line="133"/>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.cpp" line="351"/>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.cpp" line="430"/>
        <source>km</source>
        <comment>Kilometer</comment>
        <translation>km</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.cpp" line="133"/>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.cpp" line="351"/>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.cpp" line="430"/>
        <source>nmi</source>
        <comment>nautical mile</comment>
        <translation>nmi</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.cpp" line="176"/>
        <source>N/A</source>
        <comment>Not applicable</comment>
        <translation>N/A</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.cpp" line="203"/>
        <source>Problème de droits d&apos;écriture du fichier %1</source>
        <translation>Unable to write file %1</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.cpp" line="213"/>
        <source>Date :</source>
        <comment>Date and hour</comment>
        <translation>Date :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.cpp" line="216"/>
        <source>Nom du satellite :</source>
        <translation>Name of satellite :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.cpp" line="219"/>
        <source>x : %1			vx : %2</source>
        <comment>Position, velocity</comment>
        <translation>x : %1			vx : %2</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.cpp" line="222"/>
        <source>y : %1			vy : %2</source>
        <comment>Position, velocity</comment>
        <translation>y : %1			vy : %2</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.cpp" line="225"/>
        <source>z : %1			vz : %2</source>
        <comment>Position, velocity</comment>
        <translation>z : %1			vz : %2</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.cpp" line="228"/>
        <source>Éléments osculateurs :</source>
        <translation>Osculating elements :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.cpp" line="233"/>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.cpp" line="246"/>
        <source>Demi-grand axe       : %1	Ascension droite du noeud ascendant : %2</source>
        <translation>Semi-major axis   : %1 		Right ascension of the ascending node : %2</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.cpp" line="236"/>
        <source>Excentricité         : %1	Argument du périgée                 : %2</source>
        <translation>Eccentricity      : %1 		Argument of perigee                   : %2</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.cpp" line="239"/>
        <source>Inclinaison          : %1	Anomalie moyenne                    : %2</source>
        <translation>Inclination       : %1		Mean anomaly                          : %2</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.cpp" line="249"/>
        <source>Ex                   : %1	Inclinaison                         : %2</source>
        <comment>Ex = Component X of eccentricity vector</comment>
        <translation>Ex                : %1 		Inclination                           : %2</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.cpp" line="252"/>
        <source>Ey                   : %1	Position sur orbite                 : %2</source>
        <comment>Ey = Component Y of eccentricity vector</comment>
        <translation>Ey                : %1 		In orbit position                     : %2</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.cpp" line="259"/>
        <source>Demi-grand axe       : %1	Ix                 : %2</source>
        <comment>Ix = Component X of inclination vector</comment>
        <translation>Semi-major axis      : %1 	Ix                 : %2</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.cpp" line="262"/>
        <source>Excentricité         : %1	Iy                 : %2</source>
        <comment>Iy = Component Y of inclination vector</comment>
        <translation>Eccentricity         : %1 	Iy                 : %2</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.cpp" line="265"/>
        <source>Longitude du périgée : %1	Anomalie moyenne   : %2</source>
        <translation>Longitude of perigee : %1	Mean anomaly       : %2</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.cpp" line="272"/>
        <source>Demi-grand axe       : %1	Ix                          : %2</source>
        <comment>Ix = Component X of inclination vector</comment>
        <translation>Semi-major axis   : %1   	Ix                       : %2</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.cpp" line="275"/>
        <source>Ex                   : %1	Iy                          : %2</source>
        <comment>Ex = Component X of eccentricity vector, Iy = Component Y of inclination vector</comment>
        <translation>Ex                : %1  	Iy                       : %2</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.cpp" line="279"/>
        <source>Ey                   : %1	Argument de longitude vraie : %2</source>
        <comment>Ey = Component Y of eccentricity vector</comment>
        <translation>Ey                : %1  	True longtitude argument : %2</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.cpp" line="288"/>
        <source>Anomalie vraie       : %1	Apogée  (Altitude) : %2</source>
        <translation>True anomaly      : %1		Apogee  (Altitude) : %2</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.cpp" line="291"/>
        <source>Anomalie excentrique : %1	Périgée (Altitude) : %2</source>
        <translation>Eccentric anomaly : %1		Perigee (Altitude) : %2</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.cpp" line="294"/>
        <source>Champ de vue         : %1  	Période orbitale   : %2</source>
        <translation>Field of view     : %1   		Orbital period     : %2</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.cpp" line="298"/>
        <source>Divers :</source>
        <translation>Miscellaneous :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.cpp" line="299"/>
        <source>Doppler @ 100 MHz    : %1</source>
        <comment>Doppler effect at 100 MegaHertz</comment>
        <translation>Doppler @ 100 MHz : %1</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.cpp" line="302"/>
        <source>Atténuation          : %1</source>
        <translation>Free-space loss   : %1</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.cpp" line="305"/>
        <source>Délai                : %1</source>
        <comment>Delay of signal at light speed</comment>
        <translation>Delay             : %1</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/osculateurs/osculateurs.cpp" line="308"/>
        <source>Phasage              : %1</source>
        <translation>Phasing           : %1</translation>
    </message>
</context>
<context>
    <name>Outils</name>
    <message>
        <location filename="../src/interface/outils/outils.ui" line="26"/>
        <source>Outils</source>
        <translation>Tools</translation>
    </message>
    <message>
        <location filename="../src/interface/outils/outils.ui" line="58"/>
        <source>Gestionnaire éléments orbitaux</source>
        <translation>Management of orbital elements</translation>
    </message>
    <message>
        <location filename="../src/interface/outils/outils.ui" line="63"/>
        <source>Mise à jour TLE</source>
        <translation>TLE update</translation>
    </message>
    <message>
        <location filename="../src/interface/outils/outils.ui" line="68"/>
        <source>Gestionnaire TLE</source>
        <translation>TLE management</translation>
    </message>
    <message>
        <location filename="../src/interface/outils/outils.ui" line="107"/>
        <source>Age maximal des éléments orbitaux (en jours) :</source>
        <translation>Orbital elements expiry date (in days) :</translation>
    </message>
    <message>
        <location filename="../src/interface/outils/outils.ui" line="164"/>
        <source>Liste des fichiers d&apos;éléments orbitaux :</source>
        <translation>List of orbital ements files :</translation>
    </message>
    <message>
        <location filename="../src/interface/outils/outils.ui" line="180"/>
        <source>Valider</source>
        <translation>Save</translation>
    </message>
    <message>
        <location filename="../src/interface/outils/outils.ui" line="196"/>
        <source>Annuler</source>
        <translation>Cancel</translation>
    </message>
    <message>
        <location filename="../src/interface/outils/outils.ui" line="234"/>
        <location filename="../src/interface/outils/outils.ui" line="441"/>
        <source>Domaine :</source>
        <translation>Domain :</translation>
    </message>
    <message>
        <location filename="../src/interface/outils/outils.ui" line="275"/>
        <source>Nom du groupe :</source>
        <translation>Name of group :</translation>
    </message>
    <message>
        <location filename="../src/interface/outils/outils.ui" line="316"/>
        <source>Liste des satellites :</source>
        <translation>List of satellites :</translation>
    </message>
    <message>
        <location filename="../src/interface/outils/outils.ui" line="335"/>
        <source>Filtre</source>
        <translation>Filter</translation>
    </message>
    <message>
        <location filename="../src/interface/outils/outils.ui" line="375"/>
        <source>Fichiers d&apos;éléments orbitaux :</source>
        <translation>Orbital elements files :</translation>
    </message>
    <message>
        <location filename="../src/interface/outils/outils.ui" line="391"/>
        <source>Groupe d&apos;éléments orbitaux :</source>
        <translation>Orbital elements groups :</translation>
    </message>
    <message>
        <location filename="../src/interface/outils/outils.ui" line="413"/>
        <source>Cocher un groupe pour activer sa mise à jour automatique</source>
        <translation>Check a group to activate its automatic update</translation>
    </message>
    <message>
        <location filename="../src/interface/outils/outils.ui" line="464"/>
        <source>Mettre à jour le groupe sélectionné</source>
        <translation>Update the selected group</translation>
    </message>
    <message>
        <location filename="../src/interface/outils/outils.ui" line="607"/>
        <source>Mise à jour TLE auto</source>
        <translation>Auto TLE update</translation>
    </message>
    <message>
        <location filename="../src/interface/outils/outils.ui" line="723"/>
        <source>Mettre à jour maintenant</source>
        <translation>Update now</translation>
    </message>
    <message>
        <location filename="../src/interface/outils/outils.ui" line="752"/>
        <source>Affichage des messages informatifs</source>
        <translation>Display of informative messages</translation>
    </message>
    <message>
        <location filename="../src/interface/outils/outils.ui" line="757"/>
        <source>Accepter ajout/suppression de TLE</source>
        <translation>Allow TLE add/remove</translation>
    </message>
    <message>
        <location filename="../src/interface/outils/outils.ui" line="762"/>
        <source>Refuser ajout/suppression de TLE</source>
        <translation>Disallow TLE add/remove</translation>
    </message>
    <message>
        <location filename="../src/interface/outils/outils.ui" line="772"/>
        <source>Mise à jour TLE manuelle</source>
        <translation>Manual TLE update</translation>
    </message>
    <message>
        <location filename="../src/interface/outils/outils.ui" line="802"/>
        <location filename="../src/interface/outils/outils.ui" line="832"/>
        <source>Parcourir...</source>
        <translation>Browse...</translation>
    </message>
    <message>
        <location filename="../src/interface/outils/outils.ui" line="812"/>
        <source>Fichier à mettre à jour :</source>
        <translation>TLE File to update :</translation>
    </message>
    <message>
        <location filename="../src/interface/outils/outils.ui" line="852"/>
        <source>Fichier TLE à lire :</source>
        <translation>TLE File to read :</translation>
    </message>
    <message>
        <location filename="../src/interface/outils/outils.ui" line="868"/>
        <source>Mettre à jour</source>
        <translation>Update</translation>
    </message>
    <message>
        <location filename="../src/interface/outils/outils.ui" line="936"/>
        <source>Liste de fichiers TLE :</source>
        <translation>List of TLE files :</translation>
    </message>
    <message>
        <location filename="../src/interface/outils/outils.ui" line="949"/>
        <location filename="../src/interface/outils/outils.cpp" line="1071"/>
        <source>Importer TLE...</source>
        <translation>Import TLE...</translation>
    </message>
    <message>
        <location filename="../src/interface/outils/outils.cpp" line="146"/>
        <source>Créer un groupe d&apos;éléments orbitaux</source>
        <translation>Create a group of orbital elements</translation>
    </message>
    <message>
        <location filename="../src/interface/outils/outils.cpp" line="148"/>
        <source>Ajouter des fichiers d&apos;éléments orbitaux</source>
        <translation>Add files of orbital elements</translation>
    </message>
    <message>
        <location filename="../src/interface/outils/outils.cpp" line="163"/>
        <source>Aucun fichier TLE dans le répertoire d&apos;éléments orbitaux</source>
        <translation>None TLE file in the orbital element directory</translation>
    </message>
    <message>
        <location filename="../src/interface/outils/outils.cpp" line="219"/>
        <source>Copier dans le presse-papier</source>
        <translation>Copy to the clipboard</translation>
    </message>
    <message>
        <location filename="../src/interface/outils/outils.cpp" line="222"/>
        <source>Créer un groupe</source>
        <translation>Create a group</translation>
    </message>
    <message>
        <location filename="../src/interface/outils/outils.ui" line="923"/>
        <location filename="../src/interface/outils/outils.cpp" line="226"/>
        <location filename="../src/interface/outils/outils.cpp" line="234"/>
        <source>Supprimer</source>
        <translation>Remove</translation>
    </message>
    <message>
        <location filename="../src/interface/outils/outils.cpp" line="230"/>
        <source>Ajouter des fichiers</source>
        <translation>Add files</translation>
    </message>
    <message>
        <location filename="../src/interface/outils/outils.cpp" line="263"/>
        <source>Fichier %1 :</source>
        <translation>File %1 :</translation>
    </message>
    <message>
        <location filename="../src/interface/outils/outils.cpp" line="268"/>
        <source>TLE du satellite %1 (%2) non réactualisé</source>
        <translation>TLE of satellite %1 (%2) not updated</translation>
    </message>
    <message>
        <location filename="../src/interface/outils/outils.cpp" line="278"/>
        <source>Nombre de TLE(s) supprimés : %1</source>
        <translation>Number of TLE(s) removed : %1</translation>
    </message>
    <message>
        <location filename="../src/interface/outils/outils.cpp" line="283"/>
        <source>Nombre de TLE(s) ajoutés : %1</source>
        <translation>Number of TLE(s) added : %1</translation>
    </message>
    <message>
        <location filename="../src/interface/outils/outils.cpp" line="288"/>
        <source>%1 TLE(s) sur %2 mis à jour</source>
        <translation>%1 TLE(s) on %2 updated</translation>
    </message>
    <message>
        <location filename="../src/interface/outils/outils.cpp" line="293"/>
        <source>Mise à jour de tous les TLE effectuée (fichier de %1 satellite(s))</source>
        <translation>All TLE(s) updated (file of %1 satellite(s))</translation>
    </message>
    <message>
        <location filename="../src/interface/outils/outils.cpp" line="298"/>
        <source>Aucun TLE mis à jour</source>
        <translation>No TLE updated</translation>
    </message>
    <message>
        <location filename="../src/interface/outils/outils.cpp" line="401"/>
        <location filename="../src/interface/outils/outils.cpp" line="431"/>
        <source>Information</source>
        <translation>Information</translation>
    </message>
    <message>
        <location filename="../src/interface/outils/outils.cpp" line="401"/>
        <source>Voulez-vous vraiment supprimer ce fichier du groupe &lt;b&gt;%1&lt;/b&gt; ?</source>
        <translation>Do you really want to remove this file from the &lt;b&gt;%1&lt;/&gt; group?</translation>
    </message>
    <message>
        <location filename="../src/interface/outils/outils.cpp" line="402"/>
        <location filename="../src/interface/outils/outils.cpp" line="432"/>
        <location filename="../src/interface/outils/outils.cpp" line="1140"/>
        <source>Oui</source>
        <translation>Yes</translation>
    </message>
    <message>
        <location filename="../src/interface/outils/outils.cpp" line="403"/>
        <location filename="../src/interface/outils/outils.cpp" line="433"/>
        <location filename="../src/interface/outils/outils.cpp" line="1141"/>
        <source>Non</source>
        <translation>No</translation>
    </message>
    <message>
        <location filename="../src/interface/outils/outils.cpp" line="431"/>
        <source>Voulez-vous vraiment supprimer le groupe &lt;b&gt;%1&lt;/b&gt; ?</source>
        <translation>Do you really want to remove the &lt;b&gt;%1&lt;/b&gt; group?</translation>
    </message>
    <message>
        <location filename="../src/interface/outils/outils.cpp" line="708"/>
        <source>Le nom du domaine n&apos;est pas spécifié</source>
        <translation>The name of the domain is not given</translation>
    </message>
    <message>
        <location filename="../src/interface/outils/outils.cpp" line="714"/>
        <source>Le nom du groupe n&apos;est pas spécifié</source>
        <translation>The name of the group is not given</translation>
    </message>
    <message>
        <location filename="../src/interface/outils/outils.cpp" line="720"/>
        <source>La liste de fichiers est vide</source>
        <translation>The list of files is empty</translation>
    </message>
    <message>
        <location filename="../src/interface/outils/outils.cpp" line="938"/>
        <location filename="../src/interface/outils/outils.cpp" line="960"/>
        <source>Ouvrir fichier TLE</source>
        <translation>Open TLE file</translation>
    </message>
    <message>
        <location filename="../src/interface/outils/outils.cpp" line="940"/>
        <location filename="../src/interface/outils/outils.cpp" line="962"/>
        <location filename="../src/interface/outils/outils.cpp" line="1073"/>
        <source>Fichiers TLE (*.txt *.tle);;Tous les fichiers (*.*)</source>
        <translation>TLE files (*.txt *.tle);; All files (*)</translation>
    </message>
    <message>
        <location filename="../src/interface/outils/outils.cpp" line="987"/>
        <source>Le nom du fichier à mettre à jour n&apos;est pas spécifié</source>
        <translation>The name of TLE file to update is not given</translation>
    </message>
    <message>
        <location filename="../src/interface/outils/outils.cpp" line="991"/>
        <source>Le nom du fichier à lire n&apos;est pas spécifié</source>
        <translation>The name of TLE file to read is not given</translation>
    </message>
    <message>
        <location filename="../src/interface/outils/outils.cpp" line="1001"/>
        <source>Le fichier %1 n&apos;existe pas</source>
        <translation>The file %1 does not exist</translation>
    </message>
    <message>
        <location filename="../src/interface/outils/outils.cpp" line="1090"/>
        <source>Le fichier %1 existe déjà</source>
        <translation>The file %1 already exists</translation>
    </message>
    <message>
        <location filename="../src/interface/outils/outils.cpp" line="1112"/>
        <source>Le fichier %1 ne contient pas d&apos;éléments orbitaux</source>
        <translation>The file %1 does not contain orbital elements</translation>
    </message>
    <message>
        <location filename="../src/interface/outils/outils.cpp" line="1139"/>
        <source>Avertissement</source>
        <translation>Warning</translation>
    </message>
    <message>
        <location filename="../src/interface/outils/outils.cpp" line="1139"/>
        <source>Voulez-vous vraiment supprimer les fichiers TLE sélectionnés ?</source>
        <translation>Do you really want to remove the selected TLE files ?</translation>
    </message>
</context>
<context>
    <name>PreviSat</name>
    <message>
        <location filename="../src/interface/previsat.ui" line="510"/>
        <location filename="../src/interface/previsat.ui" line="577"/>
        <source>1</source>
        <translation>1</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.ui" line="515"/>
        <location filename="../src/interface/previsat.ui" line="582"/>
        <source>5</source>
        <translation>5</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.ui" line="520"/>
        <location filename="../src/interface/previsat.ui" line="587"/>
        <source>10</source>
        <translation>10</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.ui" line="525"/>
        <location filename="../src/interface/previsat.ui" line="592"/>
        <source>20</source>
        <translation>20</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.ui" line="530"/>
        <location filename="../src/interface/previsat.ui" line="597"/>
        <source>30</source>
        <translation>30</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.ui" line="535"/>
        <location filename="../src/interface/previsat.ui" line="602"/>
        <source>60</source>
        <translation>60</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.ui" line="274"/>
        <source>ISS Live</source>
        <translation>ISS Live</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.ui" line="357"/>
        <source>Fichier :</source>
        <translation>File :</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.ui" line="543"/>
        <location filename="../src/interface/previsat.ui" line="617"/>
        <location filename="../src/interface/previsat.cpp" line="2966"/>
        <location filename="../src/interface/previsat.cpp" line="2983"/>
        <source>secondes</source>
        <translation>seconds</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.ui" line="440"/>
        <source>Menu</source>
        <translation>Menu</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.ui" line="287"/>
        <source>Configuration</source>
        <translation>Settings</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.ui" line="316"/>
        <source>Dons</source>
        <translation>Donations</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.ui" line="379"/>
        <source>Météo du lieu d&apos;observation</source>
        <translation>Weather of user location</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.ui" line="408"/>
        <source>Météo des bases de la NASA</source>
        <translation>Weather of NASA bases</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.ui" line="456"/>
        <location filename="../src/interface/previsat.ui" line="1071"/>
        <source>Outils</source>
        <translation>Tools</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.ui" line="485"/>
        <location filename="../src/interface/previsat.cpp" line="1098"/>
        <source>Mode de fonctionnement</source>
        <translation>Mode</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.ui" line="553"/>
        <location filename="../src/interface/previsat.cpp" line="2658"/>
        <source>Temps réel</source>
        <translation>Real time</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.ui" line="563"/>
        <location filename="../src/interface/previsat.cpp" line="2661"/>
        <source>Mode manuel</source>
        <translation>Manual mode</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.ui" line="245"/>
        <source>Aide</source>
        <translation>Help</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.ui" line="411"/>
        <source>NASA</source>
        <translation>NASA</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.ui" line="622"/>
        <location filename="../src/interface/previsat.cpp" line="2984"/>
        <source>minutes</source>
        <translation>minutes</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.ui" line="627"/>
        <location filename="../src/interface/previsat.cpp" line="2985"/>
        <source>heures</source>
        <translation>hours</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.ui" line="632"/>
        <location filename="../src/interface/previsat.cpp" line="2986"/>
        <source>jours</source>
        <translation>days</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.ui" line="849"/>
        <source>Cliquer pour activer
le flux vidéo</source>
        <translation>Click here to activate
the video stream</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.ui" line="998"/>
        <source>Enregistrer</source>
        <translation>Save</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.ui" line="1039"/>
        <source>Fichier d&apos;aide</source>
        <translation>Help contents</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.ui" line="1042"/>
        <source>F1</source>
        <translation>F1</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.ui" line="1138"/>
        <source>Télécharger la mise à jour</source>
        <translation>Download software update</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.ui" line="1098"/>
        <source>À propos</source>
        <translation>About</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.ui" line="656"/>
        <source>Filtre</source>
        <translation>Filter</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.ui" line="975"/>
        <source>Ctrl+O</source>
        <translation>Ctrl+O</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.ui" line="984"/>
        <source>PayPal</source>
        <translation>PayPal</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.ui" line="993"/>
        <source>Tipeee</source>
        <translation>Tipeee</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.ui" line="1001"/>
        <source>Ctrl+S</source>
        <translation>Ctrl+S</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.ui" line="1006"/>
        <source>Mettre à jour GP courant</source>
        <translation>Update current GP</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.ui" line="1014"/>
        <source>Mettre à jour GP communs</source>
        <translation>Update common GP groups</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.ui" line="1022"/>
        <source>Mettre à jour tous les groupes de GP</source>
        <translation>Update all GP groups</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.ui" line="1059"/>
        <source>Options</source>
        <translation>Options</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.ui" line="1074"/>
        <source>F4</source>
        <translation>F4</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.ui" line="1083"/>
        <source>Dons ...</source>
        <translation>Donations ...</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.ui" line="1088"/>
        <source>Contact</source>
        <translation>Contact</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.ui" line="1093"/>
        <source>Partenaires ...</source>
        <translation>Partnerships ...</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.ui" line="1103"/>
        <source>Exporter fichier log ...</source>
        <translation>Export log file ...</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.ui" line="1108"/>
        <source>Sky-Watcher</source>
        <translation>Sky-Watcher</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.ui" line="1113"/>
        <source>PianetaRadio.it / CatRotator</source>
        <translation>PianetaRadio.it / CatRotator</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.ui" line="1118"/>
        <source>Éléments orbitaux ...</source>
        <translation>Orbital elements ...</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.ui" line="1123"/>
        <source>celestrak.org</source>
        <translation>celestrak.org</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.ui" line="1128"/>
        <source>space-track.org</source>
        <translation>space-track.org</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.ui" line="1133"/>
        <source>Définir par défaut</source>
        <translation>Define as default</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.ui" line="1146"/>
        <source>Mode sombre</source>
        <translation>Dark mode</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.ui" line="1149"/>
        <source>Ctrl+N</source>
        <translation>Ctrl+N</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.ui" line="1009"/>
        <source>F5</source>
        <translation>F5</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.ui" line="1017"/>
        <source>Ctrl+F5</source>
        <translation>Ctrl+F5</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.ui" line="1025"/>
        <source>Shift+F5</source>
        <translation>Shift+F5</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.ui" line="1047"/>
        <source>Informations</source>
        <translation>Information</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.ui" line="1050"/>
        <source>F2</source>
        <translation>F2</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.ui" line="1030"/>
        <source>Mettre à jour les fichiers de données</source>
        <translation>Update data files</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.cpp" line="1080"/>
        <source>Messages</source>
        <translation>Messages</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.cpp" line="1104"/>
        <location filename="../src/interface/previsat.cpp" line="2680"/>
        <source>Date</source>
        <translation>Date</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.cpp" line="1110"/>
        <location filename="../src/interface/previsat.cpp" line="2681"/>
        <source>Heure</source>
        <translation>Hour</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.cpp" line="1176"/>
        <source>Vous venez de mettre à jour %1.
%2Souhaitez-vous faire un don pour soutenir son auteur ?</source>
        <translation>You just update %1.
%2Do you want to make a donation to support its author ?</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.cpp" line="1875"/>
        <source>Fichiers PNG (*.png);;Fichiers JPEG (*.jpg *.jpeg);;Fichiers BMP (*.bmp);;Tous les fichiers (*.*)</source>
        <translation>PNG files (*.png);;JPEG files (*.jpg *.jpeg);;BMP files (*.bmp);;All files (*.*)</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.cpp" line="2677"/>
        <source>dd/MM/yyyy</source>
        <comment>date format</comment>
        <translation>MM/dd/yyyy</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.cpp" line="2966"/>
        <location filename="../src/interface/previsat.cpp" line="2983"/>
        <source>seconde</source>
        <translation>second</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.cpp" line="2984"/>
        <source>minute</source>
        <translation>minute</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.cpp" line="2985"/>
        <source>heure</source>
        <translation>hour</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.cpp" line="2986"/>
        <source>jour</source>
        <translation>day</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.cpp" line="3003"/>
        <source>Agrandir</source>
        <translation>Maximize</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.cpp" line="3012"/>
        <source>Réduire</source>
        <translation>Minimize</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.cpp" line="3038"/>
        <source>Carte du monde</source>
        <translation>World map</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.cpp" line="3046"/>
        <source>Carte du ciel</source>
        <translation>Sky map</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.cpp" line="3258"/>
        <source>onglet_general</source>
        <comment>file name (without accent)</comment>
        <translation>main_tab</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.cpp" line="3259"/>
        <source>onglet_elements</source>
        <comment>file name (without accent)</comment>
        <translation>elements_tab</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.cpp" line="3259"/>
        <source>onglet_informations</source>
        <comment>file name (without accent)</comment>
        <translation>information_tab</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.cpp" line="2670"/>
        <source>Jour julien</source>
        <translation>Julian day</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.cpp" line="1496"/>
        <location filename="../src/interface/previsat.cpp" line="1576"/>
        <source>Une mise à jour %1 est disponible. Souhaitez-vous la télécharger?</source>
        <translation>An update %1 is available. Do you want to download it?</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.cpp" line="1177"/>
        <location filename="../src/interface/previsat.cpp" line="1390"/>
        <location filename="../src/interface/previsat.cpp" line="1498"/>
        <location filename="../src/interface/previsat.cpp" line="1578"/>
        <source>Information</source>
        <translation>Information</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.cpp" line="293"/>
        <source>Mise à jour automatique des éléments orbitaux</source>
        <translation>Automatic update of orbital elements</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.cpp" line="1173"/>
        <source>Cette version comporte %1 lignes de code,
ce qui représente environ %2 pages.</source>
        <translation>This version has %1 lines of code,
which represents approximately %2 pages.</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.cpp" line="2028"/>
        <location filename="../src/interface/previsat.cpp" line="2067"/>
        <location filename="../src/interface/previsat.cpp" line="2185"/>
        <source>dddd dd MMMM yyyy  HH:mm:ss</source>
        <translation>dddd, MMMM dd yyyy  HH:mm:ss</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.cpp" line="2321"/>
        <source>Mise à jour du fichier GP %1 en cours...</source>
        <translation>Updating GP file %1...</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.cpp" line="2327"/>
        <source>Téléchargement terminé</source>
        <translation>Downloading finished</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.cpp" line="1391"/>
        <location filename="../src/interface/previsat.cpp" line="1499"/>
        <location filename="../src/interface/previsat.cpp" line="1579"/>
        <source>Oui</source>
        <translation>Yes</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.cpp" line="1498"/>
        <source>des fichiers internes</source>
        <translation>of internal files</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.cpp" line="1181"/>
        <location filename="../src/interface/previsat.cpp" line="1392"/>
        <location filename="../src/interface/previsat.cpp" line="1500"/>
        <location filename="../src/interface/previsat.cpp" line="1581"/>
        <source>Non</source>
        <translation>No</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.cpp" line="1388"/>
        <source>Les éléments orbitaux sont plus vieux que %1 jour(s). Souhaitez-vous les mettre à jour?</source>
        <translation>The orbital elements are older than %1 day(s). Do you want to update them?</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.cpp" line="347"/>
        <source>Vérification du fichier TLE %1 ...</source>
        <translation>Checking TLE file %1 ...</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.cpp" line="2671"/>
        <source>Jour</source>
        <translation>Day</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.cpp" line="2352"/>
        <source>Mise à jour du groupe d&apos;éléments orbitaux &quot;%1&quot;...</source>
        <translation>Updating &quot;%1&quot; group of orbital elements...</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.cpp" line="2387"/>
        <source>Mise à jour du groupe d&apos;éléments orbitaux &quot;%1&quot; terminée</source>
        <translation>Update of orbital elements group &quot;%1&quot; done</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.cpp" line="2875"/>
        <source>Impossible d&apos;afficher l&apos;aide en ligne</source>
        <translation>Impossible to display online help</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.ui" line="972"/>
        <location filename="../src/interface/previsat.cpp" line="3153"/>
        <source>Importer fichier GP / TLE...</source>
        <translation>Import GP / TLE file...</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.cpp" line="3155"/>
        <source>Fichiers GP (*.xml);;Fichiers TLE (*.txt *.tle);;Tous les fichiers (*.*)</source>
        <translation>GP files (*.xml);;TLE files (*.txt *.tle);;All files (*.*)</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.cpp" line="3173"/>
        <location filename="../src/interface/previsat.cpp" line="3233"/>
        <source>Le fichier %1 ne contient pas d&apos;éléments orbitaux</source>
        <translation>The file %1 does not contain orbital elements</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.cpp" line="3182"/>
        <location filename="../src/interface/previsat.cpp" line="3212"/>
        <source>Le fichier %1 existe déjà</source>
        <translation>The file %1 already exists</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.cpp" line="3265"/>
        <source>Fichiers texte (*.txt);;Tous les fichiers (*.*)</source>
        <translation>Text files (*.txt);;All files (*)</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.cpp" line="3439"/>
        <source>Commun</source>
        <comment>common orbital elements groups</comment>
        <translation>Common</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.cpp" line="3444"/>
        <source>Tous</source>
        <comment>all orbital elements groups</comment>
        <translation>All</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.cpp" line="3529"/>
        <location filename="../src/interface/previsat.cpp" line="3532"/>
        <source>Devise</source>
        <translation>Currency</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.cpp" line="3530"/>
        <location filename="../src/interface/previsat.cpp" line="3532"/>
        <source>Choisissez la devise :</source>
        <translation>Choose currency :</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.cpp" line="3546"/>
        <source>Attention : Il est possible d&apos;effectuer un don PayPal via Tipeee, mais ceci induira des frais supplémentaires</source>
        <translation>Be careful : it is possible to make a PayPal donation by means of Tipeee, but this will incur additional costs</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.cpp" line="3613"/>
        <source>Ouverture du fichier d&apos;éléments orbitaux %1 ...</source>
        <translation>Opening orbiral elements file %1 ...</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.cpp" line="3615"/>
        <source>Fichier d&apos;éléments orbitaux de %1 satellites</source>
        <translation>Orbital elements file of %1 satellites</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.cpp" line="3783"/>
        <source>&lt;b&gt;%1&lt;/b&gt; (numéro NORAD : &lt;b&gt;%2&lt;/b&gt;  -  COSPAR : %3)</source>
        <translation>&lt;b&gt;%1&lt;/b&gt; (NORAD number : &lt;b&gt;%2&lt;/b&gt;  -  COSPAR : %3)</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.cpp" line="1874"/>
        <location filename="../src/interface/previsat.cpp" line="3264"/>
        <source>Enregistrer sous...</source>
        <translation>Save as...</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.cpp" line="1724"/>
        <source>&lt;font color=&apos;blue&apos;&gt;&lt;b&gt;%1&lt;/b&gt;&lt;/font&gt;&lt;br /&gt;NORAD : &lt;b&gt;%2&lt;/b&gt;&lt;br /&gt;COSPAR : &lt;b&gt;%3&lt;/b&gt;</source>
        <translation>&lt;font color=&apos;blue&apos;&gt;&lt;b&gt;%1&lt;/b&gt;&lt;/font&gt;&lt;br /&gt;NORAD : &lt;b&gt;%2&lt;/b&gt;&lt;br /&gt;COSPAR : &lt;b&gt;%3&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../src/interface/previsat.cpp" line="3335"/>
        <source>Pas d&apos;informations à afficher</source>
        <translation>No available information</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../src/configuration/fichierobs.cpp" line="166"/>
        <source>Le fichier ne contient pas de lieux d&apos;observation</source>
        <translation>The file does not contain locations</translation>
    </message>
    <message>
        <location filename="../src/configuration/configuration.cpp" line="864"/>
        <location filename="../src/configuration/configuration.cpp" line="903"/>
        <location filename="../src/configuration/gestionnairexml.cpp" line="366"/>
        <location filename="../src/configuration/gestionnairexml.cpp" line="459"/>
        <location filename="../src/configuration/gestionnairexml.cpp" line="593"/>
        <location filename="../src/configuration/gestionnairexml.cpp" line="705"/>
        <location filename="../src/configuration/gestionnairexml.cpp" line="909"/>
        <location filename="../src/configuration/gestionnairexml.cpp" line="1067"/>
        <location filename="../src/configuration/gestionnairexml.cpp" line="1161"/>
        <location filename="../src/configuration/gestionnairexml.cpp" line="1251"/>
        <location filename="../src/configuration/gestionnairexml.cpp" line="1345"/>
        <location filename="../src/librairies/corps/corps.cpp" line="680"/>
        <source>Le fichier %1 n&apos;existe pas ou est vide, veuillez réinstaller %2</source>
        <translation>The file %1 does not exist or is empty, please re-install %2</translation>
    </message>
    <message>
        <location filename="../src/configuration/configuration.cpp" line="876"/>
        <location filename="../src/configuration/configuration.cpp" line="917"/>
        <location filename="../src/configuration/evenementsstationspatiale.cpp" line="116"/>
        <location filename="../src/configuration/gestionnairexml.cpp" line="411"/>
        <location filename="../src/configuration/gestionnairexml.cpp" line="560"/>
        <location filename="../src/configuration/gestionnairexml.cpp" line="821"/>
        <location filename="../src/configuration/gestionnairexml.cpp" line="954"/>
        <location filename="../src/configuration/gestionnairexml.cpp" line="1124"/>
        <location filename="../src/configuration/gestionnairexml.cpp" line="1214"/>
        <location filename="../src/configuration/gestionnairexml.cpp" line="1308"/>
        <location filename="../src/configuration/gestionnairexml.cpp" line="1411"/>
        <location filename="../src/librairies/corps/corps.cpp" line="670"/>
        <location filename="../src/librairies/dates/date.cpp" line="445"/>
        <source>Erreur lors de la lecture du fichier %1, veuillez réinstaller %2</source>
        <translation>Error while reading file %1, please re-install %2</translation>
    </message>
    <message>
        <location filename="../src/configuration/configuration.cpp" line="1016"/>
        <source>Le répertoire %1 n&apos;existe pas, veuillez réinstaller %2</source>
        <translation>The directory %1 does not exist, please re-install %2</translation>
    </message>
    <message>
        <location filename="../src/configuration/configuration.cpp" line="1081"/>
        <location filename="../src/configuration/gestionnairexml.cpp" line="1443"/>
        <source>Le fichier %1 n&apos;existe pas, veuillez réinstaller %2</source>
        <translation>The file %1 does not exist, please re-install %2</translation>
    </message>
    <message>
        <location filename="../src/configuration/configuration.cpp" line="1088"/>
        <source>Le fichier %1 est vide, veuillez réinstaller %2</source>
        <translation>The file %1 is empty, please re-install %2</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="648"/>
        <source>Lieu d&apos;observation              : %1     %2 %3   %4 %5   %6 %7</source>
        <translation>Location                       : %1     %2 %3   %4 %5   %6 %7</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="650"/>
        <source>Ouest</source>
        <translation>West</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="650"/>
        <source>Est</source>
        <translation>East</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="653"/>
        <source>Nord</source>
        <translation>North</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="653"/>
        <source>Sud</source>
        <translation>South</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="655"/>
        <location filename="../src/interface/afficherresultats.cpp" line="657"/>
        <source>km</source>
        <comment>kilometer</comment>
        <translatorcomment>Symbol for kilometer</translatorcomment>
        <translation>km</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="657"/>
        <source>m</source>
        <comment>meter</comment>
        <translatorcomment>Symbol for meter</translatorcomment>
        <translation>m</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="657"/>
        <source>ft</source>
        <comment>foot</comment>
        <translatorcomment>Symbol for foot unit</translatorcomment>
        <translation>ft</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="664"/>
        <source>Fuseau horaire                  : %1</source>
        <translation>Timezone                       : %1</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="665"/>
        <source>UTC</source>
        <comment>Universal Time Coordinated</comment>
        <translation>UTC</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="679"/>
        <source>Conditions d&apos;observations       :</source>
        <translation>Conditions of observations     :</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="888"/>
        <location filename="../src/interface/afficherresultats.cpp" line="1152"/>
        <source>W</source>
        <comment>West</comment>
        <translation>W</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="888"/>
        <location filename="../src/interface/afficherresultats.cpp" line="1152"/>
        <source>E</source>
        <comment>East</comment>
        <translation>E</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="889"/>
        <location filename="../src/interface/afficherresultats.cpp" line="1153"/>
        <source>N</source>
        <comment>North</comment>
        <translation>N</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="889"/>
        <location filename="../src/interface/afficherresultats.cpp" line="1153"/>
        <source>S</source>
        <comment>South</comment>
        <translation>S</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="680"/>
        <source>Hauteur minimale du satellite = %1°</source>
        <translatorcomment>Be careful with spaces</translatorcomment>
        <translation>Minimum elevation of the satellite = %1°</translation>
    </message>
    <message>
        <location filename="../src/interface/afficherresultats.cpp" line="686"/>
        <source>Hauteur maximale du Soleil = %1°</source>
        <translatorcomment>Be careful with spaces</translatorcomment>
        <translation>Maximum elevation of the Sun = %1°</translation>
    </message>
    <message>
        <location filename="../src/librairies/corps/corps.cpp" line="165"/>
        <source>Tableau de constellations vide</source>
        <translation>Empty constellation table</translation>
    </message>
    <message>
        <location filename="../src/librairies/corps/etoiles/constellation.cpp" line="101"/>
        <source>Le tableau de constellations n&apos;est pas initialisé</source>
        <translation>The constellation table is not initialized</translation>
    </message>
    <message>
        <location filename="../src/librairies/corps/etoiles/etoile.cpp" line="103"/>
        <location filename="../src/librairies/corps/etoiles/ligneconstellation.cpp" line="92"/>
        <source>Le tableau d&apos;étoiles n&apos;est pas initialisé</source>
        <translation>The star table is not initialized</translation>
    </message>
    <message>
        <location filename="../src/librairies/corps/etoiles/ligneconstellation.cpp" line="88"/>
        <source>Le tableau de lignes de constellation n&apos;est pas initialisé</source>
        <translation>The constellation line table is not initialized</translation>
    </message>
    <message>
        <location filename="../src/librairies/corps/satellite/tle.cpp" line="480"/>
        <source>Le fichier %1 n&apos;est pas valide</source>
        <translation>The file %1 is not valid</translation>
    </message>
    <message>
        <location filename="../src/librairies/corps/satellite/tle.cpp" line="494"/>
        <source>Le fichier %1 n&apos;existe pas</source>
        <translation>The file %1 does not exist</translation>
    </message>
    <message>
        <location filename="../src/librairies/corps/satellite/tle.cpp" line="499"/>
        <source>Le fichier %1 est vide</source>
        <translation>The file %1 is empty</translation>
    </message>
    <message>
        <location filename="../src/librairies/corps/satellite/gpformat.cpp" line="119"/>
        <location filename="../src/librairies/corps/satellite/gpformat.cpp" line="199"/>
        <location filename="../src/librairies/corps/satellite/tle.cpp" line="156"/>
        <source>Le fichier %1 n&apos;existe pas ou est vide</source>
        <translation>The file %1 does not exist or is empty</translation>
    </message>
    <message>
        <location filename="../src/librairies/corps/satellite/gpformat.cpp" line="130"/>
        <location filename="../src/librairies/corps/satellite/gpformat.cpp" line="142"/>
        <location filename="../src/librairies/corps/satellite/gpformat.cpp" line="210"/>
        <location filename="../src/librairies/corps/satellite/gpformat.cpp" line="222"/>
        <location filename="../src/librairies/corps/satellite/tle.cpp" line="505"/>
        <source>Le fichier %1 ne contient aucun satellite</source>
        <translation>The file %1 does not contain satellites</translation>
    </message>
    <message>
        <location filename="../src/librairies/corps/satellite/tle.cpp" line="364"/>
        <source>Le satellite %1 (numéro NORAD : %2) n&apos;existe pas dans le fichier à mettre à jour.
Voulez-vous ajouter ce TLE dans le fichier à mettre à jour ?</source>
        <translation>The satellite %1 (NORAD number : %2) does not exist in the file to update.
Do you want to add this TLE in the file to update?</translation>
    </message>
    <message>
        <location filename="../src/librairies/corps/satellite/tle.cpp" line="367"/>
        <source>Ajout du nouveau TLE</source>
        <translation>Add of the new TLE</translation>
    </message>
    <message>
        <location filename="../src/configuration/gestionnairexml.cpp" line="449"/>
        <source>Le fichier de configuration de %1 a évolué.
Souhaitez-vous tenter de récupérer les lieux d&apos;observation ?</source>
        <translation>The configuration file of %1 has evolved.
Do you want to try to recover the places of location ?</translation>
    </message>
    <message>
        <location filename="../src/configuration/gestionnairexml.cpp" line="1501"/>
        <location filename="../src/librairies/corps/satellite/tle.cpp" line="337"/>
        <location filename="../src/librairies/corps/satellite/tle.cpp" line="370"/>
        <source>Oui</source>
        <translation>Yes</translation>
    </message>
    <message>
        <location filename="../src/librairies/corps/satellite/tle.cpp" line="338"/>
        <location filename="../src/librairies/corps/satellite/tle.cpp" line="371"/>
        <source>Oui à tout</source>
        <translation>Yes to all</translation>
    </message>
    <message>
        <location filename="../src/configuration/gestionnairexml.cpp" line="1502"/>
        <location filename="../src/librairies/corps/satellite/tle.cpp" line="339"/>
        <location filename="../src/librairies/corps/satellite/tle.cpp" line="372"/>
        <source>Non</source>
        <translation>No</translation>
    </message>
    <message>
        <location filename="../src/librairies/corps/satellite/tle.cpp" line="340"/>
        <location filename="../src/librairies/corps/satellite/tle.cpp" line="373"/>
        <source>Non à tout</source>
        <translation>No to all</translation>
    </message>
    <message>
        <location filename="../src/librairies/corps/satellite/tle.cpp" line="331"/>
        <source>Le satellite %1 (numéro NORAD : %2) n&apos;existe pas dans le fichier de TLE récents.
Voulez-vous supprimer ce TLE du fichier à mettre à jour ?</source>
        <translation>The satellite %1 (NORAD number : %2) does not exist in the TLE file to read.
Do you want to remove this TLE from the file to update?</translation>
    </message>
    <message>
        <location filename="../src/librairies/corps/satellite/tle.cpp" line="266"/>
        <location filename="../src/librairies/corps/satellite/tle.cpp" line="276"/>
        <source>Le fichier %1 n&apos;est pas un TLE</source>
        <translation>The file %1 is not a TLE</translation>
    </message>
    <message>
        <location filename="../src/librairies/corps/satellite/tle.cpp" line="334"/>
        <source>Suppression du TLE</source>
        <translation>Deletion of the TLE</translation>
    </message>
    <message>
        <location filename="../src/librairies/corps/satellite/tle.cpp" line="582"/>
        <source>Une des lignes du TLE est vide</source>
        <translation>One of the TLE line is empty</translation>
    </message>
    <message>
        <location filename="../src/librairies/corps/satellite/tle.cpp" line="589"/>
        <source>Les numéros de ligne du TLE du satellite %1 (numéro NORAD : %2 ) sont incorrects</source>
        <translation>The numbers of the lines of TLE of satellite %1 (NORAD number : %2) are incorrect</translation>
    </message>
    <message>
        <location filename="../src/librairies/corps/satellite/tle.cpp" line="595"/>
        <source>La longueur des lignes du TLE du satellite %1 (numéro NORAD : %2) est incorrecte</source>
        <translation>The length of lines of TLE of the satellite %1 (NORAD number %2) is incorrect</translation>
    </message>
    <message>
        <location filename="../src/librairies/corps/satellite/tle.cpp" line="605"/>
        <source>Erreur position des espaces du TLE :
Satellite %1 - numéro NORAD : %2</source>
        <translation>Error of position of spaces in TLE :
Satellite %1 - NORAD number : %2</translation>
    </message>
    <message>
        <location filename="../src/librairies/corps/satellite/tle.cpp" line="613"/>
        <source>Erreur Ponctuation du TLE :
Satellite %1 - numéro NORAD : %2</source>
        <translation>Punctuation Error of TLE :
Satellite %1 - NORAD number : %2</translation>
    </message>
    <message>
        <location filename="../src/librairies/corps/satellite/tle.cpp" line="620"/>
        <source>Les deux lignes du TLE du satellite %1 ont des numéros NORAD différents (%2 et %3)</source>
        <translation>The two lines of TLE of satellite %1 have different NORAD numbers (%2 and %3)</translation>
    </message>
    <message>
        <location filename="../src/librairies/corps/satellite/tle.cpp" line="626"/>
        <source>Erreur CheckSum ligne 1 :
Satellite %1 - numéro NORAD : %2</source>
        <translation>CheckSum Error line 1 :
Satellite %1 - NORAD number : %2</translation>
    </message>
    <message>
        <location filename="../src/librairies/corps/satellite/tle.cpp" line="631"/>
        <source>Erreur CheckSum ligne 2 :
Satellite %1 - numéro NORAD : %2</source>
        <translation>CheckSum Error line 2 :
Satellite %1 - NORAD number : %2</translation>
    </message>
    <message>
        <location filename="../src/librairies/corps/systemesolaire/lune.cpp" line="267"/>
        <location filename="../test/src/librairies/corps/systemesolaire/lunetest.cpp" line="119"/>
        <source>Nouvelle Lune</source>
        <translation>New Moon</translation>
    </message>
    <message>
        <location filename="../src/librairies/corps/systemesolaire/lune.cpp" line="271"/>
        <source>Premier croissant</source>
        <translation>Waxing crescent</translation>
    </message>
    <message>
        <location filename="../src/librairies/corps/systemesolaire/lune.cpp" line="271"/>
        <location filename="../test/src/librairies/corps/systemesolaire/lunetest.cpp" line="108"/>
        <source>Dernier croissant</source>
        <translation>Waning crescent</translation>
    </message>
    <message>
        <location filename="../src/librairies/corps/systemesolaire/lune.cpp" line="275"/>
        <location filename="../test/src/librairies/corps/systemesolaire/lunetest.cpp" line="86"/>
        <source>Premier quartier</source>
        <translation>First quarter</translation>
    </message>
    <message>
        <location filename="../src/librairies/corps/systemesolaire/lune.cpp" line="275"/>
        <source>Dernier quartier</source>
        <translation>Last quarter</translation>
    </message>
    <message>
        <location filename="../src/librairies/corps/systemesolaire/lune.cpp" line="279"/>
        <location filename="../test/src/librairies/corps/systemesolaire/lunetest.cpp" line="97"/>
        <source>Gibbeuse croissante</source>
        <translation>Waxing gibbous</translation>
    </message>
    <message>
        <location filename="../src/librairies/corps/systemesolaire/lune.cpp" line="279"/>
        <source>Gibbeuse décroissante</source>
        <translation>Waning gibbous</translation>
    </message>
    <message>
        <location filename="../src/librairies/corps/systemesolaire/lune.cpp" line="283"/>
        <location filename="../test/src/librairies/corps/systemesolaire/lunetest.cpp" line="130"/>
        <source>Pleine Lune</source>
        <translation>Full Moon</translation>
    </message>
    <message>
        <location filename="../src/librairies/dates/date.cpp" line="516"/>
        <source>dd/MM/yyyy</source>
        <comment>Date format</comment>
        <translation>MM/dd/yyyy</translation>
    </message>
    <message>
        <location filename="../src/librairies/dates/date.cpp" line="295"/>
        <source>Date au format ISO vide</source>
        <translation>Date with ISO format is empty</translation>
    </message>
    <message>
        <location filename="../src/librairies/dates/date.cpp" line="304"/>
        <source>Date au format ISO invalide</source>
        <translation>Date with ISO format is invalid</translation>
    </message>
    <message>
        <location filename="../src/librairies/dates/date.cpp" line="342"/>
        <source>Date au format NASA vide</source>
        <translation>Date with NASA format is empty</translation>
    </message>
    <message>
        <location filename="../src/librairies/dates/date.cpp" line="351"/>
        <location filename="../src/librairies/dates/date.cpp" line="360"/>
        <location filename="../src/librairies/dates/date.cpp" line="374"/>
        <source>Date au format NASA invalide</source>
        <translation>Date with NASA format is invalid</translation>
    </message>
    <message>
        <location filename="../src/librairies/dates/date.cpp" line="409"/>
        <source>Le fichier taiutc.dat n&apos;existe pas</source>
        <translation>The taiutc.dat file does not exist</translation>
    </message>
    <message>
        <location filename="../src/librairies/dates/date.cpp" line="474"/>
        <source>dddd dd MMMM yyyy HH:mm:ss</source>
        <comment>Date format</comment>
        <translation>dddd, MMMM dd yyyy  HH:mm:ss</translation>
    </message>
    <message>
        <location filename="../src/librairies/dates/date.cpp" line="691"/>
        <source>Ecarts TAI-UTC non initialisés</source>
        <translation>TAI-UTC offsets not initialized</translation>
    </message>
    <message>
        <location filename="../src/librairies/exceptions/message.cpp" line="75"/>
        <source>Information</source>
        <translation>Information</translation>
    </message>
    <message>
        <location filename="../src/configuration/gestionnairexml.cpp" line="1500"/>
        <location filename="../src/librairies/exceptions/message.cpp" line="80"/>
        <source>Avertissement</source>
        <translation>Warning</translation>
    </message>
    <message>
        <location filename="../src/librairies/exceptions/message.cpp" line="85"/>
        <source>Erreur</source>
        <translation>Error</translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="96"/>
        <source>Une instance de %1 est déjà ouverte</source>
        <translation>An instance of %1 is already launched</translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="151"/>
        <source>Initialisation de la configuration...</source>
        <translation>Initialisation of configuration...</translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="167"/>
        <source>Ouverture du fichier GP...</source>
        <translation>Loading GP file...</translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="173"/>
        <source>Mise à jour des éléments orbitaux...</source>
        <translation>Updating orbital elements...</translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="179"/>
        <source>Démarrage de l&apos;application...</source>
        <translation>Launching application...</translation>
    </message>
    <message>
        <location filename="../src/previsions/evenementsorbitaux.cpp" line="237"/>
        <source>Noeud Ascendant - PSO = 0°</source>
        <comment>In orbit position</comment>
        <translation>Ascending node - Position = 0°</translation>
    </message>
    <message>
        <location filename="../src/previsions/evenementsorbitaux.cpp" line="238"/>
        <source>Noeud Descendant - PSO = 180°</source>
        <comment>In orbit position</comment>
        <translation>Descending node - Position = 180°</translation>
    </message>
    <message>
        <location filename="../src/previsions/evenementsorbitaux.cpp" line="265"/>
        <source>Pénombre -&gt; Ombre</source>
        <translation>Penumbra -&gt; Shadow</translation>
    </message>
    <message>
        <location filename="../src/previsions/evenementsorbitaux.cpp" line="265"/>
        <source>Ombre -&gt; Pénombre</source>
        <translation>Shadow -&gt; Penumbra</translation>
    </message>
    <message>
        <location filename="../src/previsions/evenementsorbitaux.cpp" line="284"/>
        <source>Lumière -&gt; Pénombre</source>
        <translation>Light -&gt; Penumbra</translation>
    </message>
    <message>
        <location filename="../src/previsions/evenementsorbitaux.cpp" line="284"/>
        <source>Pénombre -&gt; Lumière</source>
        <translation>Penumbra -&gt; Light</translation>
    </message>
    <message>
        <location filename="../src/previsions/evenementsorbitaux.cpp" line="306"/>
        <source>Pénombre -&gt; Ombre (Lune)</source>
        <translation>Penumbra -&gt; Shadow (Moon)</translation>
    </message>
    <message>
        <location filename="../src/previsions/evenementsorbitaux.cpp" line="306"/>
        <source>Ombre -&gt; Pénombre (Lune)</source>
        <translation>Penumbra -&gt; Shadow (Moon)</translation>
    </message>
    <message>
        <location filename="../src/previsions/evenementsorbitaux.cpp" line="325"/>
        <source>Lumière -&gt; Pénombre (Lune)</source>
        <translation>Light -&gt; Penumbra (Moon)</translation>
    </message>
    <message>
        <location filename="../src/previsions/evenementsorbitaux.cpp" line="325"/>
        <source>Pénombre -&gt; Lumière (Lune)</source>
        <translation>Penumbra -&gt; Light (Moon)</translation>
    </message>
    <message>
        <location filename="../src/previsions/evenementsorbitaux.cpp" line="366"/>
        <source>nmi</source>
        <comment>nautical mile</comment>
        <translation>nmi</translation>
    </message>
    <message>
        <location filename="../src/previsions/evenementsorbitaux.cpp" line="371"/>
        <source>Périgée : %1%2 (%3%2)</source>
        <translation>Perigee : %1%2 (%3%2)</translation>
    </message>
    <message>
        <location filename="../src/previsions/evenementsorbitaux.cpp" line="371"/>
        <source>Apogée : %1%2 (%3%2)</source>
        <translation>Apogee : %1%2 (%3%2)</translation>
    </message>
    <message>
        <location filename="../src/previsions/evenementsorbitaux.cpp" line="392"/>
        <source>Passage terminateur Jour -&gt; Nuit</source>
        <translation>Day -&gt; Night terminator pass</translation>
    </message>
    <message>
        <location filename="../src/previsions/evenementsorbitaux.cpp" line="393"/>
        <source>Passage terminateur Nuit -&gt; Jour</source>
        <translation>Night-&gt;Day terminator pass</translation>
    </message>
    <message>
        <location filename="../src/previsions/evenementsorbitaux.cpp" line="417"/>
        <source>Passage à PSO = %1°</source>
        <comment>In orbit position</comment>
        <translation>Pass to position = %1°</translation>
    </message>
    <message>
        <location filename="../src/librairies/maths/maths.cpp" line="201"/>
        <source>h</source>
        <comment>hour (angle)</comment>
        <translatorcomment>Symbol for hour (angle)</translatorcomment>
        <translation>h</translation>
    </message>
    <message>
        <location filename="../src/librairies/maths/maths.cpp" line="202"/>
        <source>m</source>
        <comment>minute (angle)</comment>
        <translatorcomment>Symbol for minute (angle)</translatorcomment>
        <translation>m</translation>
    </message>
    <message>
        <location filename="../src/librairies/maths/maths.cpp" line="203"/>
        <source>s</source>
        <comment>second (angle)</comment>
        <translatorcomment>Symbol for second (angle)</translatorcomment>
        <translation>s</translation>
    </message>
    <message>
        <location filename="../src/librairies/corps/satellite/evenements.cpp" line="75"/>
        <location filename="../test/src/librairies/corps/satellite/evenementstest.cpp" line="102"/>
        <location filename="../test/src/librairies/corps/satellite/evenementstest.cpp" line="114"/>
        <source>LOS</source>
        <comment>Loss of signal</comment>
        <translation>LOS</translation>
    </message>
    <message>
        <location filename="../src/librairies/corps/satellite/evenements.cpp" line="107"/>
        <location filename="../src/librairies/corps/satellite/evenementsconst.h" line="73"/>
        <location filename="../test/src/librairies/corps/satellite/evenementstest.cpp" line="83"/>
        <location filename="../test/src/librairies/corps/satellite/evenementstest.cpp" line="95"/>
        <source>AOS</source>
        <comment>Acquisition of signal</comment>
        <translation>AOS</translation>
    </message>
    <message>
        <location filename="../src/previsions/flashs.cpp" line="76"/>
        <source>FCB</source>
        <comment>Front, Central, Backward panels of MetOp satellite</comment>
        <translation>FCB</translation>
    </message>
    <message>
        <location filename="../src/librairies/corps/satellite/donnees.cpp" line="86"/>
        <source>N/A</source>
        <translation>N/A</translation>
    </message>
</context>
<context>
    <name>Radar</name>
    <message>
        <location filename="../src/interface/radar/radar.ui" line="58"/>
        <location filename="../src/interface/radar/radar.cpp" line="347"/>
        <location filename="../src/interface/radar/radar.cpp" line="350"/>
        <source>Nord</source>
        <translation>North</translation>
    </message>
    <message>
        <location filename="../src/interface/radar/radar.ui" line="73"/>
        <location filename="../src/interface/radar/radar.cpp" line="357"/>
        <location filename="../src/interface/radar/radar.cpp" line="360"/>
        <source>Ouest</source>
        <translation>West</translation>
    </message>
    <message>
        <location filename="../src/interface/radar/radar.ui" line="114"/>
        <location filename="../src/interface/radar/radar.cpp" line="356"/>
        <location filename="../src/interface/radar/radar.cpp" line="361"/>
        <source>Est</source>
        <translation>East</translation>
    </message>
    <message>
        <location filename="../src/interface/radar/radar.ui" line="126"/>
        <location filename="../src/interface/radar/radar.cpp" line="346"/>
        <location filename="../src/interface/radar/radar.cpp" line="351"/>
        <source>Sud</source>
        <translation>South</translation>
    </message>
    <message>
        <location filename="../src/interface/radar/radar.cpp" line="139"/>
        <source>Azimut : %1°</source>
        <translation>Azimuth : %1°</translation>
    </message>
    <message>
        <location filename="../src/interface/radar/radar.cpp" line="140"/>
        <source>Hauteur : %1°</source>
        <translation>Elevation : %1°</translation>
    </message>
    <message>
        <location filename="../src/interface/radar/radar.cpp" line="158"/>
        <source>&lt;font color=&apos;blue&apos;&gt;&lt;b&gt;%1&lt;/b&gt;&lt;/font&gt;&lt;br /&gt;NORAD : &lt;b&gt;%2&lt;/b&gt;&lt;br /&gt;COSPAR : &lt;b&gt;%3&lt;/b&gt;</source>
        <translation>&lt;font color=&apos;blue&apos;&gt;&lt;b&gt;%1&lt;/b&gt;&lt;/font&gt;&lt;br /&gt;NORAD : &lt;b&gt;%2&lt;/b&gt;&lt;br /&gt;COSPAR : &lt;b&gt;%3&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../src/interface/radar/radar.cpp" line="161"/>
        <source>&lt;b&gt;%1&lt;/b&gt; (numéro NORAD : &lt;b&gt;%2&lt;/b&gt;  -  COSPAR : &lt;b&gt;%3&lt;/b&gt;)</source>
        <translation>&lt;b&gt;%1&lt;/b&gt; (NORAD number : &lt;b&gt;%2&lt;/b&gt;  -  COSPAR : &lt;b&gt;%3&lt;/b&gt;)</translation>
    </message>
    <message>
        <location filename="../src/interface/radar/radar.cpp" line="188"/>
        <location filename="../src/interface/radar/radar.cpp" line="189"/>
        <source>Soleil</source>
        <translation>Sun</translation>
    </message>
    <message>
        <location filename="../src/interface/radar/radar.cpp" line="215"/>
        <location filename="../src/interface/radar/radar.cpp" line="216"/>
        <source>Lune</source>
        <translation>Moon</translation>
    </message>
</context>
<context>
    <name>RechercheSatellite</name>
    <message>
        <location filename="../src/interface/onglets/donnees/recherchesatellite.ui" line="54"/>
        <location filename="../src/interface/onglets/donnees/recherchesatellite.cpp" line="228"/>
        <source>Objets trouvés :</source>
        <translation>Found objects :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/recherchesatellite.ui" line="82"/>
        <source>Entrez au minimum 3 lettres du nom de l&apos;objet</source>
        <translation>Enter at least 3 letters of the object name</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/recherchesatellite.ui" line="95"/>
        <location filename="../src/interface/onglets/donnees/recherchesatellite.ui" line="187"/>
        <source>Nom :</source>
        <translation>Name :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/recherchesatellite.ui" line="221"/>
        <source>Numéro NORAD :</source>
        <translation>NORAD number :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/recherchesatellite.ui" line="234"/>
        <source>Désignation COSPAR :</source>
        <translation>COSPAR designation :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/recherchesatellite.ui" line="302"/>
        <source>Magnitude std/max :</source>
        <translation>Std/Max magnitude :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/recherchesatellite.ui" line="315"/>
        <source>Modèle orbital :</source>
        <translation>Propagation model :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/recherchesatellite.ui" line="328"/>
        <source>Dimensions/Section :</source>
        <translation>Dimensions/Section :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/recherchesatellite.ui" line="435"/>
        <source>Date de lancement :</source>
        <translation>Launch date :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/recherchesatellite.ui" line="445"/>
        <source>Date de rentrée :</source>
        <translation>Reentry date :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/recherchesatellite.ui" line="455"/>
        <source>Catégorie d&apos;orbite :</source>
        <translation>Orbital category :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/recherchesatellite.ui" line="465"/>
        <source>Pays/Organisation :</source>
        <translation>Country/Organization :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/recherchesatellite.ui" line="475"/>
        <source>Site de lancement :</source>
        <translation>Launch site :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/recherchesatellite.ui" line="568"/>
        <source>Apogée (Altitude) :</source>
        <translation>Apogee (Altitude) :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/recherchesatellite.ui" line="578"/>
        <source>Périgée (Altitude) :</source>
        <translation>Perigee (Altitude) :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/recherchesatellite.ui" line="588"/>
        <source>Période orbitale :</source>
        <translation>Orbital period :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/recherchesatellite.ui" line="601"/>
        <source>Inclinaison :</source>
        <translation>Inclination :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/recherchesatellite.ui" line="681"/>
        <source>Fichiers :</source>
        <translation>Files :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/recherchesatellite.ui" line="739"/>
        <source>COSPAR :</source>
        <translation>COSPAR :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/recherchesatellite.ui" line="783"/>
        <source>NORAD :</source>
        <translation>NORAD :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/recherchesatellite.cpp" line="116"/>
        <source>Problème de droits d&apos;écriture du fichier %1</source>
        <translation>Unable to write file %1</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/recherchesatellite.cpp" line="128"/>
        <source>Nom                :</source>
        <translation>Name                 :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/recherchesatellite.cpp" line="130"/>
        <source>Numéro NORAD       : %1		Magnitude std/max  : %2</source>
        <comment>Standard/Maximum magnitude</comment>
        <translation>NORAD number         : %1			Std/max magnitude  : %2</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/recherchesatellite.cpp" line="133"/>
        <source>Désignation COSPAR : %1		Modèle orbital     : %2</source>
        <translation>COSPAR designation   : %1		Propagation model  : %2</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/recherchesatellite.cpp" line="136"/>
        <source>Dimensions/Section : %1%2</source>
        <translation>Dimensions/Section   : %1%2</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/recherchesatellite.cpp" line="137"/>
        <location filename="../src/interface/onglets/donnees/recherchesatellite.cpp" line="495"/>
        <source>Inconnues</source>
        <translation>Unknown</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/recherchesatellite.cpp" line="140"/>
        <source>Date de lancement  : %1		Apogée  (Altitude) : %2</source>
        <translation>Launch date          : %1		Apogee  (Altitude) : %2</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/recherchesatellite.cpp" line="143"/>
        <source>Date de rentrée    : %1		</source>
        <translation>Decay date    : %1			</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/recherchesatellite.cpp" line="144"/>
        <location filename="../src/interface/onglets/donnees/recherchesatellite.cpp" line="147"/>
        <source>Catégorie d&apos;orbite : %1		</source>
        <translation>Orbital category     : %1			</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/recherchesatellite.cpp" line="145"/>
        <source>Périgée (Altitude) : %1</source>
        <translation>Perigee (Altitude) : %1</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/recherchesatellite.cpp" line="148"/>
        <location filename="../src/interface/onglets/donnees/recherchesatellite.cpp" line="151"/>
        <source>Pays/Organisation  : %1		</source>
        <translation>Country/Organization : %1			</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/recherchesatellite.cpp" line="149"/>
        <source>Période orbitale   : %1</source>
        <translation>Orbital period     : %1</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/recherchesatellite.cpp" line="152"/>
        <source>Site de lancement  : %1		</source>
        <translation>Launch site          : %1			</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/recherchesatellite.cpp" line="153"/>
        <source>Inclinaison        : %1</source>
        <translation>Inclination        : %1</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/recherchesatellite.cpp" line="156"/>
        <source>Site de lancement  : %1</source>
        <translation>Launch site          : %1</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/recherchesatellite.cpp" line="231"/>
        <source>Objets trouvés (%1) :</source>
        <translation>Found objects (%1) :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/recherchesatellite.cpp" line="417"/>
        <location filename="../src/interface/onglets/donnees/recherchesatellite.cpp" line="512"/>
        <location filename="../src/interface/onglets/donnees/recherchesatellite.cpp" line="518"/>
        <location filename="../src/interface/onglets/donnees/recherchesatellite.cpp" line="561"/>
        <location filename="../src/interface/onglets/donnees/recherchesatellite.cpp" line="564"/>
        <source>Inconnu</source>
        <translation>Unknown</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/recherchesatellite.cpp" line="423"/>
        <location filename="../src/interface/onglets/donnees/recherchesatellite.cpp" line="525"/>
        <location filename="../src/interface/onglets/donnees/recherchesatellite.cpp" line="530"/>
        <location filename="../src/interface/onglets/donnees/recherchesatellite.cpp" line="533"/>
        <location filename="../src/interface/onglets/donnees/recherchesatellite.cpp" line="558"/>
        <source>Inconnue</source>
        <translation>Unknown</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/recherchesatellite.cpp" line="448"/>
        <source>SGP4 (DS)</source>
        <comment>Orbital model SGP4 (deep space)</comment>
        <translation>SGP4 (DS)</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/recherchesatellite.cpp" line="448"/>
        <source>SGP4 (NE)</source>
        <comment>Orbital model SGP4 (near Earth)</comment>
        <translation>SGP4 (NE)</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/recherchesatellite.cpp" line="449"/>
        <source>Non applicable</source>
        <translation>Not applicable</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/recherchesatellite.cpp" line="453"/>
        <source>Modèle haute orbite</source>
        <translation>High orbit model</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/recherchesatellite.cpp" line="453"/>
        <source>Modèle basse orbite</source>
        <translation>Low orbit model</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/recherchesatellite.cpp" line="461"/>
        <source>m</source>
        <comment>meter</comment>
        <translation>m</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/recherchesatellite.cpp" line="462"/>
        <source>km</source>
        <comment>kilometer</comment>
        <translation>km</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/recherchesatellite.cpp" line="474"/>
        <source>ft</source>
        <comment>foot</comment>
        <translation>ft</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/recherchesatellite.cpp" line="475"/>
        <source>nmi</source>
        <comment>nautical mile</comment>
        <translation>nmi</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/recherchesatellite.cpp" line="480"/>
        <source>Sphérique. R=%1 %2</source>
        <comment>R = radius</comment>
        <translation>Spherical. R=%1 %2</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/recherchesatellite.cpp" line="485"/>
        <source>Cylindrique. L=%1 %2, R=%3 %2</source>
        <comment>L = height, R = radius</comment>
        <translation>Cylindrical. L=%1 %2, R=%3 %2</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/donnees/recherchesatellite.cpp" line="490"/>
        <source>Boîte. %1 x %2 x %3 %4</source>
        <translation>Box. %1 x %2 x %3 %4</translation>
    </message>
</context>
<context>
    <name>SuiviTelescope</name>
    <message>
        <location filename="../src/interface/onglets/telescope/suivitelescope.ui" line="50"/>
        <source>Lieu d&apos;observation :</source>
        <translation>Name of location :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/telescope/suivitelescope.ui" line="576"/>
        <source>Paramétrage par défaut</source>
        <translation>Default settings</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/telescope/suivitelescope.ui" line="180"/>
        <source>Générer les positions</source>
        <translation>Generate positions</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/telescope/suivitelescope.ui" line="205"/>
        <source>Lever du satellite :</source>
        <translation>Satellite rising :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/telescope/suivitelescope.ui" line="219"/>
        <source>Hauteur maximale :</source>
        <translation>Maximum elevation :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/telescope/suivitelescope.ui" line="233"/>
        <source>Coucher du satellite :</source>
        <translation>Satellite setting :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/telescope/suivitelescope.ui" line="279"/>
        <source>Hauteur minimale du satellite :</source>
        <translation>Minimum elevation of satellite :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/telescope/suivitelescope.ui" line="302"/>
        <source>0°</source>
        <translation>0°</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/telescope/suivitelescope.ui" line="307"/>
        <source>5°</source>
        <translation>5°</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/telescope/suivitelescope.ui" line="312"/>
        <source>10°</source>
        <translation>10°</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/telescope/suivitelescope.ui" line="317"/>
        <source>15°</source>
        <translation>15°</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/telescope/suivitelescope.ui" line="322"/>
        <source>20°</source>
        <translation>20°</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/telescope/suivitelescope.ui" line="327"/>
        <source>Autre...</source>
        <translation>Other...</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/telescope/suivitelescope.ui" line="404"/>
        <source>Pas de génération :</source>
        <translation>Output step :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/telescope/suivitelescope.ui" line="453"/>
        <source>ms</source>
        <extracomment>milliseconde</extracomment>
        <translation>ms</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/telescope/suivitelescope.ui" line="470"/>
        <location filename="../src/interface/onglets/telescope/suivitelescope.cpp" line="973"/>
        <source>Ouvrir Satellite Tracker</source>
        <translation>Open Satellite Tracker</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/telescope/suivitelescope.ui" line="492"/>
        <source>s</source>
        <translation>s</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/telescope/suivitelescope.ui" line="512"/>
        <source>Délai :</source>
        <comment>telescope</comment>
        <translation>Delay :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/telescope/suivitelescope.ui" line="531"/>
        <source>Afficher</source>
        <translation>Show file</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/telescope/suivitelescope.ui" line="544"/>
        <source>Envoyer les données satellite à la monture</source>
        <translation>Send satellite data to mount</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/telescope/suivitelescope.ui" line="547"/>
        <source>Démarrer</source>
        <translation>Start</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/telescope/suivitelescope.ui" line="560"/>
        <source>Ajuster les dates...</source>
        <translation>Adjust the dates...</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/telescope/suivitelescope.ui" line="590"/>
        <source>Le satellite n&apos;est pas visible depuis le lieu d&apos;observation</source>
        <translation>The satellite is not visible from the location</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/telescope/suivitelescope.ui" line="621"/>
        <source>Nom :</source>
        <translation>Name :</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/telescope/suivitelescope.ui" line="740"/>
        <source>Filtre</source>
        <translation>Filter</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/telescope/suivitelescope.cpp" line="329"/>
        <source>Satellite en éclipse</source>
        <translation>Satellite in eclipse</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/telescope/suivitelescope.cpp" line="329"/>
        <source>Satellite éclairé</source>
        <translation>Satellite illuminated</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/telescope/suivitelescope.cpp" line="342"/>
        <source>%1 (dans %2). Azimut : %3</source>
        <comment>Delay in hour, minutes, seconds</comment>
        <translation>%1 (in %2). Azimuth : %3</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/telescope/suivitelescope.cpp" line="344"/>
        <source>AOS</source>
        <comment>Acquisition of signal</comment>
        <translation>AOS</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/telescope/suivitelescope.cpp" line="363"/>
        <source>%1%2. Azimut : %3</source>
        <translation>%1%2. Azimuth : %3</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/telescope/suivitelescope.cpp" line="368"/>
        <location filename="../src/interface/onglets/telescope/suivitelescope.cpp" line="428"/>
        <source>h</source>
        <comment>hour</comment>
        <translation>h</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/telescope/suivitelescope.cpp" line="368"/>
        <location filename="../src/interface/onglets/telescope/suivitelescope.cpp" line="373"/>
        <location filename="../src/interface/onglets/telescope/suivitelescope.cpp" line="428"/>
        <location filename="../src/interface/onglets/telescope/suivitelescope.cpp" line="432"/>
        <source>min</source>
        <comment>minute</comment>
        <translation>min</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/telescope/suivitelescope.cpp" line="373"/>
        <location filename="../src/interface/onglets/telescope/suivitelescope.cpp" line="432"/>
        <source>s</source>
        <comment>second</comment>
        <translation>s</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/telescope/suivitelescope.cpp" line="404"/>
        <location filename="../src/interface/onglets/telescope/suivitelescope.cpp" line="482"/>
        <source>Satellite dans le ciel. Hauteur actuelle : %1. Azimut : %2. %3</source>
        <translation>Satellite in the sky. Current elevation : %1. Azimuth : %2. %3</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/telescope/suivitelescope.cpp" line="620"/>
        <source>Aucun satellite n&apos;est sélectionné dans la liste</source>
        <translation>None satellite selected in the list</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/telescope/suivitelescope.cpp" line="645"/>
        <source>LOS</source>
        <comment>Loss of signal</comment>
        <translation>LOS</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/telescope/suivitelescope.cpp" line="704"/>
        <source>Calculs en cours...</source>
        <translation>Calculating...</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/telescope/suivitelescope.cpp" line="705"/>
        <source>Annuler</source>
        <translation>Cancel</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/telescope/suivitelescope.cpp" line="734"/>
        <source>Calculs terminés</source>
        <translation>Done</translation>
    </message>
    <message>
        <location filename="../src/interface/onglets/telescope/suivitelescope.cpp" line="974"/>
        <source>Fichiers exécutables (*.exe)</source>
        <translation>Executable files (*.exe)</translation>
    </message>
</context>
<context>
    <name>Telechargement</name>
    <message>
        <location filename="../src/librairies/systeme/telechargement.cpp" line="98"/>
        <source>Erreur lors du téléchargement du fichier %1</source>
        <translation>Error while downloading file %1</translation>
    </message>
    <message>
        <location filename="../src/librairies/systeme/telechargement.cpp" line="124"/>
        <source>Impossible d&apos;écrire le fichier %1 dans le répertoire %2</source>
        <translation>Impossible to write the file %1 in the directory %2</translation>
    </message>
    <message>
        <location filename="../src/librairies/systeme/telechargement.cpp" line="209"/>
        <source>o/s</source>
        <translation>B/s</translation>
    </message>
    <message>
        <location filename="../src/librairies/systeme/telechargement.cpp" line="212"/>
        <source>ko/s</source>
        <translation>kB/s</translation>
    </message>
    <message>
        <location filename="../src/librairies/systeme/telechargement.cpp" line="215"/>
        <source>Mo/s</source>
        <translation>MB/s</translation>
    </message>
</context>
<context>
    <name>TelechargementOptions</name>
    <message>
        <location filename="../src/interface/options/telechargementoptions.ui" line="61"/>
        <source>Télécharger...</source>
        <translation>Download...</translation>
    </message>
    <message>
        <location filename="../src/interface/options/telechargementoptions.ui" line="77"/>
        <source>Fermer</source>
        <translation>Close</translation>
    </message>
    <message>
        <location filename="../src/interface/options/telechargementoptions.ui" line="118"/>
        <source>Filtre</source>
        <translation>Filter</translation>
    </message>
    <message>
        <location filename="../src/interface/options/telechargementoptions.cpp" line="132"/>
        <source>Télécharger des lieux d&apos;observation...</source>
        <translation>Download location files...</translation>
    </message>
    <message>
        <location filename="../src/interface/options/telechargementoptions.cpp" line="136"/>
        <source>Télécharger des cartes du monde...</source>
        <translation>Download world maps...</translation>
    </message>
    <message>
        <location filename="../src/interface/options/telechargementoptions.cpp" line="140"/>
        <source>Télécharger des fichiers de notification sonore...</source>
        <translation>Download sound files...</translation>
    </message>
    <message>
        <location filename="../src/interface/options/telechargementoptions.cpp" line="283"/>
        <source>Information</source>
        <translation>Information</translation>
    </message>
    <message>
        <location filename="../src/interface/options/telechargementoptions.cpp" line="283"/>
        <source>Veuillez redémarrer %1 pour prendre en compte la mise à jour</source>
        <translation>Please re-launch %1 to take into account the update</translation>
    </message>
</context>
<context>
    <name>cardinal point</name>
    <message>
        <location filename="../src/librairies/observateur/observateur.cpp" line="50"/>
        <source>N</source>
        <translation>N</translation>
    </message>
    <message>
        <location filename="../src/librairies/observateur/observateur.cpp" line="51"/>
        <source>NNE</source>
        <translation>NNE</translation>
    </message>
    <message>
        <location filename="../src/librairies/observateur/observateur.cpp" line="52"/>
        <source>NE</source>
        <translation>NE</translation>
    </message>
    <message>
        <location filename="../src/librairies/observateur/observateur.cpp" line="53"/>
        <source>ENE</source>
        <translation>ENE</translation>
    </message>
    <message>
        <location filename="../src/librairies/observateur/observateur.cpp" line="54"/>
        <source>E</source>
        <translation>E</translation>
    </message>
    <message>
        <location filename="../src/librairies/observateur/observateur.cpp" line="55"/>
        <source>ESE</source>
        <translation>ESE</translation>
    </message>
    <message>
        <location filename="../src/librairies/observateur/observateur.cpp" line="56"/>
        <source>SE</source>
        <translation>SE</translation>
    </message>
    <message>
        <location filename="../src/librairies/observateur/observateur.cpp" line="57"/>
        <source>SSE</source>
        <translation>SSE</translation>
    </message>
    <message>
        <location filename="../src/librairies/observateur/observateur.cpp" line="58"/>
        <source>S</source>
        <translation>S</translation>
    </message>
    <message>
        <location filename="../src/librairies/observateur/observateur.cpp" line="59"/>
        <source>SSW</source>
        <translation>SSW</translation>
    </message>
    <message>
        <location filename="../src/librairies/observateur/observateur.cpp" line="60"/>
        <source>SW</source>
        <translation>SW</translation>
    </message>
    <message>
        <location filename="../src/librairies/observateur/observateur.cpp" line="61"/>
        <source>WSW</source>
        <translation>WSW</translation>
    </message>
    <message>
        <location filename="../src/librairies/observateur/observateur.cpp" line="62"/>
        <source>W</source>
        <translation>W</translation>
    </message>
    <message>
        <location filename="../src/librairies/observateur/observateur.cpp" line="63"/>
        <source>WNW</source>
        <translation>WNW</translation>
    </message>
    <message>
        <location filename="../src/librairies/observateur/observateur.cpp" line="64"/>
        <source>NW</source>
        <translation>NW</translation>
    </message>
    <message>
        <location filename="../src/librairies/observateur/observateur.cpp" line="65"/>
        <source>NNW</source>
        <translation>NNW</translation>
    </message>
</context>
<context>
    <name>planet</name>
    <message>
        <location filename="../src/librairies/corps/systemesolaire/planeteconst.h" line="73"/>
        <source>Mercure</source>
        <translation>Mercury</translation>
    </message>
    <message>
        <location filename="../src/librairies/corps/systemesolaire/planeteconst.h" line="74"/>
        <source>Vénus</source>
        <translation>Venus</translation>
    </message>
    <message>
        <location filename="../src/librairies/corps/systemesolaire/planeteconst.h" line="75"/>
        <source>Mars</source>
        <translation>Mars</translation>
    </message>
    <message>
        <location filename="../src/librairies/corps/systemesolaire/planeteconst.h" line="76"/>
        <source>Jupiter</source>
        <translation>Jupiter</translation>
    </message>
    <message>
        <location filename="../src/librairies/corps/systemesolaire/planeteconst.h" line="77"/>
        <source>Saturne</source>
        <translation>Saturn</translation>
    </message>
    <message>
        <location filename="../src/librairies/corps/systemesolaire/planeteconst.h" line="78"/>
        <source>Uranus</source>
        <translation>Uranus</translation>
    </message>
    <message>
        <location filename="../src/librairies/corps/systemesolaire/planeteconst.h" line="79"/>
        <source>Neptune</source>
        <translation>Neptune</translation>
    </message>
</context>
</TS>
